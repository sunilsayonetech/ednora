"""
Django settings for pinry project.

Generated by 'django-admin startproject' using Django 1.9.5.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.9/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_DIR = os.path.abspath(os.path.join(BASE_DIR, os.path.pardir))
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '4&!@wp&8c#t%+&obfy-osm*#o8d-i^7pd%n_vz1#4mgt27dk^v'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
ADMINS = (
   ('Ranju', 'ranju.sayone@gmail.com'),
   ('Sunil', 'sunil@sayonetech.com')
)

MANAGERS = ADMINS

ALLOWED_HOSTS = ['www.ednora.com']


SITE_ID = 1

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    # Postr Apps
    'applications.accounts',
    'applications.activity',
    'applications.pin',
    'el_pagination',
    'rest_framework',
    'sorl.thumbnail',
    'haystack',
    'youtube_thumbnail',
    'import_export',
    'django_user_agents',
    'robots',
    'static_sitemaps',
    'ckeditor',
    'tz_detect',

]

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_user_agents.middleware.UserAgentMiddleware',
    'tz_detect.middleware.TimezoneMiddleware',
]

ROOT_URLCONF = 'postr.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates'), ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]



WSGI_APPLICATION = 'postr.wsgi.application'

#Pointing to User model
AUTH_USER_MODEL = 'accounts.User'

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INDEX_NAME': 'haystack',
    },
}
HAYSTACK_SIGNAL_PROCESSOR = 'applications.pin.update_index.ContentOnlySignalProcessor'

HAYSTACK_DEFAULT_OPERATOR = "AND"
# HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'pinry',
#         'USER': 'pinry_user',
#         'PASSWORD': '1234',
#         'HOST': 'localhost',
#         'PORT': '',
#     }
# }
# this database is used for testing downloaded images saving purpose
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'postr',
#         'USER': 'root',
#         'PASSWORD': 'root',
#         'HOST': 'localhost',
#         'PORT': '',
#     }
# }



REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ]
}

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_HOST_USER = os.getenv('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.getenv('EMAIL_HOST_PASSWORD')

DEFAULT_FROM_EMAIL = os.getenv('DEFAULT_FROM_EMAIL')
TOKEN = os.getenv('TOKEN')
# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'


USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

STATICFILES_DIRS = (os.path.join(BASE_DIR, 'staticfiles'), )

# media

MEDIA_URL = '/media/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')

LOGIN_URL = '/'

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, "templates", ),
)

STATICSITEMAPS_ROOT_SITEMAP = 'postr.sitemaps.sitemaps'


INDICOIO_API_KEY = '24f4169ada4441912d9a92c24294f6f8'

PAGE2IMAGES_API_KEY = os.getenv('PAGE2IMAGES_API_KEY')

COMMENT_PAGINATION_SIZE = 5
ARTICLE_PARA_LIMIT = 2
FLAG_COUNT_TO_SUPPRESS = 10

# THUMBNAIL_COLORSPACE = None
#
THUMBNAIL_PRESERVE_FORMAT = True

try:
    from .local import *
except:
    pass

DEV_INSTALLED_APPS = [
    'sslserver',
]

if DEBUG:
    INSTALLED_APPS = INSTALLED_APPS+DEV_INSTALLED_APPS
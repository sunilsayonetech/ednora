"""postr URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required
from django.conf.urls import (handler404, handler500)

from applications.accounts import views as accounts_view
from applications.pin import views as pin_view
from applications.accounts.views import DistrictPopulateView, SchoolPopulateView, DownloadFile, DeletePin, \
    DeletePinFromFolder, CheckContent
from applications.accounts.decorators import only_for_anonymous_user

admin.site.site_header = 'Ednora administration'
admin.site.site_title = 'Ednora site admin'

handler404 = 'applications.accounts.views.page_not_found'
handler500 = 'applications.accounts.views.server_error'

urlpatterns = [
    url(r'^0pi1ELTP/', admin.site.urls),
    url(r'^$', accounts_view.Registration.as_view(), name='register'),
    # url(r'^step-two/$', accounts_view.RegistrationStepTwo.as_view(), name='register_step_two'),
    url(r'^confirm/(?P<key>\w+)/$', accounts_view.ConfirmView.as_view(), name='email_confirm'),
    url(r'^home/$', login_required(accounts_view.LandingPageView.as_view()), name='landing_page'),
    url(r'^login/$', accounts_view.LoginView.as_view(), name='login'),
    # url(r'^register/$', accounts_view.LoginView.as_view(), name='register'),
    url(r'^logout/$', accounts_view.LogoutView.as_view(), name='logout'),
    url(r'^password-reset/$', only_for_anonymous_user(accounts_view.PasswordReset.as_view()), name='password-reset'),
    url(r'^privacy-policy/$', accounts_view.PrivacyPolicyView.as_view(), name='privacy'),
    url(r'^copyright-policy/$', accounts_view.CookiePolicyView.as_view(), name='cookie'),
    url(r'^how-it-works/$', accounts_view.HowItWorksView.as_view(), name='how_it_works'),
    url(r'^terms-of-use/$', accounts_view.TermsServiceView.as_view(), name='terms'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/', include('applications.api.urls')),
    url(r'^password-change/(?P<key>\w+)/$', only_for_anonymous_user(accounts_view.PasswordChangeView.as_view()), name='pass-change'),
    url(r'^create-folder/$', login_required(pin_view.CreateFolderView.as_view()), name="create_folder"),
    url(r'^folder/(?P<slug>[\w-]+)/$', login_required(pin_view.FolderDescriptionView.as_view()), name="folder_description"),
    url(r'^re-pin/$', login_required(pin_view.RepinView.as_view()), name="re-pin"),
    url(r'^get-content/$', login_required(pin_view.GetContentView.as_view()), name="get-content"),
    url(r'^get-tags/$', login_required(pin_view.GetTagtView.as_view()), name="get-tags"),
    url(r'^get-content-image/$',login_required(pin_view.GetContentImageView.as_view()), name="get-content-image"),
    url(r'^upload-pin/$', login_required(pin_view.UploadPin.as_view()), name="upload_pin"),
    url(r'^user-folders/$', login_required(pin_view.GetUserFolder.as_view()), name="user_folders"),
    url(r'^user-folders-repin/$', login_required(pin_view.GetUserFolderForRepin.as_view()), name="user_folders_for_re_pin"),
    url(r'^user-folders-home/$', login_required(pin_view.GetUserFolderForHomeview.as_view()), name="user_folders_for_home_view"),
    url(r'^search/?$', login_required(pin_view.CustomSearchView.as_view()), name='search_view'),
    url(r'^user-verify-link/$', login_required(accounts_view.SentVerifylink.as_view()), name="user_verify_link_sent"),
    url(r'^user-like/$', login_required(pin_view.UserLike.as_view()), name="user_like"),
    url(r'^user-flag/$', login_required(pin_view.UserFlag.as_view()), name="user_flag"),
    url(r'^user-download/$', login_required(pin_view.UserDownload.as_view()), name="user_download"),
    url(r'^my-likes/$', login_required(pin_view.MyLike.as_view()), name="my_like"),
    url(r'^activities/$', login_required(pin_view.ActivityView.as_view()), name="activities"),
    url(r'^user-ajax-like/$', login_required(pin_view.UserLikeAjax.as_view()), name="user_ajax_like"),
    url(r'^user-folders-pin-delete', login_required(pin_view.DeleteContentFoder.as_view()), name="user_folder_delete"),
    url(r'^user-like-thumbnail/', login_required(pin_view.Likeindetail.as_view()), name='user_like_thumbnail'),
    url(r'^user-flag-thumbnail/', login_required(pin_view.Flagindetail.as_view()), name='user_flag_thumbnail'),
    url(r'^user-download-thumbnail/', login_required(pin_view.Downloadindetail.as_view()), name='user_download_thumbnail'),
    url(r'^profile/$', login_required(accounts_view.ProfileView.as_view()), name='profile'),
    url(r'^upv/(?P<slug>[\w-]+)/$', pin_view.UserInviteView.as_view(), name='user_public_view'),
    url(r'^user-invites/$', login_required(pin_view.UserInvites.as_view()), name='user_invites'),
    url(r'^user-deactivate/$', login_required(accounts_view.UserDeactivate.as_view()), name='user_deactivate'),
    url(r'^generate-json/$',login_required(pin_view.GetJSONUserTracking.as_view()),name ='generate_json'),
    url(r'^user-profile-dp-delete/$',login_required(accounts_view.UserDPDeleteView.as_view()),name='delete_user_dp'),
    url(r'^user-share/$',login_required(pin_view.UserShareEmail.as_view()),name='usershareemail'),
    # url(r'^resource/(?P<slug>[\w-]+)/$', pin_view.GeneralPinView, name='viewpindetails'),
    url(r'^resource/(?P<slug>[\w-]+)/$',accounts_view.LandingPageView.as_view(), name='viewpindetails'),
    url(r'^upload-recommendations/$',login_required(pin_view.UserRecommendations.as_view()),name='upload_recommendations'),
    url(r'^upload-email-recommendations/$',login_required(pin_view.UserEmailRecommendations.as_view()),name='upload_email_recommendations'),
    url(r'^update-weekly-email-alert/$',login_required(accounts_view.UpdateWeeklyEmailAlertView.as_view()),name='user_weekly_email_alert'),
    url(r'^update-district-preference/$',login_required(accounts_view.UpdateDistrictPreference.as_view()),name='user_district_preference'),
    url(r'^update-state-preference/$',login_required(accounts_view.UpdateStatePreference.as_view()),name='user_state_preference'),
    url(r'^update-nation-preference/$',login_required(accounts_view.UpdateNationPreference.as_view()),name='user_nation_preference'),
    url(r'^robots\.txt', include('robots.urls')),
    url(r'summary/$', login_required(pin_view.SummaryView.as_view()), name='summary'),
    url(r'', include('applications.accounts.urls')),
    url(r'^user-invite-facebok/$', accounts_view.UserFacebookInvite.as_view(), name='UserFacebookInvite'),
    url(r'^sitemap.xml', include('static_sitemaps.urls')),
    url(r'^yahoo',TemplateView.as_view(template_name='yahoo_invite.html'),name='yahoo'),
    url(r'^user-share-delete', login_required(pin_view.DeleteShareTracking.as_view()), name='share_delete'),
    url(r'^user-invite-delete', login_required(pin_view.DeleteInviteTracking.as_view()), name='invite_delete'),
    url(r'^redirect',TemplateView.as_view(template_name='oauth.html'),name='redirect'),
    url(r'^pdf-screenshot', pin_view.PdfScreenshotPreview.as_view(), name='pdf_screenshot'),
    # url(r'^social-share/$',login_required(pin_view.DeleteShareTracking.as_view()),name='share_url'),
    url(r'^district-populate',DistrictPopulateView.as_view(),name='district_populate'),
    url(r'^school-populate',SchoolPopulateView.as_view(),name='school_populate'),
    url(r'^download-file/(?P<id>[0-9]+)/$',login_required(DownloadFile.as_view()),name='download_file'),
    url(r'^delete-pin', DeletePin.as_view(),name='delete_pin'),
    url(r'^get-file-image', pin_view.GetFileScreenshotImage.as_view(), name='file_image'),
    url(r'^get-resource-image', pin_view.GetResourceTypeImage.as_view(), name='resource_image'),
    url(r'^pin-delete-from-folder',DeletePinFromFolder.as_view(),name='delete_pin_from_folder'),
    url(r'^user-flag-check/', login_required(pin_view.FlaginCheck.as_view()), name='user_flag_check'),
    url(r'^check-lastfolder/', CheckContent.as_view(), name='folder_check'),
    url(r'^check-url-in-folder/', login_required(pin_view.CheckUrlInFolder.as_view()), name='url-checking'),
    url(r'^login-via-model/', accounts_view.LoginModel.as_view(), name='login-model'),
    # url(r'^search/?$', include('haystack.urls')),
    url(r'^tz_detect/', include('tz_detect.urls')),
]
if settings.DEBUG:
    pass
    # static files (images, css, javascript, etc.)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



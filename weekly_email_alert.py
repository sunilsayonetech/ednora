__author__ = 'sayone'
###################################################
#
# This code belongs to pickks.com. Any use of this
# code outside of pickks.com framework is a violation
# of pickks.com intellectual property and pickks.com
# reserves the right to take required legal action.
#
###################################################

#sklearn
#numpy
#scipy

# Following variable needs to be turned on for
# mode.
TestMode = False
debug = False
debug_user_id = 27
debug_content_id = None

import glob
import datetime
import re
import iso8601
import pytz
import json
import operator
import os

print ("Starting")
print (datetime.datetime.now())

###################################################
# Configurations
###################################################
weekly_reco_count = 10

###################################################
# Class definitions
###################################################
class UserReco:
   'User Recommendation details'

   def __init__(self, id):
       self.id = id
       self.reco = {}
       self.recoOut = []

   # Pick the top ten recommendation
   def AddRecommendation(self, age, recommendations):
      self.reco[age] = recommendations[0:9]

def AgeOfFile(filename):
   reg_exp = "result_([-_:\d]+)\.json"
   reg_comp = re.compile(reg_exp)
   reg_match = reg_comp.match(filename)
   date = reg_match.group(1)
   date = date.replace('_',' ')
   d1 = iso8601.parse_date(date)
   n1 = datetime.datetime.now(tz=pytz.utc)
   diff = n1 - d1
   diff_days = diff.days
   return diff_days

def AgeMultiplier(age):
#    d1 = iso8601.parse_date(date)
#    n1 = datetime.datetime.now(tz=pytz.utc)
#    diff = n1 - d1
#    diff_days = diff.days
   mult = 5*pow(0.5, age)+1
   return mult

uid_user_reco_hash = {}

##############################################
# This function reads the JSON file and
# populates UserReco. No other processing is
# done here
##############################################
def ReadRecommendationJSON(file_to_read, age):
   with open(file_to_read, 'r') as reco_file:
      for line in reco_file:
         reco = json.loads(line.replace('\r\n', ''))

         user_reco = None
         uid = reco["UID"]
         if uid in uid_user_reco_hash:
            user_reco = uid_user_reco_hash[uid]
         else:
            user_reco = UserReco(uid)
            uid_user_reco_hash[uid] = user_reco

         user_reco.AddRecommendation(age, reco["Recommendations"])


##############################################
# This function takes all the top recommendations
# for a user and makes a top 10 list.
# It first adds top 10 recommendations from day
##############################################
def MakeRecommendations(top_reco_count, result_dir):
   outfile_list = []
   for uid in uid_user_reco_hash.keys():
      recoWeightHash = {}
      userReco = uid_user_reco_hash[uid]
      reco = userReco.reco
      for age in reco.keys():
         reco_on_age = reco[age]
         age_mult = 5*pow(0.5, (8-age))
         for idx in range(len(reco_on_age)):
            idx_mult = 5*pow(0.5, 11-idx)
            new_weight = age_mult * idx_mult
            cid = reco_on_age[idx]
            current_weight = 0
            if cid in recoWeightHash.keys():
               current_weight = recoWeightHash[cid]
            if new_weight > current_weight:
               recoWeightHash[cid] = new_weight

      # Now sort the hash and pick top reco
      sortedReco = sorted(recoWeightHash.items(), key=operator.itemgetter(1))
      sortedReco.reverse()
      topPredContent = [w[0] for w in sortedReco[:top_reco_count]]

      rec_to_dump = {}
      rec_to_dump["Recommendations"] = topPredContent
      rec_to_dump["UID"] = uid
      outfile_list.append(rec_to_dump)

   # Now dump
   fn = str(datetime.datetime.now())
   fn = fn.replace(' ','_')
   fn = fn.split('.')[0]
   filename = result_dir + "/weekly_"+fn+".json"
   # replace any space with '_' and get rid
   # of millisecond and use that as result filename
   with open(filename, 'w') as fp:
      for item in outfile_list:
         json.dump(item, fp, sort_keys=True)
         fp.write('\n')

   return filename



###################################################
# The main routine that puts things together
###################################################
def GenerateRecommendation(dump_dir, result_dir):
   # Step 1
   # First read recommendation file from past 7 days
   glob_str = dump_dir + "result_*.json"
   all_reco_files_to_read = glob.glob(glob_str)
   for file_to_read in all_reco_files_to_read:
      print(file_to_read)
      split_filename = file_to_read.split('/')
      filename_only = split_filename[len(split_filename)-1]
      age = AgeOfFile(filename_only)
      if (age < 8):
         ReadRecommendationJSON(file_to_read, age)
      else:
         os.remove(file_to_read)
   print ("Done reading recommendation JSON file")
   print (datetime.datetime.now())

   filename = MakeRecommendations(weekly_reco_count, result_dir)
   print ("Done creating weekly JSON file")
   print (datetime.datetime.now())
   return filename


__author__ = 'sayone'

__author__ = 'sayone'
from applications.pin.models import  Content

from django.core.management.base import NoArgsCommand
from tld import get_tld

class Command(NoArgsCommand):

   def handle_noargs(self, **options):


       self.Update_Base_Url()

       self.stdout.write("\n\n%% All done %%\n\n")

   def Update_Base_Url(self):

       contents = Content.objects.all()

       for content in contents:
           url = content.absolute_url
           baseurl = get_tld(url,as_object=True)
           st = str(baseurl)
           if baseurl.subdomain:
               st = baseurl.subdomain+"."+st
           if 'www' in url:
                base_url = 'www.'+st
           content.base_url = st
           content.save()




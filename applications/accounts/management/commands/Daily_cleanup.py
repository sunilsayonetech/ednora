__author__ = 'sayone'
import datetime
import os
import pytz
from django.core.management.base import NoArgsCommand
from django.utils import timezone
from applications.accounts.models import UserFilterTracking
from applications.accounts.models import UserFilterStat
from applications.pin.models import UserPinUploadTracking,UserPinUploadTrackingStat
from applications.activity.models import EmailConfirmation



class Command(NoArgsCommand):
   help = 'Daily upda maintenance/ cleanup jobs (once a day cronjob)'

   def handle_noargs(self, **options):
       self.stdout.write("\n\n*** " + self.help + " ***\n\n")

       self.Daily_cleanup()

       self.stdout.write("\n\n%% All done %%\n\n")

   def Daily_cleanup(self):



       user_filter_tracking_objs = UserFilterTracking.objects.all()
       for obj in user_filter_tracking_objs:
            user_filter_stat = UserFilterStat()
            user_filter_stat.count = obj.count
            user_filter_stat.content_filter = obj.content_filter
            self.stdout.write("\n\n%% All done %%\n\n"+obj.content_filter)
            user_filter_stat.save()
       UserFilterTracking.objects.all().delete()


       user_upload_objs = UserPinUploadTracking.objects.all()
       user_pin_upload_tracking_obj = UserPinUploadTrackingStat()
       user_pin_upload_tracking_obj.pin_upload_using_upload_pin_popup=0
       user_pin_upload_tracking_obj.pin_upload_using_get_browser_button =0

       for objup in user_upload_objs:
            user_pin_upload_tracking_obj.pin_upload_using_upload_pin_popup = user_pin_upload_tracking_obj.pin_upload_using_upload_pin_popup+objup.pin_upload_using_upload_pin_popup
            user_pin_upload_tracking_obj.pin_upload_using_get_browser_button = user_pin_upload_tracking_obj.pin_upload_using_get_browser_button+objup.pin_upload_using_get_browser_button

       user_pin_upload_tracking_obj.save()
       UserPinUploadTracking.objects.all().delete()



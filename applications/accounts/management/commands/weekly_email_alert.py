__author__ = 'sayone'
import os
import json
import itertools

from django.core.management.base import NoArgsCommand
from django.contrib.sites.models import Site
from django.template import loader, Context
from django.core.mail import EmailMessage
from django.db.models import Count
from weekly_email_alert import GenerateRecommendation

from applications.accounts.models import User as UserModel
from applications.accounts.models import UserWeeklyEmailAlert, UserDeactivation
from applications.pin.models import UserWeeklyEmailAlertRecommendation, Content


class Command(NoArgsCommand):
    def handle_noargs(self, **options):
        self.send_weekly_email_pins()
        self.stdout.write("\n\n%% All done %%\n\n")

    def send_weekly_email_pins(self):
        site = Site.objects.get_current()
        users = UserModel.objects.filter(verified=True).exclude(is_active=False)
        subscribed_users = users.exclude(weekly_email__is_subscribed=False)
        for user in subscribed_users:
            pins = Content.get_user_accessible_contents(user)
            # contents = pins.annotate(user_like=Count('get_user_like')).order_by('-created').order_by('-user_like').filter(subject__in=user.subject.all(), grade__in=user.grade.all()).exclude(user=user)
            # contents = pins.filter(subject__in=user.subject.all(), grade__in=user.grade.all()).exclude(user=user)
            contents = pins.exclude(user=user)
            final_contents = contents
            if contents.count() < 9:
                # contents_extra = pins.annotate(user_like=Count('get_user_like')).order_by('-created').order_by('-user_like').exclude(user=user,id__in=list(contents.values_list('id',flat=True)))
                contents_extra = pins.exclude(user=user,id__in=list(contents.values_list('id',flat=True)))
                final_contents = contents | contents_extra
            final_contents = final_contents[:9]
            if final_contents.count() > 0:
                try:
                    url = str(site)
                    t = loader.get_template('email/weekly_email_template.html')
                    c = Context({'user': user, 'link': url, 'site': site.name, 'pins': final_contents})
                    mail = EmailMessage(user.first_name + ', We found these picks for you on Ednora', t.render(c),
                                        'Ednora<no-reply@ednora.com>', (user.email, ))
                    mail.content_subtype = 'html'
                    mail.send()

                except Exception as e:
                    print(e)
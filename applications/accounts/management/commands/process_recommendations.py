import json
import shutil

from django.core.management.base import NoArgsCommand

from applications.pin.models import UserRecommendation, Content as pin
from applications.accounts.models import User as user_model
from applications.pin.views import GetJSONUserTracking
from django.template import loader, Context
from django.core.mail import EmailMessage
from django.conf import settings

from recsys import *

import os

current_path = os.getcwd()
JSON_CONTAINER_FOLDER_NAME = 'user_tracking'
user_tracking_folder_path = current_path + '/' + JSON_CONTAINER_FOLDER_NAME

class Command(NoArgsCommand):

    def handle_noargs(self, **options):
        """
        Creates a new result file, processes it and populates in DB
        :param options:
        :return:
        """
        emails = ('smitasinha787@gmail.com ', 'nakul.sayone@gmail.com', 'pranayprakash1@gmail.com ')
        # process and create user_tracking contents
        view_obj = GetJSONUserTracking()
        view_obj.get('dummy')
        # Ends here

        path_result = current_path + '/user_recommendations'
        if not os.path.exists(path_result):
            os.makedirs(path_result)

        # Processing the script here
        try:

            path = GenerateRecommendation(user_tracking_folder_path, path_result)
            msg = "Recommendation Engine Executed Successfully"
        except Exception as e:
            msg = e
        t = loader.get_template('email/email_recommendation.html')
        c = Context({'msg': msg})
        mail = EmailMessage('Ednora Recommendation Engine Result', t.render(c), 'no-reply@ednora.com', emails)
        mail.content_subtype = 'html'
        mail.send()
        # ends here

        # Db population process
        result_file = open(path, 'r')
        UserRecommendation.objects.all().delete()
        file = result_file.readlines()

        for line in file:
            try:
                  row = json.loads(line)
                  uid = row['UID']
                  content_ids = row['Recommendations']
                  user = user_model.objects.filter(pk = uid)
                  ur = UserRecommendation()
                  ur.user = user[0]
                  ur.recommended_pins=json.dumps(content_ids)
                  ur.save()

            except Exception as e :
                print(e)
                '''
                    t = loader.get_template('email/email_recommendation.html')
                    c = Context({'msg': e})
                    emails = 'nakul.sayone@gmail.com'
                    mail = EmailMessage('Ednora Recommendation Engine Result', t.render(c), 'no-reply@ednora.com', emails)
                    mail.content_subtype = 'html'
                    mail.send()'''

        # Ends here

        shutil.rmtree(user_tracking_folder_path) # Comment it out if to keep the directly alive

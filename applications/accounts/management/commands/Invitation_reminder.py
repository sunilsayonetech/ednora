__author__ = 'sayone'
from applications.accounts.models import  UserInvites,User
from datetime import datetime
import pytz
from django.template import loader, Context
from django.core.mail import EmailMessage
from django.conf import settings
from django.contrib.sites.models import Site
from applications.pin.models import Content
from applications.pin.models import Pin
from django.core.management.base import NoArgsCommand

class Command(NoArgsCommand):

   def handle_noargs(self, **options):


       self.Invitation_Reminder()

       self.stdout.write("\n\n%% All done %%\n\n")

   def Invitation_Reminder(self):
       site = Site.objects.get_current()
       num = 0
       msg = ''
       user_invite_objs = UserInvites.objects.filter(is_invite_accepted=False).exclude(email = 'facebook')
       for obj in user_invite_objs:
                 try:
                   date_diff = datetime.now(pytz.UTC)-obj.created_time
                   if date_diff.days > 18:
                           obj.delete()
                   if date_diff.days in [1, 6, 12, 18]:
                       num = len(User.objects.filter(email = obj.email))
                       if num ==1:
                            msg = 'View Picks'
                       else:
                             msg = 'Accept Invite'
                       user = obj.user
                       slug = obj.user.slug
                       url = str(site)+"/upv/"+str(slug)+'/?p='+str(obj.id)
                       p = Pin.objects.filter(folder__user =user ).select_related('content').values_list('content',flat = True)
                       pins =  Content.objects.filter(pk__in=p)[:3]
                       t = loader.get_template('email/invite_email_template.html')
                       c = Context({'user': user, 'link': url, 'site': site.name,'pins':pins,'msg':msg})
                       mail = EmailMessage('Reminder: ' + user.first_name + ' has invited you to Ednora', t.render(c), user.first_name + ' via Ednora<no-reply@ednora.com>', (obj.email, ))
                       mail.content_subtype = 'html'
                       mail.send()



                 except Exception as e:
                     print(e)


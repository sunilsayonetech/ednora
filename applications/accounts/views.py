import os
import json
import pytz
import magic
import re

from binascii import hexlify
from datetime import datetime

from django.shortcuts import render
from django.template import RequestContext
from django.views.generic import TemplateView, FormView
from django.views.generic import View
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render_to_response
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
from django.template import loader, Context
from django.core.mail import EmailMessage
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.utils.decorators import method_decorator
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.views.generic import TemplateView, DetailView
from django.http import JsonResponse
from django.utils.encoding import smart_str
from django.contrib.auth.decorators import login_required
from django.http import Http404

from .forms import SignupForm, LoginForm
from .models import User, State, UserFeedBack, Contact, District
from .models import UserTracking, UserDeactivation, UserDeviceTracking, UserFilterTracking, TermsOfUse
from .models import PrivacyPolicy, CopyrightPolicy, Grade, Subject, School
from applications.pin.models import Level
from applications.activity.models import EmailConfirmation
from applications.accounts.forms import ChangePasswordForm, UpdateProfile
from applications.pin.models import Content, ContentType, Folder, Pin, UserRecommendation, Tag, LearningObjective, \
    LearningObjectiveCode, MediaType, Comment
from applications.accounts.utils import get_client_ip
from applications.accounts.models import UserWeeklyEmailAlert, UserShareTracking, UserInvites, HowItWorks


def verified_required(function):
    """
    to check the user is verified or not
    :param function:
    :return:
    """

    def wrapper(request, *args, **kw):
        user = request.user
        if user.verified:
            return function(request, *args, **kw)
        else:
            return HttpResponseRedirect(reverse('landing_page'))

    return wrapper

class Registration(TemplateView):
    """
    View for rendering register page
    """
    template_name = 'register/register.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):

        if self.request.user.is_authenticated():
            return redirect('landing_page')
        return super(Registration, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        """
        pass login form and signup form in context
        """
        context = super(Registration, self).get_context_data(**kwargs)
        flag = self.request.session.get('data', None)
        if flag:
            context['form'] = SignupForm(initial=flag)
        else:
            context['form'] = SignupForm()
        context['login_form'] = LoginForm()
        return context

    def post(self, request, *args, **kwargs):
        """
        stores the data posted by user into session
        """
        form = SignupForm(request.POST)
        login_form = LoginForm()
        if form.is_valid():
            user_name = form.clean_email()
            user_password = form.cleaned_data['password']
            grade_list = request.POST.getlist('grade')
            subject_list = request.POST.getlist('subject')
            user_obj = get_user_model().objects.create_user(username=user_name, email=form.clean_email(),
                                                            password=user_password)
            user_obj.first_name = re.sub(' +', ' ', form.clean_first_name())
            user_obj.last_name = re.sub(' +', ' ', form.clean_last_name())
            user_obj.state = get_object_or_404(State, pk=form.clean_state().id)
            user_obj.district = get_object_or_404(District, pk=form.clean_district().id)
            # user_obj.school = get_object_or_404(School, pk=form.clean_school().id)
            school_obj, created = School.objects.get_or_create(
                name=re.sub(' +', ' ', form.cleaned_data["skool"]), district=form.cleaned_data["district"]
            )
            user_obj.school = school_obj
            user_obj.grade = grade_list
            user_obj.subject = subject_list
            user_obj.save()

            # Send email with activation key
            key = self.generate_hash_key()
            # generating confirmation link to be send to user using hash_key
            link = self.generate_link(key)
            object = EmailConfirmation.objects.create(user=user_obj, key=key)
            object.save()
            t = loader.get_template('email/confirm_email.html')

            site = Site.objects.get_current()
            c = Context({'user': user_obj.first_name, 'link': link, 'site': site.name})
            # email = EmailMessage('Verify your email address', t.render(c), settings.EMAIL_HOST_USER, (user_obj.email, ))
            email = EmailMessage('Verify your email address', t.render(c), 'Ednora <no-reply@ednora.com>',
                                 (user_obj.email, ))
            email.content_subtype = 'html'
            email.send()

            user = authenticate(username=user_name, password=user_password)
            self.request.session.flush()
            login(request, user)

            return HttpResponseRedirect(reverse('landing_page'))

        else:
            return render(request, self.template_name, {'form': form, 'login_form': login_form})

    def generate_hash_key(self):
        hash_key = hexlify(os.urandom(15))
        return hash_key

    def generate_link(self, key):
        url = reverse('email_confirm', kwargs={'key': key})
        current_site = Site.objects.get_current()
        url = current_site.domain + url
        return url


class LandingPageView(TemplateView):
    """
     to display pins associated for users
    """
    template_name = 'home/home.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(LandingPageView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        """
        pass all pins in backend as context and filter
        """
        context = super(LandingPageView, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated():
            user = self.request.user
            context['pins'] = Content.get_user_accessible_contents(user)
            if user.first_login and user.verified:
                user.first_login = False
                user.save()
        if re.match(r'^/resource', self.request.path):
            context['in_resource_page'] = True
            try:
                context['detailpin'] = Content.objects.get(slug=kwargs['slug'])
            except:
                raise Http404  #otherwise after deleting resource this will create 500


        return context

    def render_to_response(self, context, **response_kwargs):
        response = super(LandingPageView, self).render_to_response(context, **response_kwargs)
        response.set_cookie('loggedin', 'true')
        response.delete_cookie('wl_auth')
        return response

    def get(self, request, *args, **kwargs):
        super(LandingPageView, self).get(request, *args, **kwargs)
        context = self.get_context_data(**kwargs)
        page_template = 'home/pagination_snippet_home.html'
        context['is_user_deactivated'] = self.request.session.get('is_user_deactivated',"")
        try:
            if self.request.session.get('is_user_deactivated', "") is True:
                site = Site.objects.get_current()
                t = loader.get_template('email/mail_account_activation_success.html')
                c = Context({'user': self.request.user,  'site': site.name})
                mail = EmailMessage(self.request.user.first_name+', your Ednora account has been reactivated', t.render(c), 'Ednora <no-reply@ednora.com>', (self.request.user.email, ))
                mail.content_subtype = 'html'
                mail.send()
        except:pass
        self.request.session['is_user_deactivated']  = False
        context.update({'page_template': page_template})
        if request.is_ajax():
           template = page_template
        else:
            template = self.template_name
        if not self.request.user.is_authenticated():
            template = self.template_name
            page_template = 'home/single_resource.html'
            if context['slug']:
                context['pin'] = Content.objects.get(slug=kwargs['slug'])
                try:
                    content_share_id = int(self.request.GET.get('p'))
                    if content_share_id:
                        content_share_objects = UserShareTracking.objects.get(pk=content_share_id)
                        content_share_objects.is_visited = True
                        content_share_objects.save()
                        context['title'] = 'View shared resource'
                        context['welcome_message'] = str(content_share_objects.user.first_name)+"  has shared a pick with you!"
                except:
                    context['title'] = 'Sign in to continue'
                    template = self.template_name
                context.update({'page_template': page_template})
        return render_to_response(template, context, context_instance=RequestContext(request))

    def post(self, request, *args, **kwargs):
        ctx = {}
        ur = UserRecommendation.objects.get(user = self.request.user)


        post = request.POST.copy()
        content_type_id = post.get('id')
        my_folders = self.request.user.get_folder.all()
        my_pins = Content.objects.filter(pk__in=ur.recommended_pins.values_list('id',flat=True))
        ctx['status'] = 'success'
        ctx['html'] = render_to_string('folder/filter_folder.html', {'pins': my_pins, 'folder_obj': my_folders})
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class ConfirmView(View):
    """
    to check the email is verified
    """

    def get(self, request, *args, **kwargs):
        key = self.kwargs['key']
        if key:
            try:
                pass_obj = EmailConfirmation.objects.get(key=key)
                if pass_obj.link_expired:
                    if not request.user.is_anonymous():
                            return HttpResponseRedirect(reverse('landing_page'))
                    else:
                        messages.add_message(request, messages.INFO, 'The verification link you clicked has expired, Kindly request another link.', extra_tags='expired')
                else:
                    pass_obj.link_expired = True
                    pass_obj.save()
                    if not request.user.is_anonymous():
                        if pass_obj.user.email != request.user.email:
                            logout(request)
                    user_obj = User.objects.get(email=pass_obj.user.email)
                    user_obj.verified = True
                    user_obj.first_login = True
                    user_obj.save()
                    messages.add_message(request, messages.INFO, "Thank you for verifying your email. You now have full access to resources on Ednora"+"\n"+
                                         "- discover and save resources for future reference!"+"<br>"+
                                         "You may also upload content that you come across and find worth sharing on Ednora.",
                                         extra_tags='varify')
                    #return HttpResponseRedirect(reverse('landing_page'))
            except:
                messages.add_message(request, messages.INFO, 'The verification link you clicked is invalid, Kindly request another link.', extra_tags='expired')
        return HttpResponseRedirect(reverse('login'))


class LoginView(TemplateView):
    """
    User login
    """
    template_name = 'register/register.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        """
        if the user is logged in, redirect from login page to home page
        """
        if self.request.user.is_authenticated():
            return redirect('landing_page')
        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        context['login_form'] = LoginForm()
        context['form'] = SignupForm()

        return context

    def post(self, request, *args, **kwargs):
        email = request.POST.get("email", "")
        email = email.strip()
        password = request.POST['password']
        password = password.strip()
        data = {'email': email, 'password': password}
        login_form = LoginForm(data)
        flag_is_user_active =False
        form = SignupForm()
        if login_form.is_valid():
            next = request.POST.get("next", "")
            q = request.POST.get("url", "")
            q = q.strip()
            user = authenticate(username=email, password=password)
            user.first_login = False
            user.save()
            if user is not None:
                if not user.is_active:
                    flag_is_user_active = True
                    user.is_active = True
                    user.save()
                    # track user reactivation
                    ud = UserDeactivation()
                    ud.user = user
                    ud.type = '2'
                    ud.save()
                    request.user = user
                if user.is_active:
                    login(request, user)
                    if next and q:
                        response = redirect(next+"?url="+q)
                        response.set_cookie('loggedin', 'true')
                        return response

                    if next:
                        response = redirect(next)
                        response.set_cookie('loggedin', 'true')
                        return response

                    request.session['is_user_deactivated'] = flag_is_user_active

                    '''user device  tracking'''

                    ctx = dict()
                    ctx['os'] = self.request.user_agent.os.family
                    ctx['device'] = self.request.user_agent.device.family
                    ctx['browser'] = self.request.user_agent.browser.family
                    ctx['ip'] = get_client_ip(self.request)
                    ctx['user'] = self.request.user
                    UserDeviceTracking.objects.create(**ctx)

                    response = redirect('landing_page') # replace redirect with HttpResponse or render
                    response.set_cookie('loggedin', 'true')
                    return response

        else:
            return render(request, self.template_name, {'login_form': login_form, 'form': form})


class LogoutView(View):
    """
    logout user
    """

    def get(self, request, *args, **kwargs):
        logout(request)
        response = redirect('register')
        response.delete_cookie('loggedin')
        return response


class PrivacyPolicyView(TemplateView):
    template_name = 'privacy-policy.html'

    def get_context_data(self, **kwargs):
        context = super(PrivacyPolicyView, self).get_context_data(**kwargs)
        text = PrivacyPolicy.objects.all()
        context['text'] = text[0].text
        return context


class CookiePolicyView(TemplateView):
    template_name = 'copyright-policy.html'

    def get_context_data(self, **kwargs):
        context = super(CookiePolicyView, self).get_context_data(**kwargs)
        text = CopyrightPolicy.objects.all()
        context['text'] = text[0].text

        return context


class TermsServiceView(TemplateView):
    template_name = 'terms-of-service.html'

    def get_context_data(self, **kwargs):
        context = super(TermsServiceView, self).get_context_data(**kwargs)
        text = TermsOfUse.objects.all()
        context['text'] = text[0].text
        return context


class HowItWorksView(TemplateView):
    template_name = 'how-it-works.html'

    def get_context_data(self, **kwargs):
        context = super(HowItWorksView, self).get_context_data(**kwargs)
        obj = HowItWorks.objects.all().first()
        context['obj'] = obj
        return context


class PasswordReset(TemplateView):
    template_name = 'register/forgot-password.html'

    def get_context_data(self, **kwargs):
        context = super(PasswordReset, self).get_context_data(**kwargs)
        return context


class PasswordChangeView(TemplateView):
    template_name = 'register/confirm_password.html'

    def get(self, request, *args, **kwargs):
        form = ChangePasswordForm()
        key = self.kwargs['key']
        pass_obj = get_object_or_404(EmailConfirmation, key=key)
        date_diff = datetime.now(pytz.UTC)-pass_obj.created_time
        if date_diff.days>=1:
           pass_obj.link_expired = True
           pass_obj.save()

        # to check link is expired or not
        if pass_obj.link_expired:
            return render(request, 'register/link_expired.html')
        else:
            return render(request, 'register/confirm_password.html', {'form': form})

    def post(self, request, *args, **kwargs):
        key = self.kwargs['key']
        pass_obj = EmailConfirmation.objects.get(key=key)
        form = ChangePasswordForm(request.POST)

        if form.is_valid():
            newpassword = form.cleaned_data['newpassword1']
            user_obj = User.objects.get(email=pass_obj.user.email)
            user_obj.set_password(newpassword)
            user_obj.save()
            pass_obj.link_expired = True
            pass_obj.save()
            t = loader.get_template('email/password_reset_success_mail.html')

            site = Site.objects.get_current()
            link = Site.objects.get_current()
            c = Context({'user': user_obj.first_name, 'link': link, 'site': site.name})
            email = EmailMessage(user_obj.first_name+', your password was successfully reset', t.render(c), 'Ednora <no-reply@ednora.com>',
                                 (user_obj.email, ))
            email.content_subtype = 'html'
            email.send()

            return render(request, 'register/password_change_done.html')
        else:
            return render(request, self.template_name, {'form': form})


class SentVerifylink(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SentVerifylink, self).dispatch(*args, **kwargs)

    def post(self, request):
        ctx = {}
        post = request.POST.copy()
        user_id = post.get('user_id')
        user_obj = get_object_or_404(User, id=user_id)

        # Send email with activation key
        key = self.generate_hash_key()
        # generating confirmation link to be send to user using hash_key
        link = self.generate_link(key)
        previous_links = EmailConfirmation.objects.filter(user=user_obj)
        for links in previous_links:
            links.link_expired = True
            links.save()
        objct = EmailConfirmation.objects.create(user=user_obj, key=key)
        objct.save()
        t = loader.get_template('email/confirm_email.html')

        site = Site.objects.get_current()
        c = Context({'user': user_obj.first_name, 'link': link, 'site': site.domain})
        email = EmailMessage('Verify your email address', t.render(c), 'Ednora <no-reply@ednora.com>', (user_obj.email, ))
        email.content_subtype = 'html'
        email.send()
        ctx['status'] = 'success'
        ctx['done'] = True
        return HttpResponse(json.dumps(ctx), content_type='application/json')

    def generate_hash_key(self):
        hash_key = hexlify(os.urandom(15))
        return hash_key

    def generate_link(self, key):
        url = reverse('email_confirm', kwargs={'key': key})
        current_site = Site.objects.get_current()
        url = current_site.domain + url
        return url


class ProfileView(FormView):
    template_name = 'profile.html'
    form_class = UpdateProfile
    success_url = '/profile/'

    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ProfileView, self).dispatch(*args, **kwargs)

    def get_initial(self):
        """
        Returns the initial data to use for forms on this view.
        """
        initial = super(ProfileView, self).get_initial()
        initial['first_name'] = self.request.user.first_name
        initial['last_name'] = self.request.user.last_name
        initial['state'] = self.request.user.state
        initial['district'] = self.request.user.district
        initial['school'] = self.request.user.school
        initial['subject'] = self.request.user.subject.all()
        initial['grade'] = self.request.user.grade.all()
        initial['state_preference'] = self.request.user.state_preference
        initial['nation_preference'] = self.request.user.nation_preference
        user_weekly_email_obj = UserWeeklyEmailAlert.objects.filter(user=self.request.user)
        if len(user_weekly_email_obj) > 0:
            initial['email_preference'] = user_weekly_email_obj[0].is_subscribed
        return initial

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        context['email'] = self.request.user.email
        my_folders = self.request.user.get_folder.all().order_by('name')
        context['folder_obj'] = my_folders
        user_weekly_email_obj = UserWeeklyEmailAlert.objects.filter(user=self.request.user)
        user_obj = User.objects.filter(id=self.request.user.id)
        user_write_obj = User.objects.get(id=self.request.user.id)
        user_write_obj.first_login = False
        user_write_obj.save()
        context['message'] = "Successfully saved"

        if len(user_weekly_email_obj) > 0:
           context['weekly_alert'] = user_weekly_email_obj[0].is_subscribed
        else:
            user_weekly_email_obj = UserWeeklyEmailAlert()
            user_weekly_email_obj.user = self.request.user
            user_weekly_email_obj.is_subscribed = True
            user_weekly_email_obj.save()
            context['weekly_alert'] = user_weekly_email_obj.is_subscribed

        if len(user_obj)>0:
           context['district_preference'] = user_obj[0].district_preference
           context['state_preference'] = user_obj[0].state_preference
           context['nation_preference'] = user_obj[0].nation_preference
        else:
            user_obj.district_preference = False
            user_obj.state_preference = False
            user_obj.nation_preference = False
            user_obj.save()
        context['form'].fields["district"].queryset = District.objects.filter(state_id=self.request.user.state)

        return context

    def form_valid(self, form):
        user = self.request.user
        user.first_name = re.sub(' +', ' ', self.request.POST['first_name']).strip()
        user.last_name = re.sub(' +', ' ', self.request.POST['last_name']).strip()
        user.school, created = School.objects.get_or_create(name=re.sub(' +', ' ', self.request.POST['school']).strip(),
                                                            district=user.district)
        grade_list = self.request.POST.getlist('grade')
        email_prefrence = self.request.POST.getlist('email_preference')
        user_weekly_email_obj = UserWeeklyEmailAlert.objects.filter(user=self.request.user)
        if len(user_weekly_email_obj)>0:
            if len(email_prefrence) > 0:
                user_weekly_email_obj[0].is_subscribed = True
            else:
                user_weekly_email_obj[0].is_subscribed = False
        user_weekly_email_obj[0].save()
        user.grade = grade_list
        subject_list = self.request.POST.getlist('subject')
        user.subject = subject_list
        level_list = self.request.POST.getlist('level')
        user.level = level_list
        #state and nation is on By default so not updating
        # state_preference = self.request.POST.getlist('state_preference')
        # user.state_preference = state_preference
        # nation_preference = self.request.POST.getlist('nation_preference')
        # user.nation_preference = nation_preference
        user.save()
        messages.success(self.request, "Changes saved successfully")
        return HttpResponseRedirect(self.get_success_url(),'')


class PasswordResetProfile(View):
    """
    to change the password from profile page
    """

    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(PasswordResetProfile, self).dispatch(*args, **kwargs)

    def post(self, request):
        ctx = {}
        post = request.POST.copy()
        old_password = post.get('old_password')
        new_password = post.get('new_password')
        retyped_new_password = post.get('re_type_new_password')
        if request.user.check_password(old_password):
            if new_password != retyped_new_password:
                ctx['status'] = 'success'
                ctx['new_password_mismatch'] = True
                return HttpResponse(json.dumps(ctx), content_type='application/json')
            user_obj = User.objects.get(id=request.user.id)
            user_obj.set_password(new_password)
            user_obj.save()
            update_session_auth_hash(request, user_obj)

            ctx['status'] = 'success'
            ctx['reset_success'] = True
            t = loader.get_template('email/password_reset_success_mail.html')

            site = Site.objects.get_current()
            link = Site.objects.get_current()
            c = Context({'user': user_obj.first_name, 'link': link, 'site': site.name})
            email = EmailMessage(user_obj.first_name+', your password was successfully reset', t.render(c), 'Password Reset<no-reply@ednora.com>',
                                 (user_obj.email, ))
            email.content_subtype = 'html'
            email.send()
            return HttpResponse(json.dumps(ctx), content_type='application/json')
        ctx['status'] = 'success'
        ctx['old_password_mismatch'] = True
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class ChangeProfileImage(View):
    """
    to change the user image
    """

    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ChangeProfileImage, self).dispatch(*args, **kwargs)

    def post(self, request):
        ctx = {}
        user_uploded_dp = request.FILES.get('user_image')
        user_obj = User.objects.get(id=request.user.id)
        user_obj.profile_pic = user_uploded_dp
        user_obj.save()
        ctx['status'] = 'success'
        ctx['profile_image_url'] = user_obj.profile_pic.url
        ctx['html'] = render_to_string('profile_image.html', {'image': user_obj.profile_pic.url})
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class UserFeedBackView(View):
    """
    to store the feedback
    """

    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UserFeedBackView, self).dispatch(*args, **kwargs)

    def post(self, request):
        ctx = {}
        like_about_postr = request.POST.get('like_about_postr')
        like_to_see_on_postr = request.POST.get('like_to_see_on_postr')
        change_on_postr = request.POST.get('change_on_postr')
        like_to_no_see = request.POST.get('like_to_no_see')
        comments = request.POST.get('comments')
        UserFeedBack.objects.create(user=request.user, like_about_postr=like_about_postr,
                                        like_to_see_on_postr=like_to_see_on_postr,
                                        change_on_postr=change_on_postr, like_to_no_see=like_to_no_see,
                                        comments=comments)

        ctx['status'] = "success"
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class ContactUs(View):
    """
    to save the contacted person detail
    """

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ContactUs, self).dispatch(*args, **kwargs)

    def post(self, request):
        ctx = {}
        name = request.POST.get('name').strip()
        email = request.POST.get('email')
        message = request.POST.get('message').strip()
        Contact.objects.create(name=name, email=email, message=message)
        ctx['status'] = "success"
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class CommentListingView(View):

    def get(self, request):
        content_id = request.GET.get('content_id', '')
        comments = Comment.objects.filter(content_type__model='Content', object_id=content_id).order_by('-id')
        values = render_to_string("home/comments.html",  {'comments': comments})
        return HttpResponse(json.dumps({'html': values}), content_type="application/json")


class CheckUrlFileResourceView(View):
    """
    to check whether the Resource is url or file
    """

    @method_decorator(verified_required)
    def get(self, request):
        content_id = request.GET.get('content_id', '')
        content = Content.objects.get(id=content_id)
        if content.file_upload:
            is_file = True
        else:
            is_file = False
        return HttpResponse(json.dumps({'is_file': is_file}), content_type="application/json")


class UserTrackingView(View):
    """
    to track the user activity
    """

    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UserTrackingView, self).dispatch(*args, **kwargs)

    def post(self, request):
        ctx = {}
        content_id = request.POST.get('content_id','')
        media = request.POST.get('media')
        if not content_id:
            content_on = Content.objects.get(slug=request.POST.get('content_slug',''))

            if media:
                user_share_tracking_obj = UserShareTracking()
                user_share_tracking_obj.user = request.user
                user_share_tracking_obj.email = media
                user_share_tracking_obj.content = content_on
                user_share_tracking_obj.save()
        else:
            content_on = Content.objects.get(id=int(content_id))


        type = request.POST.get('type')
        UserTracking.objects.create(user=request.user, type=type, content=content_on)
        ctx['status'] = "success"
        view_count = UserTracking.objects.filter(content=content_on, type=type).count()
        if not content_id:
            ctx['id'] = user_share_tracking_obj.pk
            ctx['title'] = content_on.title
        ctx['view_count'] = view_count
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class UserTrackinglikeDeleteView(View):
    """
    to delete the like
    """

    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UserTrackinglikeDeleteView, self).dispatch(*args, **kwargs)

    def post(self, request):
        ctx = {}
        content_id = request.POST.get('content_id')
        content_on = Content.objects.get(id=int(content_id))
        type = request.POST.get('type')
        user_tracking_obj = UserTracking.objects.get(user=request.user, type=type, content=content_on)
        user_tracking_obj.delete()
        ctx['status'] = "success"
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class UserDeactivate(TemplateView):
    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UserDeactivate, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        try:
            reason = request.POST.get('reason', '')
            user = request.user
            ud = UserDeactivation()
            ud.user = user
            ud.reason_for_deactivate = reason
            user.is_active = False
            user.save()
            ud.save()
            site = Site.objects.get_current()
            t = loader.get_template('email/mail_deactivation_success.html')
            c = Context({'user': user,  'site': site.name})
            mail = EmailMessage(user.first_name+', your Ednora account has been deactivated', t.render(c),
                                'Ednora <no-reply@ednora.com>', (user.email, ))
            mail.content_subtype = 'html'
            mail.send()
        except Exception as e:print(e)
        ctx={}
        ctx['status'] =  "success"
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class UserDPDeleteView(View) :
    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UserDPDeleteView, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):

        user = request.user
        user.profile_pic.delete(False)
        user.save()
        request.user = user
        image_src = user.get_user_dp()
        ctx={}
        ctx['status'] =  "success"
        ctx['image'] = image_src
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class UpdateWeeklyEmailAlertView(View):
    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UpdateWeeklyEmailAlertView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        user_weekly_email_alert_obj = UserWeeklyEmailAlert.objects.get(user=request.user)
        status = request.POST.get('subscription')
        if status == 'true':
            user_weekly_email_alert_obj.is_subscribed = True
        else:
             user_weekly_email_alert_obj.is_subscribed = False
        user_weekly_email_alert_obj.save()
        ctx = {}
        ctx['status'] = 'success'
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class UpdateDistrictPreference(View):
    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UpdateDistrictPreference, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        user_obj = User.objects.get(id=request.user.id)
        status = request.POST.get('subscription')
        if status =='true':
            user_obj.district_preference = True
        else:
             user_obj.district_preference = False
             user_obj.state_preference = False
             user_obj.nation_preference = False
        user_obj.save()
        ctx ={}
        ctx['status'] = 'success'
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class UpdateStatePreference(View):
    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UpdateStatePreference, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        user_obj = User.objects.get(id=request.user.id)
        status = request.POST.get('subscription')
        if status =='true':
            user_obj.state_preference = True
            user_obj.district_preference = True
        else:
            user_obj.state_preference = False
            user_obj.nation_preference = False
        user_obj.save()
        ctx ={}
        ctx['status'] = 'success'
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class UpdateNationPreference(View):
    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UpdateNationPreference, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        user_obj = User.objects.get(id=request.user.id)
        status = request.POST.get('subscription')
        if status =='true':
            user_obj.nation_preference = True
            user_obj.state_preference = True
            user_obj.district_preference = True
        else:
             user_obj.nation_preference = False
        user_obj.save()
        ctx ={}
        ctx['status'] = 'success'
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class UserFacebookInvite(View):
    """
    to track the user activity
    """

    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UserFacebookInvite, self).dispatch(*args, **kwargs)

    def post(self, request):
        ctx = {}
        user_invite = UserInvites()
        user_invite.user = request.user
        user_invite.email = 'facebook'
        user_invite.channel = 'facebook'

        user_invite.save()
        ctx['status'] = "success"
        ctx['id'] = user_invite.pk
        ctx['slug'] = request.user.slug
        return HttpResponse(json.dumps(ctx), content_type='application/json')


def page_not_found(request):
    """
    Page not found Error 404
    """
    response = render_to_response('404.html', context_instance=RequestContext(request))
    response.status_code = 404
    return response


def server_error(request):
    """
    Server Error 500
    """
    response = render_to_response('500.html', context_instance=RequestContext(request))
    response.status_code = 500
    return response


class DistrictPopulateView(View):

    http_method_names = ('get',)

    def get(self, request):
        state = request.GET.get('state')
        dist_list_obj = District.objects.filter(state__id=state).order_by('name')
        dist_list = []
        for obj in dist_list_obj:
            dist_list.append({'name': obj.name, 'id': obj.id})

        return JsonResponse(dist_list, safe=False)


class SchoolPopulateView(View):
    http_method_names = ('get',)

    def get(self,request):
        # district = self.request.POST.get('state')
        district = request.GET.get('district')
        query = request.GET.get('query')
        state = request.GET.get('state')
        on = request.GET.get('on', None)
        if on == 'signup' or 'profile':
            school_list = School.objects.filter(district__id=district, name__icontains=query)
        else:
            school_list = School.objects.filter(district__name=district, name__icontains=query)
        l=[]
        for obj in school_list:
            l.append(obj.name)
        return JsonResponse(l, safe=False)


class DownloadFile(View):
    http_method_names = ('get',)

    def get(self,request, *args, **kwargs):
        id = kwargs['id']
        try:
            content = Content.objects.get(pk = id)
        except Content.DoesNotExist:
            raise Http404
        '''
        record download activity
        '''
        ut = UserTracking()
        ut.user = request.user
        ut.type = 'download'
        ut.content = content
        ut.save()

        file_name = str(content.file_upload).rsplit('/', 1)[-1]
        file_type = file_name.rsplit('.', 1)[-1]
        path_to_file =settings.MEDIA_ROOT + str(content.file_upload)
        content_type = magic.from_file(path_to_file, mime=True)
        print(magic.from_file(path_to_file, mime=True), "********************************************************")
        response = HttpResponse(content.file_upload,content_type= content_type)
        response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(file_name)
        response['X-Sendfile'] = smart_str(path_to_file)
        return response


class DeletePin(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(DeletePin, self).dispatch(*args, **kwargs)

    def post(self, request):
        id = request.POST.get('id')
        folder_name = request.POST.get('folder')
        try:
            pin = Pin.objects.get(content_id=id)
            content = Content.objects.get(pk=id)
            content.delete()
            ctx = {}
            ctx['status'] = 'success'
        except:
            folder_obj = Folder.objects.get(name=folder_name, user=request.user)
            content = Pin.objects.get(content_id=id, folder=folder_obj)
            content_on = Content.objects.get(id=id)
            UserTracking.objects.create(user=request.user, type='dff', content=content_on)
            content.delete()
            ctx = {}
            ctx['status'] = 'success'
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class DeletePinFromFolder(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(DeletePinFromFolder, self).dispatch(*args, **kwargs)

    def post(self, request,):
        id = request.POST.get('id')
        folder = request.POST.get('folder')
        folder_id = request.POST.get('folder_id')
        content = Pin.objects.get(content_id=id, folder__id=folder_id)
        content_on = Content.objects.get(id=id)
        UserTracking.objects.create(user=request.user, type='dff', content=content_on)
        content.delete()
        ctx ={}
        ctx['status'] = 'success'
        return HttpResponse(json.dumps(ctx), content_type='application/json') # Redirect after POST


class CheckContent(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CheckContent, self).dispatch(*args, **kwargs)

    def post(self, request,):
        id = request.POST.get('id')
        try:
            pin = Pin.objects.get(content_id=id)
            ctx ={}
            ctx['status'] = 'success'
        except:
            ctx ={}
            ctx['status'] = 'fail'
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class ContentEditingView(View):

    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(ContentEditingView, self).dispatch(*args, **kwargs)

    def post(self, request):
        ctx = {}
        id = request.POST.get('content_id', '')
        content = Content.objects.get(id=id)
        if content.file_upload:
            c = str(content.file_upload)
            c = c.rsplit('/', 1)[1]
            ctx['source'] = c
            ctx['type'] = 'file'
        else:
            ctx['source'] = content.base_url
            ctx['type'] = 'url'
        ctx['title'] = content.title
        ctx['text'] = content.text
        ctx['base_url'] = content.base_url
        grade = content.grade.all().values('grade')
        grade_list = [d['grade'] for d in grade]
        ctx['grade'] = list(set(grade_list))
        subject =content.subject.all().values('subject')
        subject = [d['subject'] for d in subject]
        subject_id = []
        for each in subject:
            sub_id = Subject.objects.get(subject=each)
            subject_id.append(sub_id.id)
        # ctx['subject'] = list(set(subject))
        ctx['subject'] = subject_id
        ctx['objective'] = list(content.objective.all().values_list('name', flat=True))
        ctx['tags'] = list(content.tags.all().values_list('name', flat=True))
        ctx['learning_objective_code'] = list(
            content.content_learning_objective_code.all().values_list('code', flat=True))
        ctx['content_type'] = content.content_type.name
        # ctx['media_type'] = content.media_type.name
        if content.image:
            ctx['image'] = content.image.url
            ctx['thumbnail'] = content.get_image('380x360')
        level = content.content_level.all().values('name')
        level_list = [d['name'] for d in level]
        level_id = []
        for each in level_list:
            lvl_id = Level.objects.get(name=each)
            level_id.append(lvl_id.id)
        # ctx['level'] = list(set(level_list))
        ctx['level'] = level_id
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class SaveContentView(DetailView):
    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SaveContentView, self).dispatch(*args, **kwargs)

    def post(self, request):
        ctx = {}
        cont_id = request.POST.get('content_id')
        content = Content.objects.get(id=cont_id)
        content.title = request.POST.get('content_title')
        content.text = request.POST.get('content_text')

        value = request.POST.get('grade')
        choices = value.replace("'", "")
        choices = choices.replace(",", " ")
        choice_list = [s for s in choices.split()]
        grade = Grade.objects.filter(grade__in=choice_list)
        excludegrade = Grade.objects.exclude(grade__in=choice_list)
        content.grade.remove(*excludegrade)
        content.grade.add(*grade)
        subject = request.POST.get('subject')
        if subject:
            subject_choices = subject.replace("'", "")
            subject_choices = subject_choices.split(',')
            sub_list = []
            for sub in subject_choices:
                sub_obj = Subject.objects.get(id=sub)
                sub_list.append(sub_obj.id)
            subject = Subject.objects.filter(id__in=sub_list)
            excludesubject = Subject.objects.exclude(id__in=sub_list)
            content.subject.remove(*excludesubject)
            content.subject.add(*subject)
            try:
                content_type_obj = ContentType.objects.get(name=subject.subject)
            except:
                content_type_obj = ContentType.objects.get(name='Other')
            content.content_type = content_type_obj

        # level = request.POST.get('level')
        # if level:
        #     level_choices = level.replace("'", "")
        #     level_choices = level_choices.split(',')
        #     lvl_list = []
        #     for lvl in level_choices:
        #         lvl_obj = Level.objects.get(id=lvl)
        #         lvl_list.append(lvl_obj.id)
        #     level = Level.objects.filter(id__in=lvl_list)
        #     excludelevel = Level.objects.exclude(id__in=lvl_list)
        #     content.content_level.remove(*excludelevel)
        #     content.content_level.add(*level)
        # objectives = request.POST.get('learning_objectives')
        # if objectives:
        #     objective_choices = objectives.split(',')
        #     objective_choices = list(set(objective_choices))
        #     objective_list = []
        #     for objective in objective_choices:
        #         if not objective == '':
        #             objective_obj, created = LearningObjective.objects.get_or_create(name=objective.strip())
        #             objective_list.append(objective_obj.id)
        #     objective = LearningObjective.objects.filter(id__in=objective_list)
        #     excludeobjective = LearningObjective.objects.exclude(id__in=objective_list)
        #     content.objective.remove(*excludeobjective)
        #     content.objective.add(*objective)

        # if request.POST.get('objectives_code'):
        #     codes = request.POST.get('objectives_code')
        #     code_choices = codes.split(',')
        #     code_choices = list(set(code_choices))
        #     code_list = []
        #     for code in code_choices:
        #         if not code == '':
        #             code_obj, created = LearningObjectiveCode.objects.get_or_create(code=code.strip())
        #             code_list.append(code_obj.id)
        #     code = LearningObjectiveCode.objects.filter(id__in=code_list)
        #     excludecode = LearningObjectiveCode.objects.exclude(id__in=code_list)
        #     content.content_learning_objective_code.remove(*excludecode)
        #     content.content_learning_objective_code.add(*code)
        # else:
        # content.content_learning_objective_code.clear()

        tags = request.POST.get('tags')

        if tags:
            tag_choices = tags.split(',')
            tag_choices = list(set(tag_choices))
            tag_list = []
            for tag in tag_choices:
                if not tag == '':
                    tag_obj, created = Tag.objects.get_or_create(name=tag.strip())
                    tag_list.append(tag_obj.id)
            tag_to_include = Tag.objects.filter(id__in=tag_list)
            exclude_tag_set = Tag.objects.exclude(id__in=tag_list)
            content.tags.remove(*exclude_tag_set)
            content.tags.add(*tag_to_include)
        else:
            content.tags.clear()
        # if request.POST.get('content_type'):
        #     type = ContentType.objects.get(name=request.POST.get('content_type'))
        #     content.content_type = type
        # if request.POST.get('media_type'):
        #     media = MediaType.objects.filter(name=request.POST.get('media_type'))
        #     for media in media:
        #         content.media_type = media
        if request.POST.get('folder'):
            current_folder = request.POST.get('current_folder')
            folder_name = request.POST.get('folder')
            try:
                folder = Folder.objects.get(user=request.user, name=folder_name)
                old_folder = Folder.objects.get(user=request.user, name=current_folder)
                old_pin = Pin.objects.get(content=content, folder=old_folder)
                old_pin.delete()
                Pin.objects.get_or_create(folder=folder, content=content)
            except:
                folder = Folder.objects.get(user=request.user, name=folder_name)
                pin = Pin.objects.get_or_create(folder=folder, content=content)
        content.save()
        ctx['pass'] = True
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class LoginModel(View):
    """login user using login-model"""
    def post(self, request, *args, **kwargs):
        context = {}
        email = request.POST.get("email", "")
        email = email.strip()
        password = request.POST['password']
        password = password.strip()
        data = {'email': email, 'password': password}
        login_form = LoginForm(data)
        flag_is_user_active =False
        form = SignupForm()
        if login_form.is_valid():
            next = request.POST.get("next", "")
            user = authenticate(username=email, password=password)
            user.first_login = False
            user.save()
            if user is not None:
                if not user.is_active:
                    flag_is_user_active = True
                    user.is_active = True
                    user.save()
                    # track user reactivation
                    ud = UserDeactivation()
                    ud.user = user
                    ud.type = '2'
                    ud.save()
                    request.user = user
                if user.is_active:
                    login(request, user)
                    context['status'] = True
                    context['next'] = next
                    request.session['is_user_deactivated'] = flag_is_user_active

                    '''user device  tracking'''
                    ctx = dict()
                    ctx['os'] = self.request.user_agent.os.family
                    ctx['device'] = self.request.user_agent.device.family
                    ctx['browser'] = self.request.user_agent.browser.family
                    ctx['ip'] = get_client_ip(self.request)
                    ctx['user'] = self.request.user
                    UserDeviceTracking.objects.create(**ctx)
        else:
            context['status'] = 'Invalid username or password'
        return HttpResponse(json.dumps(context), content_type='application/json')
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate
import base64
from .models import User
from applications.accounts.models import Country, Actor, School, Grade,State,District,Subject
from applications.pin.models import Level
import re
from email_split import email_split


class SignupForm(forms.ModelForm):
    """
    form for user signup
    """
    first_name = forms.CharField(error_messages={'required': 'Required field'},label=_("First Name"), required=True, )
    last_name = forms.CharField(error_messages={'required': 'Required field'},label=_("Last Name"), required=True)
    email = forms.EmailField(error_messages={'required': 'Required field'},label=_("Email"), required=True)
    password = forms.CharField(error_messages={'required': 'Required field'},label=_("Password"), widget=forms.PasswordInput, required=True)
    password1 = forms.CharField(error_messages={'required': 'Required field'},label=_("Re-Enter Password"), widget=forms.PasswordInput, required=True)
    state = forms.ModelChoiceField(error_messages={'required': 'Required field'},label=_("State"), widget=forms.Select(), queryset=State.objects.order_by('name'), empty_label="Select Country*", required=True)
    district = forms.ModelChoiceField(error_messages={'required': 'Required field'},label=_("District"), widget=forms.Select(), queryset=District.objects.filter(), empty_label="Select State*", required=True)
    skool = forms.CharField(error_messages={'required': 'Required field'},label=_("School"), required=True)
    grade = forms.ModelMultipleChoiceField(error_messages={'required': 'Required field'},label=_("Grade"),queryset=Grade.objects.all(), widget=forms.SelectMultiple())
    subject = forms.ModelMultipleChoiceField(error_messages={'required': 'Required field'},label=_("Subject"), widget=forms.SelectMultiple(), queryset=Subject.objects.order_by('subject'))
    termsandcondition = forms.BooleanField(error_messages={'required': 'Please confirm that you have read and agree to our Terms and Policies.'})
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email',
                  'password', 'password1', 'state','district', 'skool',
                  'grade',
                  'subject')

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['placeholder'] = _('First Name*')
        self.fields['first_name'].widget.attrs['tabindex'] = _('7')
        self.fields['first_name'].widget.attrs['class'] = _('form-control')
        self.fields['last_name'].widget.attrs['placeholder'] = _('Last Name*')
        self.fields['last_name'].widget.attrs['tabindex'] = _('8')
        self.fields['last_name'].widget.attrs['class'] = _('form-control')
        self.fields['email'].widget.attrs['placeholder'] = _('Work Email*')
        self.fields['email'].widget.attrs['tabindex'] = _('4')
        self.fields['email'].widget.attrs['class'] = _('form-control')
        self.fields['password'].widget.attrs['placeholder'] = _('Password*')
        self.fields['password'].widget.attrs['tabindex'] = _('5')
        self.fields['password'].widget.attrs['class'] = _('form-control')
        self.fields['password'].widget.attrs['placeholder'] = _('Password*')
        self.fields['password'].widget.attrs['data-toggle'] = _('popover')
        self.fields['password'].widget.attrs['title'] = _('<div class = "test">   '
                                                          '<br> <ul><li>Password must be 8 or more characters</li> <li>Must contain uppercase character</li>'
                                                          '<li>Must contain lowercase character</li><li>Must contain one numeric or special character</li></ul>'
                                                          ' </div>')
        self.fields['password'].widget.attrs['data-content'] = _(' ')
        self.fields['password'].widget.attrs['data-html'] = _('true')
        self.fields['password1'].widget.attrs['placeholder'] = _('Re-Enter Password*')
        self.fields['password1'].widget.attrs['tabindex'] = _('6')
        self.fields['password1'].widget.attrs['class'] = _('form-control')

        self.fields['state'].widget.attrs['placeholder'] = _('Select Country*')
        self.fields['state'].widget.attrs['id'] = _('id_state')
        self.fields['state'].widget.attrs['class'] = _('sign_up_drop')
        self.fields['state'].widget.attrs['tabindex'] = _('9')

        self.fields['district'].widget.attrs['placeholder'] = _('Select State*')
        self.fields['district'].widget.attrs['id'] = _('id_district')
        self.fields['district'].widget.attrs['class'] = _('sign_up_drop')
        self.fields['district'].widget.attrs['tabindex'] = _('9')

        self.fields['skool'].widget.attrs['placeholder'] = _('Enter City*')
        self.fields['skool'].widget.attrs['class'] = _('form-control')
        self.fields['skool'].widget.attrs['tabindex '] = _('10')

        self.fields['grade'].widget.attrs['data-placeholder'] = _('Select Grade(s) of Interest*')
        self.fields['grade'].widget.attrs['class'] = _('sign_up_drop, multiselect')
        self.fields['grade'].widget.attrs['tabindex '] = _('11')

        self.fields['subject'].widget.attrs['class'] = _('sign_up_drop, multiselect')
        self.fields['subject'].widget.attrs['data-placeholder'] = _('Select Topic(s) of Interest*')
        self.fields['subject'].widget.attrs['tabindex'] = _('12')
        self.fields['termsandcondition'].widget.attrs['tabindex'] = 13

    def clean_password(self):
        password = self.cleaned_data["password"]
        regex1 = re.compile('^(?=.*[a-z])(?=.*[A-Z])(?=.*?[0-9]|.*?[#?!@$%^&*-])[a-zA-Z\d]{8,}')
        regex2 = re.compile('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$')
        if (regex1.match(password) ==None)  and  (regex2.match(password) == None):
            raise forms.ValidationError("Weak password")
        return password

    def clean_email(self):
        email = self.cleaned_data["email"]
        email = str(email).strip()
        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            return email
        raise forms.ValidationError(_("This email ID already exists"))

    def clean_first_name(self):
        first_name = self.cleaned_data["first_name"]
        first_name = first_name.strip()
        if first_name:
            return first_name
        raise forms.ValidationError(_("Required"))

    def clean_last_name(self):
        last_name = self.cleaned_data["last_name"]
        last_name = last_name.strip()
        if last_name:
            return last_name
        raise forms.ValidationError(_("Required"))

    def clean_password1(self):
        password = self.cleaned_data.get("password", "")
        password1 = self.cleaned_data["password1"]
        password1 = password1.strip()
        password = password.strip()
        print(password)
        l = len(password)
        if l != 0:
            if password != password1:
                raise forms.ValidationError(_("Passwords don't match"))
            return password1

    def clean_state(self):
        state = self.cleaned_data["state"]
        if state:
            return state
        raise forms.ValidationError(_("Required"))

    def clean_district(self):
        district = self.cleaned_data["district"]
        if district:
            return district
        raise forms.ValidationError(_("Required"))

    def clean_skool(self):
        school = self.cleaned_data["skool"]
        school = school.strip()
        if school:
            if len(str(school))>200:
                error = 'Ensure this value has at most 200 characters (it has %s).' % (len(str(school)))
                raise forms.ValidationError(error)
            return school
        raise forms.ValidationError(_("Required"))

    def clean_grade(self):
        grade = self.cleaned_data["grade"]
        if grade:
            return grade
        raise forms.ValidationError(_("Required"))

    def clean_subject(self):
        subject = self.cleaned_data["subject"]
        if subject:
            return subject
        raise forms.ValidationError(_("Required"))

    def clean(self):

        district = self.cleaned_data.get("district", None)
        if not district:
            raise forms.ValidationError("")
        email = self.cleaned_data.get("email",None)
        if not email:
            raise forms.ValidationError("")
        # email = email_split(email)
        # dist_obj = District.objects.get(id=district.id)
        # dist_domain = dist_obj.email_domail
        # if(email.domain != dist_domain):
        #      raise forms.ValidationError("Please enter a valid work email for your school. If you entered valid work email and still see this error then please contact us at support@ednora.com.")
        return self.cleaned_data


class SignupFormTwo(forms.ModelForm):
    """
    form for user signup part two
    """
    name = forms.CharField(label=_("Name"), required=True )

    class Meta:
        model = Actor
        fields = ('name',)

    def __init__(self, *args, **kwargs):
        super(SignupFormTwo, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['placeholder'] = _('Comma separated areas of interest (eg: IoT, FPGA, STA)*')


class LoginForm(forms.Form):
    """
    Form for using login
    """
    email = forms.EmailField(label=_("Email"), required=True)
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput, required=True)

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['placeholder'] = _('Email*')
        self.fields['email'].widget.attrs['class'] = _('form-control')
        self.fields['email'].widget.attrs['id'] = _('exampleInputEmail2')
        self.fields['email'].widget.attrs['tabindex'] = _('1')
        self.fields['password'].widget.attrs['tabindex'] = _('2')
        self.fields['password'].widget.attrs['placeholder'] = _('Password*')
        self.fields['password'].widget.attrs['class'] = _('form-control')
        self.fields['password'].widget.attrs['id'] = _('exampleInputEmail2')

    def clean(self):
        email = self.cleaned_data.get('email')
        email = str(email).strip()
        password = self.cleaned_data.get('password')
        password = str(password).strip()
        user = authenticate(username=email, password=password)
        if not user:
            raise forms.ValidationError(_("Invalid username or password"))
        return self.cleaned_data


class ChangePasswordForm(forms.Form):
    """
    form for change password
    """
    newpassword1 = forms.CharField(error_messages={'required': 'Required field'}, max_length=20, widget=forms.PasswordInput, required=True)
    newpassword2 = forms.CharField(error_messages={'required': 'Required field'},max_length=20, widget=forms.PasswordInput, required=True)

    def __init__(self, *args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.fields['newpassword1'].widget.attrs['placeholder'] = _('New Password*')
        self.fields['newpassword1'].widget.attrs['class'] = _('form-control')
        self.fields['newpassword1'].widget.attrs['id'] = _('confirm_passwod_id1')
        self.fields['newpassword1'].widget.attrs['data-toggle'] = _('popover')
        self.fields['newpassword1'].widget.attrs['title'] = _('<div class = "test">'
                                                          '<br> <ul><li>Password must be 8 or more characters</li> <li>Must contain uppercase character</li>'
                                                          '<li>Must contain lowercase character</li><li>Must contain one numeric or special character</li></ul>'
                                                          ' </div>')
        self.fields['newpassword1'].widget.attrs['data-content'] = _('')
        self.fields['newpassword1'].widget.attrs['data-html'] = _('true')
        self.fields['newpassword2'].widget.attrs['placeholder'] = _('Confirm New Password*')
        self.fields['newpassword2'].widget.attrs['class'] = _('form-control')
        self.fields['newpassword2'].widget.attrs['id'] = _('confirm_passwod_id2')

    def clean(self):
        """
        to check the password length(8)
        :return:
        """
        password1 = self.cleaned_data.get('newpassword1')
        if password1:
            regex1 = re.compile('^(?=.*[a-z])(?=.*[A-Z])(?=.*?[0-9]|.*?[#?!@$%^&*-])[a-zA-Z\d]{8,}')
            regex2 = re.compile('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$')
            if (regex1.match(password1) ==None)  and  (regex2.match(password1) == None):
                raise forms.ValidationError("Weak password")

            if 'newpassword1' in self.cleaned_data and 'newpassword2' in self.cleaned_data:
                if self.cleaned_data['newpassword1'] != self.cleaned_data['newpassword2']:
                    raise forms.ValidationError(_("Passwords don't match."))
            return self.cleaned_data


class UpdateProfile(forms.ModelForm):
    first_name = forms.CharField(error_messages={'required': 'Required field'}, required=True)
    last_name = forms.CharField(error_messages={'required': 'Required field'}, required=True)
    state = forms.ModelChoiceField(error_messages={'required': 'Required field'}, label=_("State"), widget=forms.Select(), queryset=State.objects.order_by('name'), empty_label="Select Country*", required=True)
    district = forms.ModelChoiceField(error_messages={'required': 'Required field'},label=_("District"), widget=forms.Select(), queryset=District.objects.filter(), empty_label="Select State*", required=True)
    school = forms.CharField(error_messages={'required': 'Required field'}, required=True)
    grade = forms.ModelMultipleChoiceField(error_messages={'required': 'Required field'}, label=_("Grade"),
                                           queryset=Grade.objects.filter(), widget=forms.SelectMultiple())
    subject = forms.ModelMultipleChoiceField(error_messages={'required': 'Required field'},
                                             label=_("Subject"), widget=forms.SelectMultiple(),
                                             queryset=Subject.objects.order_by('subject'))
    level = forms.ModelMultipleChoiceField(label=_("Level"), widget=forms.SelectMultiple(),
                                           required=False, queryset=Level.objects.all())
    district_preference = forms.BooleanField(required=False, initial=True)
    state_preference = forms.BooleanField(required=False,)
    nation_preference = forms.BooleanField(required=False,)
    email_preference = forms.BooleanField(required=False,  initial=True)

    def __init__(self, *args, **kwargs):
        super(UpdateProfile, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control sign_up_select'
        self.fields['grade'].widget.attrs['class'] = _('form-control sign_up_select sign_up_drop multiselect')
        self.fields['subject'].widget.attrs['class'] = _('form-control sign_up_select sign_up_drop multiselect')
        self.fields['grade'].widget.attrs['id'] = _('id_grade')
        self.fields['grade'].widget.attrs['data-placeholder'] = _('Select Grade(s) of Interest*')
        self.fields['subject'].widget.attrs['data-placeholder'] = _('Select Topic(s) of Interest*')
        self.fields['subject'].widget.attrs['id'] = _('id_subject')
        self.fields['level'].widget.attrs['id'] = _('id_level')
        self.fields['district_preference'].widget.attrs['class'] = 'switch-input'
        # self.fields['district_preference'].widget.attrs['disabled'] = 'true'
        self.fields['state_preference'].widget.attrs['class'] = 'switch-input'
        self.fields['nation_preference'].widget.attrs['class'] = 'switch-input'
        self.fields['email_preference'].widget.attrs['class'] = 'switch-input'
        self.fields['school'].widget.attrs['placeholder'] = _('Enter City*')

    def clean_state(self):
        return State.objects.get(name=self.cleaned_data['state'])

    def clean_email(self):
        email = self.cleaned_data.get('email')

        if email and User.objects.filter(email=email).count() > 1:
            raise forms.ValidationError('This email address is already in use. Please supply a different email address')
        return email

    def clean_school(self):
        school = self.cleaned_data.get('school')
        if len(str(school))>200:
            error = 'Ensure this value has at most 200 characters (it has %s).' % (len(str(school)))
            raise forms.ValidationError(error)

    class Meta:
        model = User
        fields = ('first_name', 'last_name')

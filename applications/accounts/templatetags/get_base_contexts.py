from django import template

from applications.accounts.models import Grade, Subject, State, District
from applications.pin.models import Level, LearningObjective, LearningObjectiveCode, Tag, MediaType, ContentType

register = template.Library()


@register.assignment_tag()
def get_grades():
    return Grade.objects.all()

@register.assignment_tag()
def get_subjects():
    return Subject.objects.order_by('subject')

@register.assignment_tag()
def get_states():
    return State.objects.order_by('name')

@register.assignment_tag()
def get_districts():
    return District.objects.order_by('name')

@register.assignment_tag()
def get_levels():
    return Level.objects.order_by('name')

@register.assignment_tag()
def get_learning_objectives():
    return LearningObjective.objects.order_by('name')

@register.assignment_tag()
def get_learning_objective_codes():
    return LearningObjectiveCode.objects.order_by('code')

@register.assignment_tag()
def get_tags():
    return Tag.objects.order_by('name')

@register.assignment_tag()
def get_resource_type():
    return ContentType.objects.order_by('name')

@register.assignment_tag()
def get_media_type():
    return MediaType.objects.order_by('name')

@register.assignment_tag()
def get_user_folders(user, authenticated):
    if authenticated:
        return user.get_folder.all().order_by('name')

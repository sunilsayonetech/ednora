from django import template

register = template.Library()


@register.simple_tag
def get_user_full_name(user):
    return "%s %s" % (user.first_name, user.last_name)
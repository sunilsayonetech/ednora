from django import template
from django.conf import settings

register = template.Library()


@register.simple_tag
def get_analytics_token(user):
    return "%s" % (settings.TOKEN)

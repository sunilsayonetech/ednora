from django.conf.urls import url, include, patterns


from applications.accounts import views as accounts_view
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'password-in-profile', login_required(accounts_view.PasswordResetProfile.as_view()), name="password_in_profile"),
    url(r'change-user-dp', login_required(accounts_view.ChangeProfileImage.as_view()), name="change_user_dp"),
    url(r'user-feedback', login_required(accounts_view.UserFeedBackView.as_view()), name="user_feedback"),
    url(r'contact-us', accounts_view.ContactUs.as_view(), name="contact-us"),
    url(r'user-tracking', login_required(accounts_view.UserTrackingView.as_view()), name="user_tracking"),
    url(r'edit-content', login_required(accounts_view.ContentEditingView.as_view()), name="content-editing"),
    url(r'save-content', login_required(accounts_view.SaveContentView.as_view()), name="save-content"),
    #url(r'delete-like', login_required(accounts_view.UserTrackinglikeDeleteView.as_view()), name="delete")
    url(r'comment-listing', login_required(accounts_view.CommentListingView.as_view()), name="comment_listing"),
    url(r'check-url-file', login_required(accounts_view.CheckUrlFileResourceView.as_view()), name="check_url_file"),
    ]


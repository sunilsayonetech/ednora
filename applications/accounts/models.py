from django.db import models
from django.db.models import signals
from django.contrib.sites.models import Site
from django.template import loader, Context
from django.core.mail import EmailMessage
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.fields import AutoSlugField

from ckeditor.fields import RichTextField

from applications.pin.models import Content, Tag


class Actor(models.Model):
    """
    modal for actors
    """
    name = models.CharField(verbose_name=_('Name'), max_length=255, null=True, blank=True, unique=True)

    class Meta:
        verbose_name = _('Actor')
        verbose_name_plural = _('Actors')

    def __str__(self):
        return self.name


class Country(models.Model):
    """
    modal for country name
    """
    name = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')


class ReviewsInfluence(models.Model):
    """
    Saving reviews influence
    """
    text = models.CharField(verbose_name=_('Text'), max_length=300, blank=True, null=True)
    valid = models.BooleanField(default=True)

    class Meta:
        verbose_name = _('Review influence')
        verbose_name_plural = _('Reviews influence')

    def __str__(self):
        return self.text


class UserFeedBack(models.Model):
    user = models.ForeignKey('accounts.User', verbose_name=_('User'))
    like_about_postr = models.CharField(_(' Like About Postr'), max_length=300, null=True, blank=True)
    like_to_see_on_postr = models.CharField(_(' Like To See Postr'), max_length=300, null=True, blank=True)
    change_on_postr = models.CharField(_(' Change On Postr'), max_length=300, null=True, blank=True)
    like_to_no_see = models.CharField(_(' Like to No See'), max_length=300, null=True, blank=True)
    comments = models.TextField(_('comments'), null=True, blank=True)

    class Meta:
        verbose_name = _('Feed Back')
        verbose_name_plural = _('Feed Backs')

    def __str__(self):
        return self.user.email


class Contact(models.Model):
    name = models.CharField(max_length=40)
    email = models.EmailField(max_length=50)
    message = models.TextField()
    date_created = models.DateTimeField("Created Date", auto_now_add=True)

    class Meta:
        verbose_name = _('Contact Us')
        verbose_name_plural = _('Contact Us')

    def __str__(self):
        return self.name


class UserTracking(models.Model):
    TYPES = (
        ('cte', 'ClickToExpand'),
        ('cfu', 'ClickToFollowURL'),
        ('like', 'Like'),
        ('cts', 'ClickToShare'),
        ('pick', 'Pick'),
        ('re-pick', 'Re-Pick'),
        ('unlike', 'Unlike'),
        ('flag', 'Flag'),
        ('unflag', 'Unflag'),
        ('download', 'Download'),
        ('dff', 'DeleteFromFolder')
    )
    user = models.ForeignKey('accounts.User', verbose_name=_('User'))
    type = models.CharField(max_length=20, choices=TYPES, default='cte')
    content = models.ForeignKey(Content, verbose_name=_('Content'), related_name="get_user_tracking")
    tags = models.ManyToManyField(Tag, null=True, blank=True)
    created_time = models.DateTimeField("Created Date", auto_now_add=True)

    class Meta:
        verbose_name = _('User Tracking')
        verbose_name_plural = _('User Tracking')

    def __str__(self):
        return self.content.title


class UserInvites(models.Model):
    user = models.ForeignKey('accounts.User', verbose_name=_('User'))
    email = models.CharField(verbose_name=_('Source'), max_length=50)
    created_time = models.DateTimeField("Created Date", auto_now_add=True)
    is_invite_accepted = models.BooleanField(default=False, verbose_name=_('Invite Accepted'))
    channel = models.CharField(verbose_name=_('channel'), max_length=100, default='gmail')

    class Meta:
        verbose_name = _('User Invites')
        verbose_name_plural = _('User Invites')

    def __str__(self):
        return self.user.username


class UserDeactivation(models.Model):
    TYPES = (
        ('1', 'Deactivation'),
        ('2', 'Reactivation'),

    )
    user = models.ForeignKey('accounts.User', verbose_name=_('User'))
    type = models.CharField(choices=TYPES, max_length=20, default='1')
    reason_for_deactivate = models.TextField(_('Reason for Deactivation'), null=True, blank=True)
    deactivated_time = models.DateTimeField("Created Date", auto_now_add=True)

    class Meta:
        verbose_name = _('User Deactivation Tracking')
        verbose_name_plural = _('User Deactivation')

    def __str__(self):
        return self.user.username


class UserShareTracking(models.Model):
    user = models.ForeignKey('accounts.User', verbose_name=_('User'))
    email = models.CharField(_('Share source'), max_length=300, null=True, blank=True)
    created_time = models.DateTimeField("Created Date", auto_now_add=True)
    content = models.ForeignKey('pin.Content', verbose_name=_('Content'))
    is_visited = models.BooleanField(default=False, verbose_name=_('Is Visited'))

    class Meta:
        verbose_name = _('User Share Tracking')
        verbose_name_plural = _('User Share Tracking')

    def __str__(self):
        return self.user.username


class UserDeviceTracking(models.Model):
    user = models.ForeignKey('accounts.User',verbose_name=_('User'))
    device = models.CharField(_('Device'), max_length=300, null=True, blank=True)
    browser = models.CharField(_('Browser'), max_length=300, null=True, blank=True)
    os = models.CharField(_('Os'), max_length=300, null=True, blank=True)
    ip = models.CharField(_('IP'), max_length=300, null=True, blank=True)
    created_time = models.DateTimeField("Created Date", auto_now_add=True, help_text=_('**-UTC TIME-**'))

    class Meta:
        verbose_name = _('User Device Tracking')
        verbose_name_plural = _('User Device Tracking')

    def __str__(self):
        return self.user.username


class UserSearchTracking(models.Model):
    user = models.ForeignKey('accounts.User', verbose_name=_('User'))
    search_keyword = models.CharField(_('Keyword'), max_length=300, null=True, blank=True)
    created_time = models.DateTimeField("Created Date", auto_now_add=True)

    class Meta:
        verbose_name = _('User Search Keyword Tracking')
        verbose_name_plural = _('User Search Keyword Tracking')

    def __str__(self):
        return self.user.username


class UserFilterTracking(models.Model):
    user = models.ForeignKey('accounts.User', verbose_name=_('User'))
    content_filter = models.CharField(_('Filter Params'), max_length=300, null=True, blank=True)
    count = models.IntegerField(_('Count'), null=True, blank=True)
    created_time = models.DateTimeField("Created Date", auto_now_add=True)

    class Meta:
        verbose_name = _('User Filter Tracking')
        verbose_name_plural = _('User Filter Tracking')

    def __str__(self):
        return self.user.username


class UserFilterStat(models.Model):
    content_filter = models.CharField(_('Content Filter'), max_length=300, null=True, blank=True)
    count = models.IntegerField(_('Count'), null=True, blank=True)
    created_time = models.DateTimeField("Created Date", auto_now_add=True)

    class Meta:
        verbose_name = _('User Filter Statics')
        verbose_name_plural = _('User Filter Statics')

    def __str__(self):
        return self.content_filter


class UserContentUploadTracking(models.Model):
    upload_using_popup = models.IntegerField(_(' Upload Using Popup'), null=True, blank=True)
    upload_using_button = models.IntegerField(_('UploadUsingButon'), null=True, blank=True)
    created_time = models.DateTimeField("Created Date", auto_now_add=True)

    class Meta:
        verbose_name = _('User Upload Statics')
        verbose_name_plural = _('User Upload Statics')

    def __str__(self):
        return self.content_filter


class UserWeeklyEmailAlert(models.Model):
    user = models.ForeignKey('accounts.User', verbose_name=_('User'), related_name='weekly_email')
    is_subscribed = models.BooleanField(default=True)

    class Meta:
        verbose_name = _('User Weekly  Email Alert')
        verbose_name_plural = _('User Weekly  Email Alert')

    def __str__(self):
        return self.user.username


class TermsOfUse(models.Model):
    text = RichTextField()

    class Meta:
        verbose_name = _('Terms Of Use')
        verbose_name_plural = _('Terms Of Use')

    def __str__(self):
        return 'Terms Of Service'


class PrivacyPolicy(models.Model):
    text = RichTextField()

    class Meta:
        verbose_name = _('Privacy Policy')
        verbose_name_plural = _('Privacy Policy')

    def __str__(self):
        return 'Privacy Policy'


class CopyrightPolicy(models.Model):
    text = RichTextField()

    class Meta:
        verbose_name = _('Copyright Policy')
        verbose_name_plural = _('Copyright Policy')

    def __str__(self):
        return 'Copyright Policy'


class HowItWorks(models.Model):

    intro_title = models.CharField(max_length=250)
    how_it_works_title = models.CharField(max_length=250)
    how_it_works_text = models.TextField()
    intro_text = models.TextField()
    organise_resource_title =  models.CharField(max_length=250)
    organise_resource_text=models.TextField()
    organise_resource_image= models.ImageField(upload_to='how-it-works/resource/')
    discover_title = models.CharField(max_length=250)
    discover_text = models.TextField()
    discover_image = models.ImageField(upload_to="how_it_works/")
    discuss_title = models.CharField(max_length=250)
    discuss_text = models.TextField()
    discuss_image = models.ImageField(upload_to='how_it_works/')
    footer_title = models.CharField(max_length=250)
    footer_text = models.TextField()

    class Meta:
        verbose_name = _('How It Works')
        verbose_name_plural = _('How It Works')

    def __str__(self):
        return 'How It Works'



def send_feedback(sender, instance, **kwargs):
    try:
        site = Site.objects.get_current()
        t = loader.get_template('email/feedback_email.html')
        c = Context({'feedback': instance,  'site': site.name})
        mail = EmailMessage('New feedback added', t.render(c), 'Ednora<no-reply@ednora.com>', ('feedback@ednora.com', ))
        mail.content_subtype = 'html'
        mail.send()
    except:
        pass

signals.post_save.connect(send_feedback, sender=UserFeedBack)


def send_contact(sender, instance, **kwargs):
    try:
        site = Site.objects.get_current()
        t = loader.get_template('email/contact_email.html')
        c = Context({'contact': instance,  'site': site.name})
        mail = EmailMessage('New contact-us added', t.render(c), 'Ednora<no-reply@ednora.com>', ('contactus@ednora.com', ))
        mail.content_subtype = 'html'
        mail.send()
    except:
        pass

signals.post_save.connect(send_contact, sender=Contact)


class Preference(models.Model):
    name = models.CharField(verbose_name=_('Name'), max_length=255, null=True, blank=True, unique=True)

    class Meta:
        verbose_name = _('Preference')
        verbose_name_plural = _('Preference')

    def __str__(self):
        return self.name


class UserPreference(models.Model):
    user = models.ForeignKey('accounts.User', verbose_name=_('User'))
    preference = models.ManyToManyField(Preference)

    class Meta:
        verbose_name = _('User Preferences')
        verbose_name_plural = _('User Preferences')

    def __str__(self):
        return self.user.get_full_name()


class State(models.Model):
    name = models.CharField(verbose_name=_('Name'), max_length=255, null=True, blank=True, unique=True)

    class Meta:
        verbose_name = _('State')
        verbose_name_plural = _('States')

    def __str__(self):
        return self.name


class District(models.Model):
    state = models.ForeignKey('accounts.State', verbose_name=_('State'))
    name = models.CharField(verbose_name=_('Name'), max_length=255, null=True, blank=True)
    email_domail = models.CharField(verbose_name=_('Email domail'), max_length=300, blank=True, null=True)

    class Meta:
        verbose_name = _('District')
        verbose_name_plural = _('Districts')

    def __str__(self):
        return self.name


class School(models.Model):
    district = models.ForeignKey('accounts.District', verbose_name=_('District'))
    name = models.CharField(verbose_name=_('Name'), max_length=255, null=True, blank=True)

    class Meta:
        verbose_name = _('School')
        verbose_name_plural = _('Schools')

    def __str__(self):
        return self.name


class Grade(models.Model):
    grade = models.CharField(verbose_name=_('Grade'), max_length=12, null=False, blank=False, unique=True)
    created = models.DateTimeField(auto_now_add=True, null=True)

    class Meta:
        verbose_name = _('Grade')
        verbose_name_plural = _('Grades')

    def __str__(self):
        return self.grade


class Subject(models.Model):
    subject = models.CharField(verbose_name=_('Subject'), max_length=255, null=False, blank=False, unique=True)
    created = models.DateTimeField(auto_now_add=True, null=True)

    class Meta:
        verbose_name = _('Subject')
        verbose_name_plural = _('Subjects')

    def __str__(self):
        return self.subject


class User(AbstractUser):
    """
    Model extends auth.user and stores more information like country, movie_freq_home  etc

    """

    school = models.ForeignKey(School, verbose_name=_('School'), null=True, blank=True)# Used as city
    grade = models.ManyToManyField(Grade, verbose_name=_('Grade'), null=True, blank=True)
    district = models.ForeignKey(District, verbose_name=_('District'), null=True, blank=True)# Used as state
    state = models.ForeignKey(State, verbose_name=_('State'), null=True, blank=True)# Used as country now
    subject = models.ManyToManyField(Subject, verbose_name=_('Subject'), null=True, blank=True)# Topics
    verified = models.BooleanField(default=False)
    first_login = models.BooleanField(default=False)
    profile_pic = models.ImageField(upload_to='profile-images/', null=True, blank=True)
    district_preference = models.BooleanField(default=True)
    state_preference = models.BooleanField(default=True)
    nation_preference = models.BooleanField(default=True)
    slug = AutoSlugField(populate_from='first_name',  separator='')
    created_time = models.DateTimeField("Created Date", auto_now_add=True)

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def __str__(self):
        if self.first_name == '':
            return self.username
        else:
            return "%s %s" % (self.first_name, self.last_name)

    def get_user_dp(self):
        if self.profile_pic:
            return self.profile_pic.url
        else:
            file_name = self.first_name[0] if self.first_name else self.username[0]
            file_name_in_upper = file_name.upper()
            return '/static/assets/img/Letters/' + file_name_in_upper + '.png'

    def get_user_dp_status(self):
        if self.profile_pic:
            return True
        else:
            return False

    def get_state(self):
        return self.state

    def get_district(self):
        return self.district

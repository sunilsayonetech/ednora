# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-04-12 08:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0024_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='district_preference',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='user',
            name='nation_preference',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='user',
            name='state_preference',
            field=models.BooleanField(default=False),
        ),
    ]

from import_export.admin import ImportExportModelAdmin

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from applications.accounts.models import User, Actor, UserPreference, UserFeedBack,\
    Contact, UserTracking, UserInvites, UserDeactivation, UserDeviceTracking, UserShareTracking,\
    UserSearchTracking, UserFilterTracking, TermsOfUse, PrivacyPolicy, CopyrightPolicy, UserFilterStat, Preference, \
    State, District, School, Grade, Subject, UserWeeklyEmailAlert, HowItWorks


class UserTrackingAdmin(admin.ModelAdmin):
    readonly_fields = ('created_time',)
    list_display = ['user', 'type', 'created_time']
    fields = ['user', 'type', 'content', 'created_time']

    def get_form(self, request, obj=None, **kwargs):
        if obj and obj.type == "re-pick":
            if 'tags' not in self.fields:
                self.fields.append('tags')
        else:
            if 'tags' in self.fields:
                self.fields.remove('tags')
        form = super(UserTrackingAdmin, self).get_form(request, obj, **kwargs)
        return form


class UserWeeklyEmailAlertInline(admin.StackedInline):

    model = UserWeeklyEmailAlert
    can_delete = False
    extra = 1
    max_num = 1

    def get_form(self, request, obj=None, **kwargs):    # Just added this override
        form = super(UserWeeklyEmailAlertInline, self).get_form(request, obj, **kwargs)
        form.widget.can_add_related = False
        return form


class UserDeviceTrackingAdmin(admin.ModelAdmin):
    readonly_fields = ('created_time',)


class UserDeactivationAdmin(admin.ModelAdmin):
    readonly_fields = ('deactivated_time',)
    list_display = ['user', 'type', 'deactivated_time']
    fields = ['user', 'type', 'deactivated_time']

    def get_form(self, request, obj=None, **kwargs):
        if obj and obj.type != "2":
            if 'reason_for_deactivate' not in self.fields:
                self.fields.append('reason_for_deactivate')
        else:
            if 'reason_for_deactivate' in self.fields:
                self.fields.remove('reason_for_deactivate')
        form = super(UserDeactivationAdmin, self).get_form(request, obj, **kwargs)
        return form


class UserSearchTrackingAdmin(admin.ModelAdmin):
    readonly_fields = ('created_time',)


class UserInviteTrackingAdmin(admin.ModelAdmin):
    readonly_fields = ('created_time',)
    list_display = ['user', 'email', 'is_invite_accepted']


class UserFilterStatTrackingAdmin(admin.ModelAdmin):
    readonly_fields = ('created_time',)
    list_display = ['content_filter', 'created_time']


class UserModeladmin(UserAdmin):
    list_display = ['get_full_name', 'email', 'verified', ]
    search_fields = ['first_name', 'last_name', 'email']
    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('school', 'grade', 'district', 'state', 'subject', 'verified', 'first_login',
                           'profile_pic', 'district_preference', 'state_preference', 'nation_preference',)}),
    )
    inlines = [
        UserWeeklyEmailAlertInline,
    ]


class ActorModeladmin(admin.ModelAdmin):
    search_fields = ['name']


class PreferenceModelAdmin(admin.ModelAdmin):
    search_fields = ['name']


class SchooltModeladmin(admin.ModelAdmin):
    list_display = ['name', 'district']
    search_fields = ['name', ]


class DistrictModeladmin(ImportExportModelAdmin):
    list_display = ['name', 'state']
    search_fields = ['name', ]
    export_order = ['state', 'name', 'email_domail']


class StateModeladmin(ImportExportModelAdmin):
    list_display = ['name']
    search_fields = ['name']
    export_order = ['name']


class ContactAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'date_created']
    search_fields = ['name']


class UserShareTrackingAdmin(admin.ModelAdmin):
    list_display = ['user', 'email', 'created_time']
    search_fields = ['email']


admin.site.register(User, UserModeladmin)
admin.site.register(Actor, ActorModeladmin)
admin.site.register(UserFeedBack)
admin.site.register(Contact, ContactAdmin)
admin.site.register(UserTracking, UserTrackingAdmin)
admin.site.register(UserInvites, UserInviteTrackingAdmin)
admin.site.register(UserDeactivation, UserDeactivationAdmin)
admin.site.register(UserDeviceTracking, UserDeviceTrackingAdmin)
admin.site.register(UserShareTracking, UserShareTrackingAdmin)
admin.site.register(UserFilterTracking)
admin.site.register(UserSearchTracking, UserSearchTrackingAdmin)
# admin.site.register(UserFilterStat, UserFilterStatTrackingAdmin)
admin.site.register(TermsOfUse)
admin.site.register(CopyrightPolicy)
admin.site.register(PrivacyPolicy)
# admin.site.register(UserPreference)
admin.site.register(Preference, PreferenceModelAdmin)
admin.site.register(State, StateModeladmin)
admin.site.register(District, DistrictModeladmin)
admin.site.register(School, SchooltModeladmin)
admin.site.register(Grade)
admin.site.register(Subject)
admin.site.register(HowItWorks)

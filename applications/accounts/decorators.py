from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect


def only_for_anonymous_user(function):
    def wrapper(request, *args, **kw):
        if request.user.is_authenticated():
            return HttpResponseRedirect("/")
        return function(request, *args, **kw)

    return wrapper
import json
import os
from binascii import hexlify

import django
from django.contrib.sites.models import Site
from django.core.files.temp import NamedTemporaryFile
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.template import loader, Context
from django.template.loader import render_to_string
from rest_framework import permissions
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from applications.accounts.models import Actor, Grade, School, User, Preference
from applications.activity.models import EmailConfirmation
from applications.api.exceptions import CommentViewException
from applications.pin.models import LearningObjective, LearningObjectiveCode, Comment, Content
from .serializers import ContentSerializer

model_app_mapping = {'content': 'pin', 'comment': 'pin'}


def get_app(): return lambda x: model_app_mapping[x]
get_application = get_app()


class TempFile:

    tmpfile = NamedTemporaryFile()

    def set_file(self,f1):
        self.tmpfile = f1
        return self.tmpfile

    def get_file(self):
        return self.tmpfile


t = TempFile()



class ActorListView(APIView):
    """
    provides list of actors.
    """
    http_method_names = ['get', ]
    permission_classes = [permissions.AllowAny]

    def get(self, request):
        """
        Return list of actors.
        """
        # acts = list()
        # from django.forms.models import model_to_dict
        # actors_list = Actor.objects.all()
        # for x in actors_list:
        #     acts.append(model_to_dict(x))
        actors_list = [x.name for x in Actor.objects.all()]
        return Response(actors_list)


class PreferenceListView(APIView):
    """
    provides list of actors.
    """
    http_method_names = ['get', ]
    permission_classes = [permissions.AllowAny]

    def get(self, request):
        """
        Return list of Preference.
        """
        preference_list = [x.name for x in Preference.objects.all()]
        return Response(preference_list)


class MovieWatchingView(APIView):
    """
    provides values
    """
    http_method_names = ['get', ]

    def get(self, request):
        """
        Return list of actors.
        """
        movi = list()
        from django.forms.models import model_to_dict
        movi_list = School.objects.filter()
        for x in movi_list:
            movi.append(model_to_dict(x))
        movi_list = [x.text for x in School.objects.filter()]
        return Response(movi_list)


class ReviewsInfluenceView(APIView):
    """
    provides list of values.
    """
    http_method_names = ['get', ]
    permission_classes = [permissions.AllowAny]

    def get(self, request):
        """
        Return list of actors.
        """
        revi = list()
        from django.forms.models import model_to_dict
        revi_list = Grade.objects.filter()
        for x in revi_list:
            revi.append(model_to_dict(x))
        revi_list = [x.text for x in Grade.objects.filter()]
        return Response(revi_list)


class PasswordResetView(APIView):
    http_method_names = ('post',)
    permission_classes = [permissions.AllowAny]

    def post(self, request):
        email = self.request.POST.get("email")
        try:
            user_obj = User.objects.get(email=email)
            key = self.generate_hash_key()
            # generating confirmation link to be send to user using hash_key
            link = self.generate_link(key)
            previous_links = EmailConfirmation.objects.filter(user=user_obj)
            for links in previous_links:
                links.link_expired = True
                links.save()
            EmailConfirmation.objects.create(user=user_obj, key=key)
            # load the email template
            template = loader.get_template('email/password_reset.html')
            site = Site.objects.get_current()
            context = Context({'user': user_obj.first_name, 'link': link, 'site': site})
            # sending email
            email = EmailMessage(user_obj.first_name+', your Ednora password reset link', template.render(context),
                                 'Ednora <no-reply@ednora.com>', (user_obj.email,))
            email.content_subtype = 'html'
            email.send()
            data = {'success_status': 'email has been sent'}
            return Response(data)
        except:
            data = {'error_status': 'Not a valid email'}
            return Response(data)

    def generate_hash_key(self):
        hash_key = hexlify(os.urandom(15))
        return hash_key

    def generate_link(self, key):
        url = reverse('pass-change', kwargs={'key': key})
        current_site = Site.objects.get_current()
        url = current_site.domain + url
        return url


class LearningObjectiveView(APIView):
    """
    provides list of learning objectives.
    """
    http_method_names = ['get', ]
    permission_classes = [permissions.AllowAny]

    def get(self, request):
        """
        Return list of learning objectives.
        """
        learning_objective_list = [x.name for x in LearningObjective.objects.all()]
        return Response(learning_objective_list)


class LearningObjectiveCodeView(APIView):
    """
    provides list of learning codes.
    """
    http_method_names = ['get', ]
    permission_classes = [permissions.AllowAny]

    def get(self, request):
        """
        Return list of learning codes.
        """
        learning_objective_code_list = [x.code for x in LearningObjectiveCode.objects.all()]
        return Response(learning_objective_code_list)


class CommentView(APIView):
    """
    API to reply to comment/comment on event, photo, video, user, artist.
    """
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        the_model = request.data.get('model')
        model_id = request.data.get('model_id')
        comment_text = request.data.get('comment_text')
        user = request.user
        if not the_model:
            raise CommentViewException('no model parameter in post request')
        if not model_id:
            raise CommentViewException('no model_id parameter in post request')
        if not comment_text:
            return Response({'status': 'Empty comment, not created'}, status.HTTP_200_OK)
        try:
            the_model_class = django.apps.apps.get_model(get_application(the_model), the_model)
        except:
            raise CommentViewException('app_model_mapping error')
        try:
            the_model_object = the_model_class.objects.get(pk=model_id)
        except:
            raise CommentViewException('no object of given model_id in post request')
        comment_obj = Comment.objects.create(user=user, comment_text=comment_text, content_object=the_model_object)
        ctx = {}
        if the_model == 'comment':
            ctx['html'] = render_to_string('comments/comment_reply_snippet.html', {'comment':comment_obj})
        if the_model =='content':
            ctx['html'] = render_to_string('comments/comment_snippet.html', {'comment':comment_obj})

        return Response(ctx, status.HTTP_200_OK)


class GetRelatedContentView(APIView):
    """
    provides list of learning objectives.
    """
    http_method_names = ['get', ]
    permission_classes = [permissions.AllowAny]

    def get(self, request):
        """
        Return list of learning codes.
        """
        id = self.request.GET.get('id')
        object_content = Content.objects.get(id=id)
        subjects = object_content.subject.all()
        objectives = object_content.objective.all()
        grades = object_content.grade.all()
        related_content = Content.objects.exclude(id=id).filter(
            Q(grade__in=grades) | Q(subject__in=subjects) | Q(objective__in=objectives)).distinct()[:6]
        return Response(json.loads(json.dumps(ContentSerializer(related_content, many=True).data)))
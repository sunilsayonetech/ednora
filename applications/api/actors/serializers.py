from django.template.defaultfilters import truncatechars

from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from applications.accounts.models import Actor, Preference
from applications.pin.models import LearningObjective, LearningObjectiveCode, Comment, Content
from applications.accounts.models import Subject


class ActorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Actor
        fields = ('name',)


class PreferenceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Preference
        fields = ('name',)


class ContentSerializer(serializers.ModelSerializer):
    sub = SerializerMethodField()
    desc_small = SerializerMethodField()
    title_small = SerializerMethodField()
    content_type = SerializerMethodField()

    class Meta:
        model = Content
        fields = ('id', 'slug','sub','title','text','image','base_url', 'desc_small', 'title_small', 'content_type')


    def get_sub(self,obj):
        subs = obj.subject.all()
        sub_name =''
        for sub in subs[:2]:
            sub_name = sub_name + sub.subject+', '
        return sub_name[:-2]

    def get_desc_small(self, obj):
        return truncatechars(obj.text, 30)

    def get_title_small(self, obj):
        return truncatechars(obj.title, 30)

    def get_content_type(self, obj):
        return obj.content_type.name



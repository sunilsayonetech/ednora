class CommentViewException(Exception):
    """
    To handle exception in comment module
    """
    pass
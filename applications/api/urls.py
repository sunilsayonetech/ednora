from rest_framework.routers import DefaultRouter
from django.conf.urls import include, url

from applications.api.actors import views as actors_view
from applications.api import ContentApiViews as content_api_views


router = DefaultRouter()

urlpatterns = [
    url(r'^v3/', include(router.urls)),
    url(r'actors', actors_view.ActorListView.as_view(), name="actors"),
    url(r'reviews_influence', actors_view.MovieWatchingView.as_view(), name="reviews_influence"),
    url(r'movies_watching', actors_view.ReviewsInfluenceView.as_view(), name="movies_watching"),
    url(r'password_reset', actors_view.PasswordResetView.as_view(), name="password_reset"),
    url(r'preference', actors_view.PreferenceListView.as_view(), name="preference"),
    url(r'learning-objective/$', actors_view.LearningObjectiveView.as_view(), name="learning_objective"),
    url(r'learning-objective-code/$', actors_view.LearningObjectiveCodeView.as_view(), name="learning_objective_code"),
    url(r'comment-post/$', actors_view.CommentView.as_view(), name="comment_post"),
    url(r'get-related-content', actors_view.GetRelatedContentView.as_view(), name="get_related_content"),
    url(r'get-content-info/$', content_api_views.GetContentDetails.as_view(), name="get_content_details"),
    url(r'load-more-comments/$', content_api_views.LoadMoreComments.as_view(), name="load_more_comments"),




]


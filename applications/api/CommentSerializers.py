from rest_framework import serializers
from django.utils.timezone import now

from applications.pin.models import Comment


class CommentSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    comment_date = serializers.SerializerMethodField()
    user_image = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        exclude = ()

    def get_user(self, obj):
        return obj.user.get_full_name()

    def get_comment_date(self, obj):

        date_diff = now() - obj.comment_date
        if date_diff.days > 1:
            return str(date_diff.days) + ' days ago'
        elif date_diff.days == 1:
            return str(date_diff.days) + ' day ago'
        if (str(date_diff).split(':')[0] == '0' and str(date_diff).split(':')[1] != '00'):
            return str(date_diff).split(':')[1] + ' minutes ago'
        elif str(date_diff).split(':')[1] == '00':
            return 'just now'
        elif str(date_diff).split(':')[0] != '0':
            return str(date_diff).split(':')[0] + ' hours ago'

    def get_user_image(self, obj):
        if obj.user.profile_pic:
            return obj.user.profile_pic.url
        else:
            return '/static/assets/images/pikachu-6.png'


class NestedCommentSerializer(serializers.ModelSerializer):
    comments = CommentSerializer(read_only=True, many=True)
    user = serializers.SerializerMethodField()
    comment_date = serializers.SerializerMethodField()
    user_image = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        exclude = ()

    def get_user(self, obj):
        return obj.user.get_full_name()

    def get_comment_date(self, obj):
        date_diff = now() - obj.comment_date
        if date_diff.days > 1:
            return str(date_diff.days) + ' days ago'
        elif date_diff.days == 1:
            return str(date_diff.days) + ' day ago'
        if (str(date_diff).split(':')[0] == '0' and str(date_diff).split(':')[1] != '00'):
            return str(date_diff).split(':')[1] + ' minutes ago'
        elif str(date_diff).split(':')[1] == '00':
            return 'just now'
        elif str(date_diff).split(':')[0] != '0':
            return str(date_diff).split(':')[0] + ' hours ago'

    def get_user_image(self, obj):
        if obj.user.profile_pic:
            return obj.user.profile_pic.url
        else:
            return "/static/assets/images/pikachu-6.png"

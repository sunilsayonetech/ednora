from urllib import parse


from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string

from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics

from applications.pin.models import Content
from applications.api.ContentSerializers import ContentDetailSerializer
from applications.api.CommentSerializers import CommentSerializer
from applications.api.pagination import CommentSetPagination
from applications.pin.models import Comment
from applications.utils.related_resource import get_related_contents


class GetContentDetails(APIView):
    """
    get details of content
    """
    http_method_names = ['post', ]
    permission_classes = [permissions.IsAuthenticated]
    serializer = ContentDetailSerializer

    def post(self, request, *args, **kwargs):
        slug = request.POST.get('slug')
        content_obj = get_object_or_404(Content, slug=slug)
        serialized = self.serializer(content_obj, context= {'request':request})
        return Response(serialized.data, status=status.HTTP_200_OK)


class LoadMoreComments(generics.ListAPIView):

    permission_classes = [permissions.IsAuthenticated]
    pagination_class = CommentSetPagination
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()

    def list(self, request, *args, **kwargs):
        qs = get_object_or_404(Content, id=request.GET.get('content_id')).comments.all()[1:]
        page = self.paginate_queryset(qs)
        next =False
        if self.paginator.get_next_link():
            next = True
        else:
            next =False

        print (page)

        # for render to string
        ctx = {'comments': page, 'page': request.GET.get('page')}
        # for responce
        context = {'html':render_to_string('comments/comment.html', ctx),'next_url':next, 'page':int( request.GET.get('page'))}
        return Response(context)

class GetRelatedResource(APIView):

    def get(self, request, *args,**kwargs):
        content_obj = get_object_or_404(Content, id=request.GET.get('content_id'))
        contents = get_related_contents(request.user, content_obj)










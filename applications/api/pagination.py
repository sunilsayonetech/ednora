from django.conf import settings
from rest_framework.pagination import PageNumberPagination


class CommentSetPagination(PageNumberPagination):
    page_size = settings.COMMENT_PAGINATION_SIZE
    page_size_query_param = 'page_size'
    max_page_size = settings.COMMENT_PAGINATION_SIZE
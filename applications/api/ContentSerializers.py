from rest_framework import serializers
from django.template.loader import render_to_string
from django.conf import settings

from applications.pin.models import Content,Comment
from applications.utils.related_resource import get_related_contents


class ContentDetailSerializer(serializers.ModelSerializer):
    media_type = serializers.StringRelatedField()
    objective = serializers.SerializerMethodField()
    grade = serializers.SerializerMethodField()
    subject = serializers.SerializerMethodField()
    tags = serializers.SerializerMethodField()
    content_type = serializers.StringRelatedField()
    content_level = serializers.SerializerMethodField()
    content_learning_objective_code = serializers.SerializerMethodField()
    state = serializers.SerializerMethodField()
    district = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()
    user_image = serializers.SerializerMethodField()
    download_count = serializers.SerializerMethodField()
    view_count = serializers.SerializerMethodField()
    flag_count = serializers.SerializerMethodField()
    like_count = serializers.SerializerMethodField()
    is_file = serializers.SerializerMethodField()
    is_liked = serializers.SerializerMethodField()
    is_flagged = serializers.SerializerMethodField()
    comments = serializers.SerializerMethodField()
    show_more_comments = serializers.SerializerMethodField()
    related_resources = serializers.SerializerMethodField()
    is_content_long = serializers.SerializerMethodField()
    trimmed_content = serializers.SerializerMethodField()

    class Meta:
        model = Content
        exclude = ()

    def get_state(self, obj):
        if obj.user.state:
            return obj.user.state.name
        return ""

    def get_district(self, obj):

        if obj.user.district:
            return obj.user.district.name
        return ""

    def get_user(self, obj):
        return obj.user.get_full_name()

    def get_user_image(self, obj):
        return obj.user.get_user_dp()

    def get_download_count(self, obj):
        return obj.get_user_tracking.filter(type='download').count()

    def get_view_count(self, obj):
        return obj.get_user_tracking.filter(type='cfu').count()

    def get_flag_count(self, obj):
        return obj.get_user_flag.count()

    def get_like_count(self, obj):
        return obj.get_user_like.count()

    def get_is_file(self, obj):
        if obj.file_upload:
            return True
        return False

    def get_is_liked(self, obj):
        return self.context['request'].user.user_like_contents.filter(content=obj).exists()

    def get_is_flagged(self, obj):
        return self.context['request'].user.user_flagged_contents.filter(content=obj).exists()

    def get_comments(self, obj):
        qs = obj.comments.all()[:1]
        return render_to_string("comments/comment.html", {'comments': qs, 'page': 0})

    def get_show_more_comments(self, obj):
        total_count = Content.objects.get(id=obj.id).comments.all()
        check = False
        for each_obj in total_count:
            total_comments_reply_count = each_obj.comments.all().count()
            if total_comments_reply_count > 0:
                check = True
        if obj.comments.all().count() > 1 or check == True:
            return True
        else:
            return False

    def get_subject(self, obj):
        return ", ".join(obj.subject.all().values_list('subject', flat=True))

    def get_grade(self, obj):
        return ", ".join(obj.grade.all().values_list('grade', flat=True))

    def get_content_learning_objective_code(self, obj):
        return ", ".join(obj.content_learning_objective_code.all().values_list('code', flat=True))

    def get_objective(self, obj):
        return ", ".join(obj.objective.all().values_list('name', flat=True))

    def get_tags(self, obj):
        return ", ".join(obj.tags.all().values_list('name', flat=True))

    def get_content_level(self, obj):
        return ", ".join(obj.content_level.all().values_list('name', flat=True))

    def get_is_content_long(self, obj):
        if len(obj.text) > 150:
            return True
        else:
            return False

    def get_trimmed_content(self, obj):
        return obj.text[:150]

    def get_related_resources(self, obj):
        return render_to_string('related_resource.html',
                                {'contents': get_related_contents(self.context['request'].user, obj)})

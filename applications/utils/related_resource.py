from django.db.models import Q, Count

from applications.pin.models import Content


def get_related_contents(user, content):

    content_list = []
    qs = Content.objects.all()

    # sharing preference filtering
    qs = Content.get_user_accessible_contents(user)
    qs = qs.exclude(id=content.id).annotate(
        total_likes=Count('get_user_like')).order_by('total_likes').distinct()

    # filtering through subjects grade tags
    first_qs = qs.filter(grade__in=content.grade.all(), \
                         subject__in=content.subject.all(), \
                         tags__in=content.tags.all())
    if first_qs.count() >= 30:
        return first_qs[:30]
    else:
        content_list = content_list+list(first_qs)

    # filtering through subjects tags
    second_qs = qs.filter(
        subject__in=content.subject.all(), \
        tags__in=content.tags.all()).exclude(id__in=first_qs.values_list('id', flat=True)).distinct()
    if second_qs.count() >= 30:
        return second_qs[:30]
    else:
        content_list = content_list+list(second_qs)
        if len(content_list) > 30:
            return content_list[:30]

    # filtering through subjects  tags
    excluding_ids = list(first_qs.values_list('id', flat=True)) + list(second_qs.values_list('id', flat=True))
    third_qs = qs.filter(
        subject__in=content.subject.all(), \
        tags__in=content.tags.all(), \
        ).exclude(id__in=excluding_ids).distinct()

    if third_qs.count() >= 30:
        return third_qs[:30]
    else:
        content_list = content_list + list(third_qs)
        if len(content_list) > 30:
            return content_list[:30]

    # filtering through subjects
    excluding_ids = list(first_qs.values_list('id', flat=True)) + list(second_qs.values_list('id', flat=True)) + list(third_qs.values_list('id', flat=True))
    fourth_qs = qs.filter(subject__in=content.subject.all()).exclude(id__in=excluding_ids).distinct()
    content_list = content_list + list(fourth_qs)

    if fourth_qs.count() >= 30:
        return fourth_qs[:30]
    else:
        if(content_list):
            return (content_list[:30])
        else:
            content_list = content_list+list(qs)
    return content_list[:30]
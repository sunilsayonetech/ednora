import operator

from django.conf import settings

import indicoio


class BaseService(object):

    def __init__(self):
        pass


class TagService(BaseService):
    def __init__(self, *args, article=None):
        indicoio.config.api_key = settings.INDICOIO_API_KEY
        if article:
            self.article = self.parse_article(article)
            if args:
                args = list(args)
                args.append(self.article)
            else:
                args = [self.article]
        if args:
            args = [arg.strip() for arg in args if arg]
            self.text = ', '.join(args)
            self.tags_dict = self.get_tags_dict()
            self.tags_selected = self.process_tags()
        else:
            self.text = None
            self.tags_selected = None

    def get_tags_dict(self):
        return indicoio.keywords(self.text)

    def process_tags(self):
        sorted_x = sorted(self.tags_dict.items(), key=operator.itemgetter(1), reverse=True)
        top_five = sorted_x[:5]
        top_five = [tag[0] for tag in top_five]
        return top_five

    def parse_article(self, article):
        article_slices = article.split('\n')
        article_slices_selected = [content.strip() for content in article_slices if content]
        return '\n'.join(article_slices_selected[: settings.ARTICLE_PARA_LIMIT])

import urllib.parse
from urllib.parse import urlparse
import urllib.request
import tempfile
import urllib.parse
import urllib.request
import os
import requests
import time
from random import randint

from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium import webdriver
from bs4 import BeautifulSoup
from PIL import Image
from postr.settings import PROJECT_DIR
from pyvirtualdisplay import Display
import time
import urllib.parse
import urllib.request
import urllib.parse
import urllib.request
import re
from pdf_miner.pdfminer.pdfinterp import PDFResourceManager, process_pdf
from pdf_miner.pdfminer.converter import TextConverter
from pdf_miner.pdfminer.layout import LAParams
from io import StringIO

from django.conf import settings
try:
    from hashlib import sha1 as sha_constructor
except ImportError:
    from django.utils.hashcompat import sha_constructor

from preview_generator.manager import PreviewManager


def get_top_images(url):
    opener = urllib.request.FancyURLopener({})
    opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0'), ('Connection', 'keep-alive')]
    try:
        content = opener.open(url)
        urlcontent = content.read().decode('utf-8', errors='ignore')
        opener.close()
        urls = []
        cleaned_urls = cleaned_urls_regex1(url, urlcontent) + cleaned_urls_regex2(url,
                                                                                  urlcontent) + cleaned_urls_using_img_tag(
            url, urlcontent)
        cleaned_urls1 = list(cleaned_urls)
        for url in cleaned_urls:
            if ('facebook' in url or 'icon' in url or 'logo' in url or 'banner' in url or 'google' in url or 'adds' in url):
                cleaned_urls1.remove(url)

        # remove duplicates
        cleaned_urls = list(set(cleaned_urls1))
        return cleaned_urls
    except Exception as e:
        print(e)
        return []


def cleaned_urls_regex1(url, html):
    regex = re.compile("([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png))")
    urls = regex.findall(html, re.M)
    count = len(urls)
    cleaned_urls = []
    i = 1
    for url1 in urls:
        if url1[0].startswith("htt"):
            cleaned_urls.append(url1[0])

        elif url1[0].startswith("\\"):
            cleaned_urls.append("http:" + url1[0])

        else:
            cleaned_urls.append(urllib.parse.urljoin(url, url1[0]))

    return cleaned_urls


def cleaned_urls_regex2(url, html):
    regex = re.compile("(https?:/?/?[\w_\-&%?./]*?\.jpg|png|jpeg)")
    urls = regex.findall(html, re.M)
    count = len(urls)
    cleaned_urls = []
    i = 1
    for url1 in urls:
        if url1[0].startswith("htt"):
            cleaned_urls.append(url1[0])

        elif url1[0].startswith("\\"):
            cleaned_urls.append("http:" + url1[0])

        else:
            cleaned_urls.append(urllib.parse.urljoin(url, url1[0]))

    return cleaned_urls


def cleaned_urls_using_img_tag(url, html):
    cleaned_urls = []
    image_url = ""
    soup = BeautifulSoup(html, "html.parser")
    imgs = soup.find_all("img")
    for img in imgs:

        if img.has_attr("src"):
            if '.gif' not in img['src']:
                image_url = img['src']

        elif img.has_attr('data-src'):
            if '.gif' not in img['data-src']:
                image_url = img['data-src']

        elif img.has_attr('data-url'):
            if '.gif' not in img['data-url']:
                image_url = img['data-url']

        elif img.has_attr('data-image_url'):
            if '.gif' not in img['data-url']:
                image_url = img['data-image_url']

        if image_url.startswith("htt"):
            cleaned_urls.append(image_url)

        elif image_url.startswith("\\"):
            cleaned_urls.append("http:" + image_url)

        else:
            cleaned_urls.append(urllib.parse.urljoin(url, image_url))

    return cleaned_urls


def is_valid_url(url):
    opener = urllib.request.FancyURLopener({})
    opener.addheaders = [('User-agent', 'Mozilla/5.0'), ]
    try:
        url_object = urlparse(url)

        groups = url_object.netloc.split('.')
        regex = '.'.join(groups[:2]), '.'.join(groups[2:])
        content = opener.open(url)
        con = urlparse(content.geturl())

        if regex[0] in con.netloc:
            return True
        else:
            return False
    except Exception as e:

        print(e)


def get_pdf_screenshot(url):
    try:
        print('..................here in get_pdf_screenshot method.....................')
        display = Display(visible=0, size=(1800, 1600))
        display.start()
        # now Firefox will run in a virtual display.
        # you will not see the browser.
        browser = webdriver.Firefox()
        browser.set_window_size(1480, 1320)
        browser.implicitly_wait(20)
        browser.set_page_load_timeout(50)
        browser.get(url)
        time.sleep(9)
        f = tempfile.NamedTemporaryFile(suffix=".png", dir="media", delete=False)
        browser.save_screenshot(filename=f.name)
        browser.quit()
        display.stop()
        img = Image.open(f.name)
        height = img.size[1]
        width = img.size[0]
        if height > 1024:
            img = img.crop((0, 0, width, 1024))
            img.save(f.name)
        return f

    except Exception as e:
        print(e, "ooooooooooooooooooooo here is the exception oooooooooooooooooooooooooooooooooooooooooooooo")
        f = open(PROJECT_DIR+"/ednora/static/assets/img/pdf_default.png")
        return f


def get_website_screenshot(url):
    try:
        dcap = dict(DesiredCapabilities.PHANTOMJS)
        dcap["phantomjs.page.settings.userAgent"] = (
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 Safari/537.36")
        page = webdriver.PhantomJS(desired_capabilities=dcap,
                                   service_args=['--ignore-ssl-errors=true', '--ssl-protocol=any',
                                                 '--web-security=false'])
        page.set_window_size(1500, 900)
        page.implicitly_wait(20)
        page.set_page_load_timeout(150)
        page.get(url)
        page.execute_script('document.body.style.background = "white"')
        page.set_window_size(1024, 768)
        f = tempfile.NamedTemporaryFile(suffix=".png", dir="media", delete=False)
        page.save_screenshot(filename=f.name)
        os.chmod(f.name, 0o666)
        page.quit()
        img = Image.open(f.name)
        height = img.size[1]
        width = img.size[0]
        if height > 1024:
            img = img.crop((0, 0, width, 1024))
            img.save(f.name)
            os.chmod(f.name, 0o666)
        return f

    except Exception as e:
        print(e)
        return []

def filter_urls(url):
    url = url.lower()

    if 'logo' in url : return False
    if 'icon' in url : return False
    if 'banner' in url : return False

    return True
# def get_pdf_info(url):
#                 try:
#                     opener = urllib.request.FancyURLopener({})
#                     opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0'), ('Connection', 'keep-alive')]
#                     remoteFile = opener.open(url).read()
#                     memoryFile = BytesIO(remoteFile)
#                     pdfFile = PdfFileReader(memoryFile)
#                     pdf_info = {'title':pdfFile.getDocumentInfo().title,'description':pdfFile.getPage(0).extractText().strip()}
#                     return pdf_info
#                 except Exception as e:
#                     return {'title':'','description':''}


def get_base_url(url):
    try:
        url = url.replace('https://','')
        url = url.replace('http://','')
        if url[-1] == '/':
            url = url[:-1]
        return url
    except : return ''

def is_bot_request(request):
    bot_list= ['FacebookBot', 'Google-HTTP-Java-Client' ,'AppleBot' ,'GooglePlusBot']
    flag = False
    if 'bot' in  str(request.user_agent).lower():
        return True
    else:
         for bot in bot_list:
             if bot in str(request.user_agent):
                 Flag = True
                 break
             else:
                 Flag = False

         return Flag



def get_pdf_info(url):
    try:
        password = ''
        pagenos = set()
        maxpages = 3
        caching = True
        laparams = LAParams()
        outfp = StringIO()
        rsrcmgr = PDFResourceManager(caching=caching)
        opener = urllib.request.FancyURLopener({})
        opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0'),
                             ('Connection', 'keep-alive')]
        device = TextConverter(rsrcmgr, outfp, laparams=laparams)
        fp = opener.open(url)
        f = process_pdf(rsrcmgr, device, fp, pagenos, maxpages=maxpages, password=password, caching=caching,
                        check_extractable=True)
        fp.close()
        device.close()
        ctx={}
        ctx = get_paragraphs(outfp.getvalue().strip())
        return  ctx
    except Exception as e:
            return {'title':'','description':''}



def get_paragraphs(text):
    #print '##################################'
    #print text
    #print '##################################'
    ########################################
    # Note: This variable is used to configure
    # the number of pages to be read
    words_in_para = 15
    para_toreturn = 3
    ########################################
    return_title = ""
    return_val = ""
    buffer = ""
    prev_line = ""
    para_count = 0
    for line in text.splitlines():
        # Remove the final '-'
        # print (line)
        # TODO: Fix this
        if line.endswith('-'):
            line = line[:-1]
        # If line is empty (\n only) then we probably ended
        # a paragraph, so add buffer to return_val
        if line == "":
            # Some paragraphs should be skipped
            # Let us see if we can find them
            accept_para = True
            # Criterion 1: Number of words in paragraph to be
            #              greater than 10
            # Criterion 2: If return_title is null, then the first line that doesn't have anything stupid may be
            #              the title. Also should have more than 3 words
            if return_title == "":
                if len(buffer.split()) > 3:
                    if any(c.isalpha() for c in buffer):
                        return_title = buffer
                    # I accepted the para for title, let us not take it for description
                    accept_para = False

            if len(buffer.split()) < 10:
                accept_para = False
            if accept_para:
                return_val += buffer
                #print ('Accepting: ' + buffer)
                para_count += 1
            buffer = ""
        else:
            # Some lines should be skipped. Let us
            # see if we can find them
            accept_line = True
            # Criterion 1: Line should not contain more than 4 consecutive special characters (.,#,/,\)
            if re.search('\.\.\.\.', line) or re.search('\#\#\#\#', line) or re.search('\\\\\\\\', line) or re.search('\/\/\/\/', line):
                accept_line = False
                #print line
            if accept_line:
                buffer += line
                buffer += ' '

        if para_count == 3:
            break
        prev_line = line
    return {'title':return_title,'description':return_val}


def get_file_screenshot(url):
   try:
       display = Display(visible=0, size=(1800, 1600))
       display.start()
       # now  Chrome will run in a virtual display.
       # you will not see the browser.
       #For local
       # browser = webdriver.Chrome('/home/sayone/projects/ednora/chromedriver')
       # For Pickks Staging
       # browser = webdriver.Chrome('/home/ubuntu/chromedriver')
       # For Pickks Production
       browser = webdriver.Chrome('/var/ednora/ednora/chromedriver')
       browser.set_window_size(1480, 1320)
       browser.implicitly_wait(20)
       browser.set_page_load_timeout(150)
       browser.get(url)
       time.sleep(9)
       try:
        f = tempfile.NamedTemporaryFile(suffix=".png", dir="media", delete=False)
       except Exception as e:
           pass
       browser.save_screenshot(filename=f.name)
       os.chmod(f.name, 0o666)
       browser.quit()
       display.stop()
       try:
        img = Image.open(f.name)
       except Exception as e:
           pass
       height = img.size[1]
       width = img.size[0]

       if height > 1024:
           img = img.crop((0, 0, width, 1024))
           img.save(f.name)
           os.chmod(f.name, 0o666)
       return f

   except Exception as e:
       print(e)
       f = open(PROJECT_DIR + "/static/assets/img/pdf_default.png")
       return f


def get_file_preview(f_path, width=830, height=842):
    """
    file_path - file path up to media eg: /media/tmp/example.pdf
    return - will return full path of preview
    """
    time_str = time.strftime("%Y%m%d")
    manager = PreviewManager(path=os.path.join(settings.MEDIA_ROOT, 'preview', time_str), create_folder=True)
    path_to_file = manager.get_jpeg_preview(page=0, file_path=os.path.join(
        settings.MEDIA_ROOT, f_path), height=height, width=width,
    )
    return path_to_file


def get_url_preview(url):
    image_url = get_page2images_img_by_polling(url)
    if image_url == 'error':
        return ""
    return save_image_url_to_local_file(image_url)


def save_image_url_to_local_file(img_url):
    """
    Save remote url to our server
    :param img_url:
    :return:
    """
    time_str = time.strftime("%Y%m%d")
    rand_filename = time_str+str(randint(9999, 99999))+'.png'
    url_preview_path = os.path.join(settings.MEDIA_ROOT, 'url_preview')
    preview_path = '/media/url_preview'
    urllib.request.urlretrieve(img_url, os.path.join(url_preview_path, rand_filename))
    return os.path.join(preview_path, rand_filename)

def get_page2images_img_by_polling(url):
    """
    Try for 25 secs, if no image assume it as failed
    :param url:
    :return:
    """
    for interval in range(16):
        image_url = get_page2images_img(url)
        if image_url:
            return image_url
        time.sleep(3)
    return 'error'


def get_page2images_img(url):
    """
    http://www.page2images.com/Create-website-screenshot-online-API
    :param url:
    :return:
    """
    req_url = "http://api.page2images.com/restfullink?p2i_url=%s&p2i_key=%s" % (
        url, settings.PAGE2IMAGES_API_KEY)
    data = requests.get(req_url).json()
    if data['status'] == 'finished':
        return data['image_url']
    elif data['status'] == 'error':
        return 'error'
    return False


def generate_sha1(string, salt=None):
    """
    Generates a sha1 hash for supplied string.

    :param string:
        The string that needs to be encrypted.

    :param salt:
        Optionally define your own salt. If none is supplied, will use a random
        string of 5 characters.

    :return: Tuple containing the salt and hash.

    """
    import random
    string = str(string)
    if not salt:
        salt = sha_constructor(str(random.random()).encode('utf-8')).hexdigest()[:5]
    hash = sha_constructor((salt+string).encode('utf-8')).hexdigest()

    return salt, hash
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType as CoreContentType
from django_extensions.db import fields
from django.contrib.contenttypes.fields import GenericRelation
from django.conf import settings
from django.db.models import Q
from sorl.thumbnail import get_thumbnail
from django_extensions.db.fields import AutoSlugField
from ckeditor.fields import RichTextField
from django.utils import timezone

from django.db.models import Count
from django.db.models import BooleanField, Case, When

import uuid
import json

from applications.pin.utils import generate_sha1


class ContentType(models.Model):
    """
    Saving different Content Types
    """
    slug = AutoSlugField(populate_from='name', blank=True, null=True, max_length=255)
    name = models.CharField(verbose_name=_('Name'), max_length=100, unique=True)
    image = models.ImageField(upload_to='accounts/content-type-images/', verbose_name=_('Content Type Image '),
                              max_length=1000)

    class Meta:
        verbose_name = _('Content Type')
        verbose_name_plural = _('Content Types')

    def __str__(self):
        return self.name

    def image_tag(self):
        return u'<img src="%s" />' % self.image.url
    image_tag.short_description = 'Image'
    image_tag.allow_tags = True

class ContentTypeImage(models.Model):
    image = models.ImageField(upload_to='accounts/content-type-images/', verbose_name=_('Content Type Image '),
                              max_length=1000)
    content_type = models.ForeignKey(ContentType, null=True, blank=True, related_name='content_type_images')


class MediaType(models.Model):
    """
    Saving different Media Types
    """
    name = models.CharField(verbose_name=_('Name'), max_length=100, blank=True, null=True)

    class Meta:
        verbose_name = _('Media Type')
        verbose_name_plural = _('Media Types')

    def __str__(self):
        return self.name


def get_file_path_for_content_image(instance, filename):
    ext = filename.split('.')[-1]
    if instance.id:
        random_value = uuid.uuid1()
        filename = "accounts/content-images/postr-1000%s%s.%s" % (instance.id, random_value, ext)
    else:
        from applications.accounts.models import User
        images = User.objects.all().order_by('-id')
        val = 1
        if images:
            val = images[0].id + 1
        filename = "accounts/content-images/postr-1000%s.%s" % (val, ext)
    return filename


class Tag(models.Model):
    """
    Saving different Content Types
    """
    name = models.CharField(verbose_name=_('Name'), max_length=200, unique=True)

    class Meta:
        verbose_name = _('User Tag')
        verbose_name_plural = _('User Tag')
        ordering = ('name', )

    def __str__(self):
        return self.name


class Level(models.Model):
    name = models.CharField(verbose_name=_('Name'), max_length=255, null=True, blank=True, unique=True)

    class Meta:
        verbose_name = _('Level')
        verbose_name_plural = _('Levels')

    def __str__(self):
        return self.name


class LearningObjective(models.Model):
    name = models.CharField(verbose_name=_('Name'), max_length=200, null=True, blank=True, unique=True)

    class Meta:
        verbose_name = _('Learning Objective')
        verbose_name_plural = _('Learning Objectives')
        ordering = ('name', )

    def __str__(self):
        return self.name


class LearningObjectiveCode(models.Model):
    code = models.CharField(verbose_name=_('Code'), max_length=200, null=True, blank=True, unique=True)
    created = models.DateTimeField(auto_now_add=True, null=True)

    class Meta:
        verbose_name = _('Learning Objective Code')
        verbose_name_plural = _('Learning Objective Code')
        ordering = ('code', )

    def __str__(self):
        return self.code


def upload_to_unqiue_folder(instance, filename):
    """
    Uploads a file to an unique generated Path to keep the original filename
    """

    salt, hash = generate_sha1('{}{}'.format(filename, timezone.now()))

    return '%(path)s%(hash_path)s/%(filename)s' % {'path': 'content/'+timezone.now().date().strftime("%Y/%m/%d/"),
                                               'hash_path': hash[:10],
                                               'filename': filename}

class Content(models.Model):
    """
    Saving Contents of each type
    """
    user = models.ForeignKey('accounts.User', related_name='user_details', verbose_name=_('User'), null=True, blank=True)
    content_type = models.ForeignKey(ContentType, verbose_name=_('Content Type'))
    media_type = models.ForeignKey(MediaType, verbose_name=_('Media Type'), null=True, blank=True)
    base_url = models.CharField(verbose_name=_('Content Base URL'), blank=True, max_length=1000)
    absolute_url = models.URLField(verbose_name=_('Content Absolute URL'), default='', max_length=1000)
    file_upload = models.FileField(verbose_name=_('File Path'), null=True, blank=True, max_length=1000, upload_to=upload_to_unqiue_folder)
    title = models.CharField(verbose_name=_('Content Title'), max_length=255, null=True, blank=True)
    text = models.TextField(verbose_name=_('Content Text'), null=True, blank=True)
    grade = models.ManyToManyField('accounts.Grade', related_name='accounts_grade')
    subject = models.ManyToManyField('accounts.Subject', related_name='accounts_subject')
    level = models.ForeignKey(Level, verbose_name=_('Level'), null=True, blank=True)
    content_level = models.ManyToManyField('Level', related_name='accounts_level', null=True)
    objective = models.ManyToManyField(LearningObjective, related_name='learning_objective')
    learning_objective_code = models.ForeignKey(LearningObjectiveCode, verbose_name=_('Learning Objective Code'), max_length=255, null=True, blank=True)
    content_learning_objective_code = models.ManyToManyField(LearningObjectiveCode, verbose_name=_('Learning Objective Code'), related_name='learning_objective_code')
    image = models.ImageField(upload_to='accounts/content-images/', verbose_name=_('Content Image '), null=True, blank=True, max_length=1000)
    created = fields.CreationDateTimeField(verbose_name=_('Created'))
    tags = models.ManyToManyField(Tag)
    slug = AutoSlugField(populate_from='created', unique=True, separator='')
    comments = GenericRelation('pin.Comment')

    class Meta:
        verbose_name = _('Content')
        verbose_name_plural = _('Contents')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('viewpindetails', args=[self.slug])

    def get_image(self, size='300x300'):
        if self.image:
            thumbnail = get_thumbnail(self.image, size, crop=False)
            return thumbnail.url

    def get_like_count(self):
        objects = self.get_user_like.all()
        return objects.count()

    def get_flag_count(self):
        objects = self.get_user_flag.all()
        return objects.count()

    def get_state(self):
        return self.user.state

    def get_district(self):
        return self.user.district

    def get_district_preference(self):
        return self.user.district_preference

    def get_state_preference(self):
        return self.user.state_preference

    @staticmethod
    def get_user_accessible_contents(user):
        # district preference is on by default, no need of filter
        pins = Content.objects.filter().annotate(
            user_like=Count('get_user_like')).annotate(flag_count=Count('get_user_flag')).annotate(
            flag_count_greater=Case(
                    When(flag_count__gt=settings.FLAG_COUNT_TO_SUPPRESS, then=True),
                    default=False,
                    output_field=BooleanField(),
                ),
            my_related_content_both=Case(
                When(Q(subject__in=user.subject.all()) | Q(grade__in=user.grade.all()), then=True),
                default=False,
                output_field=BooleanField(),
            ),
            my_related_content_subject=Case(
                When(subject__in=user.subject.all(), then=True),
                default=False,
                output_field=BooleanField(),
            ),
            my_related_content_grade=Case(
                When(grade__in=user.grade.all(), then=True),
                default=False,
                output_field=BooleanField(),
            ),
            my_related_content_country=Case(
                When(user__state=user.state, then=True),
                default=False,
                output_field=BooleanField(),
            ),
            my_own_pins=Case(
                When(user=user, then=True),
                default=False,
                output_field=BooleanField(),
            )
        )
        return pins.order_by('flag_count_greater', 'my_own_pins', '-my_related_content_both','-my_related_content_subject', '-my_related_content_grade', '-my_related_content_country', '-created', '-user_like')

    def get_croped_image(self, size='190x190'):
        if self.image:
            thumbnail = get_thumbnail(self.image, size, crop='center',)
            return "https://www.ednora.com/"+thumbnail.url


class Folder(models.Model):
    """
    Folder for items by particular user
    """
    user = models.ForeignKey('accounts.User', verbose_name=_('User'), related_name="get_folder")
    name = models.CharField(verbose_name=_('Folder Name'), max_length=255, blank=True)
    created = fields.CreationDateTimeField(verbose_name=_('Created'))
    slug = AutoSlugField(populate_from='name', null=True, max_length=255)

    class Meta:
        verbose_name = _('Folder')
        verbose_name_plural = _('Folders')

    def __str__(self):
        return self.name


class Pin(models.Model):
    """
    Saving Pinned Item
    """
    folder = models.ForeignKey(Folder, verbose_name=_('Folder'), related_name="get_pin")
    content = models.ForeignKey(Content, verbose_name=_('Content'), related_name="get_content")
    created = fields.CreationDateTimeField(verbose_name=_('Created'))

    class Meta:
        verbose_name = _('Pin')
        verbose_name_plural = _('Pins')

    def __str__(self):
        return self.folder.name


class Like(models.Model):
    """
    save the content which the user liked
    """
    user = models.ForeignKey('accounts.User', verbose_name=_('User'), related_name='user_like_contents')
    content = models.ForeignKey(Content, verbose_name=_('Content'), related_name="get_user_like")

    class Meta:
        verbose_name = _('Like')
        verbose_name_plural = _('Likes')

    def __str__(self):
        return self.user.email

    def get_like_count(self):
        return self.related_name.all()


class Flag(models.Model):
    """
    save the content which the user flagged
    """
    user = models.ForeignKey('accounts.User', verbose_name=_('User'), related_name="user_flagged_contents")
    content = models.ForeignKey(Content, verbose_name=_('Content'), related_name="get_user_flag")

    class Meta:
        verbose_name = _('Flag')
        verbose_name_plural = _('Flags')

    def __str__(self):
        return self.user.email

    def get_flag_count(self):
        return self.related_name.all()


class UserPinUploadTracking(models.Model):
    user = models.ForeignKey('accounts.User', verbose_name=_('User'))
    pin_upload_using_get_browser_button = models.IntegerField()
    pin_upload_using_upload_pin_popup = models.IntegerField()

    def __str__(self):
        return self.user.first_name


class UserRecommendation(models.Model):
    user = models.ForeignKey('accounts.User', verbose_name=_('User'))
    recommended_pins = models.TextField()

    class Meta:
        verbose_name = _('User Recommendation')
        verbose_name_plural = _('User Recommendation')

    def __str__(self):
        return self.user.email

    def get_query_set(self):
        content_ids = json.loads(self.recommended_pins)
        return Content.objects.filter(pk__in=content_ids)

    def get_serialized_picks(self):
        content_ids = json.loads(self.recommended_pins)
        contents = Content.objects.filter(pk__in=content_ids).distinct()
        return contents


class UserWeeklyEmailAlertRecommendation(models.Model):
    user = models.ForeignKey('accounts.User', verbose_name=_('User'))
    recommended_pins = models.ManyToManyField(Content, verbose_name=_('Content'))
    is_email_send = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('User Email  Recommendation')
        verbose_name_plural = _('User Email  Recommendation')

    def __str__(self):
        return self.user.first_name


class UserPinUploadTrackingStat(models.Model):
    pin_upload_using_get_browser_button = models.IntegerField()
    pin_upload_using_upload_pin_popup = models.IntegerField()
    created = fields.CreationDateTimeField(verbose_name=_('Created'))

    def __str__(self):
        return str(self.created)


class Comment(models.Model):
    """
    to store details concerning comments made by users
    """
    comment_text = RichTextField(_("Comment Text"))
    comment_date = models.DateTimeField(_("Date Created"), auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    content_type = models.ForeignKey(CoreContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    comments = GenericRelation('Comment')

    class Meta:
        abstract = False
        verbose_name = _('Comment')
        verbose_name_plural = _('Comments')
        ordering = ['-id']

    def __str__(self):
        return self.comment_text[:30]

__author__ = 'sayone'

from haystack import indexes

from applications.pin.models import Content
from applications.accounts.models import UserTracking

from django.db.models import BooleanField, Case, When
from django.conf import settings
# indexing content with title and text


class ContentIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    # text = indexes.EdgeNgramField(document=True, use_template=True)
    content_id = indexes.IntegerField()
    object_id = indexes.IntegerField()
    tags = indexes.MultiValueField(model_attr='tags__id')
    absolute_url = indexes.EdgeNgramField(model_attr='absolute_url')
    subject = indexes.MultiValueField(model_attr='subject__id')
    grade = indexes.MultiValueField(model_attr='grade__id')
    objective = indexes.MultiValueField(model_attr='objective__id', null=True)
    content_learning_objective_code = indexes.MultiValueField(model_attr='content_learning_objective_code__id', null=True)
    content_level = indexes.MultiValueField(model_attr='content_level__id')
    download_count = indexes.IntegerField()
    view_count = indexes.IntegerField()
    user = indexes.CharField(model_attr='user', null=True)
    state = indexes.CharField(null=True)
    district = indexes.CharField(model_attr='get_district', null=True)
    district_preference = indexes.BooleanField(model_attr='user__district_preference')
    state_preference = indexes.BooleanField(model_attr='user__state_preference')
    nation_preference = indexes.BooleanField(model_attr='user__nation_preference')
    like_count = indexes.CharField(model_attr='get_like_count', null=True)
    flag_count = indexes.CharField(model_attr='get_flag_count', null=True)
    media_type = indexes.MultiValueField(model_attr='media_type__id')
    content_type = indexes.MultiValueField(model_attr='content_type__id')
    flag_count_greater = indexes.BooleanField()

    def get_model(self):
        return Content

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()

    def prepare_content_id(self, obj):
        return obj.content_type.pk

    def prepare_object_id(self, obj):
        return obj.id

    def prepare_state(self, obj):
        return obj.user.state_id

    def prepare_download_count(self, obj):
        return UserTracking.objects.filter(type='download',content=obj.id).count()

    def prepare_view_count(self, obj):
        return UserTracking.objects.filter(type='cte', content=obj.id).count()

    def prpare_flag_count_greater(self, obj):
        return  Case(
            When(flag_count__gt=settings.FLAG_COUNT_TO_SUPPRESS, then=True),
            default=False,
            output_field=BooleanField(),
        )
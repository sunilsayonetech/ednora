import datetime

from rest_framework import serializers

from applications.accounts.models import User, UserSearchTracking, UserPreference,UserTracking, School, District, State
from applications.pin.models import Content,Tag, LearningObjectiveCode


class UserSerializer(serializers.ModelSerializer):
    # reviews_influence = serializers.SerializerMethodField()
    UID = serializers.SerializerMethodField()
    # movie_watching = serializers.SerializerMethodField()
    # UserPreference = serializers.SerializerMethodField()
    country = serializers.SerializerMethodField()
    # New Fields
    school = serializers.SerializerMethodField()
    grade = serializers.SerializerMethodField()
    district = serializers.SerializerMethodField()
    state = serializers.SerializerMethodField()
    subject = serializers.SerializerMethodField()
    district_preference = serializers.SerializerMethodField()
    state_preference = serializers.SerializerMethodField()
    nation_preference = serializers.SerializerMethodField()

    class Meta:
        model = User
        exclude = ('verified', 'password','school','grade','district','state','subject','district_preference',
                   'state_preference','nation_preference',
                   'user_permissions', 'last_login',
                   'groups', 'is_superuser', 'is_staff', 'id', 'is_active', 'date_joined',
                   'slug', 'created_time', 'profile_pic', 'email', 'first_name', 'last_name','country', 'username')

    # def get_reviews_influence(self, instance):
    #     if instance.reviews_influence:
    #         return instance.reviews_influence.text

    def get_UID(self, instance):
        return instance.id

    # def get_movie_watching(self, instance):
    #     return instance.movie_watching.text

    # def get_UserPreference(self, instance):
    #     all_preferences = []
    #     user_preferences = UserPreference.objects.filter(user=instance)
    #     for user_preference in user_preferences:
    #         preferences = user_preference.preference.all()
    #         all_preferences += [preference.name for preference in preferences]
    #     return all_preferences

    def get_country(self, instance):
        return instance.country.name

    def get_school(self, instance):

        if instance.school:
            school = instance.school.name
            return school
        else:
            return ""


    def get_district(self,instance):

        if instance.district:
            district = instance.district.name
            return district
        else:
            return ""

    def get_state(self,instance):

        if instance.state:
            state = instance.state.name
            return state
        else:
            return ""


    def get_grade(self,instance):
        grades = list(instance.grade.values_list('grade', flat=True))
        return grades

    def get_subject(self,instance):
        subjects = list(instance.subject.values_list('subject', flat=True))
        return subjects

    def get_district_preference(self,instance):
        district_sharing_preference = instance.district_preference
        return district_sharing_preference

    def get_state_preference(self,instance):
        state_sharing_preference = instance.state_preference
        return state_sharing_preference

    def get_nation_preference(self,instance):
        nation_sharing_preference = instance.nation_preference
        return nation_sharing_preference

class UserSearchTrackingSerializer(serializers.ModelSerializer):
    String = serializers.SerializerMethodField()
    Time = serializers.SerializerMethodField()
    class Meta:
        model = UserSearchTracking
        exclude = ('user', 'created_time', 'id', 'search_keyword')

    def get_String(self, instance):
        return instance.search_keyword

    def get_Time(self, instance):

        return instance.created_time.isoformat()

class ContentSerializer(serializers.ModelSerializer):
    CID = serializers.SerializerMethodField()
    Tags = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()
    URL =  serializers.SerializerMethodField()

    #New Fields
    # User = serializers.SerializerMethodField()
    Grades = serializers.SerializerMethodField()
    Subjects = serializers.SerializerMethodField()
    Content_Level = serializers.SerializerMethodField()
    Objective = serializers.SerializerMethodField()
    Learning_Objective_Code = serializers.SerializerMethodField()


    class Meta:
        model = Content
        exclude = ('id', 'content_type', 'base_url', 'title', 'text', 'image', 'created', 'tags', 'slug', 'absolute_url',
                   'grade', 'subject', 'content_level', 'objective', 'learning_objective_code','level')

    def get_CID(self, instance):
        return instance.id

    # def get_User(self, instance):
    #     if instance.user:
    #         id = instance.user.id
    #         return id
    #     else:
    #         return ""

    def get_Tags(self, instance):
        tags = list(instance.tags.values_list('name', flat=True))
        return tags
    def get_title(self, instance):
        st = instance.title.replace("\"", "\'")
        return st

    def get_URL(self, instance):
        return instance.absolute_url

    def get_Grades(self,instance):
        grades = list(instance.grade.values_list('grade', flat=True))
        return grades

    def get_Subjects(self,instance):
        subjects = list(instance.subject.values_list('subject', flat=True))
        return subjects

    def get_Content_Level(self,instance):
        content_levels = list(instance.content_level.values_list('name', flat=True))
        return content_levels

    def get_Objective(self,instance):
        objectives = list(instance.objective.values_list('name', flat=True))
        return objectives

    def get_Learning_Objective_Code(self,instance):
        content_learning_objective_code = list(instance.content_learning_objective_code.values_list('code', flat=True))
        return content_learning_objective_code


class UserTrackingSerializer(serializers.ModelSerializer):
      content_id = serializers.SerializerMethodField()
      type = serializers.SerializerMethodField()
      time = serializers.SerializerMethodField()

      class Meta:
          model = UserTracking
          exclude = ('id','type', 'content', 'time')
      def get_content_id(self, instance ):
          return instance.content.id

      def get_type(self, instance):
          return instance.type
      def get_time(self, instance):
          return instance.created_time.isoformat()

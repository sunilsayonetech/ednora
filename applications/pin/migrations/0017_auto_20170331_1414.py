# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-03-31 14:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pin', '0016_auto_20170331_1212'),
    ]

    operations = [
        migrations.AddField(
            model_name='content',
            name='file_upload',
            field=models.FileField(blank=True, max_length=1000, upload_to='media/content/content_files/%Y/%m/%d/', verbose_name='Content Absolute URL'),
        ),
        migrations.AlterField(
            model_name='content',
            name='absolute_url',
            field=models.URLField(blank=True, max_length=1000, null=True, verbose_name='Content Absolute URL'),
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-11-24 07:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pin', '0007_auto_20161020_1644'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userrecommendation',
            name='recommended_pins',
        ),
        migrations.AddField(
            model_name='userrecommendation',
            name='recommended_pins',
            field=models.TextField(default='test'),
            preserve_default=False,
        ),
    ]

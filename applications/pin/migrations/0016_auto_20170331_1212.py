# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-03-31 12:12
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('pin', '0015_auto_20170330_0742'),
    ]

    operations = [
        migrations.CreateModel(
            name='Flag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'verbose_name': 'Flag',
                'verbose_name_plural': 'Flags',
            },
        ),
        migrations.RemoveField(
            model_name='content',
            name='level',
        ),
        migrations.AddField(
            model_name='content',
            name='level',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='pin.Level', verbose_name='Level'),
        ),
        migrations.RemoveField(
            model_name='content',
            name='media_type',
        ),
        migrations.AddField(
            model_name='content',
            name='media_type',
            field=models.ManyToManyField(blank=True, null=True, to='pin.MediaType'),
        ),
        migrations.AddField(
            model_name='flag',
            name='content',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='get_user_flag', to='pin.Content', verbose_name='Content'),
        ),
        migrations.AddField(
            model_name='flag',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='User'),
        ),
    ]

from django.db import models
from django.core.exceptions import ObjectDoesNotExist

from haystack import signals

from .models import Content, Flag, Like
from applications.accounts.models import UserTracking, User


class ContentOnlySignalProcessor(signals.RealtimeSignalProcessor):

    def setup(self):
        # Listen only to the ``User`` model.
        models.signals.post_save.connect(self.handle_save, sender=Content)
        models.signals.post_delete.connect(self.handle_delete, sender=Content)

        models.signals.post_save.connect(self.handle_save, sender=UserTracking)
        models.signals.post_delete.connect(self.handle_delete, sender=UserTracking)

        models.signals.post_save.connect(self.handle_save, sender=User)
        models.signals.post_delete.connect(self.handle_delete, sender=User)

        models.signals.post_save.connect(self.handle_save, sender=Flag)
        models.signals.post_delete.connect(self.handle_delete, sender=Flag)

        models.signals.post_save.connect(self.handle_save, sender=Like)
        models.signals.post_delete.connect(self.handle_delete, sender=Like)

    def teardown(self):
        # Disconnect only for the ``User`` model.
        models.signals.post_save.disconnect(self.handle_save, sender=Content)
        models.signals.post_delete.disconnect(self.handle_delete, sender=Content)

        models.signals.post_save.disconnect(self.handle_save, sender=UserTracking)
        models.signals.post_delete.disconnect(self.handle_delete, sender=UserTracking)

        models.signals.post_save.disconnect(self.handle_save, sender=User)
        models.signals.post_delete.disconnect(self.handle_delete, sender=User)

        models.signals.post_save.disconnect(self.handle_save, sender=Flag)
        models.signals.post_delete.disconnect(self.handle_delete, sender=Flag)

        models.signals.post_save.disconnect(self.handle_save, sender=Like)
        models.signals.post_delete.disconnect(self.handle_delete, sender=Like)

    def handle_save(self, sender, instance, **kwargs):
        """
        Given an individual model instance, determine which backends the
        update should be sent to & update the object on those backends.
        Case: Content
        1)Update the index
        Case: UserTracking
        1)Set the sender as Content
        2)Set the correct content from user Tracking object
        Case: User
        1)Set the sender as Content
        2)Update all the index of content uploaded by that user(To make the preferences in effect)
        """
        sender = instance.__class__
        instances = [instance]
        if UserTracking == sender:
            try:
                instances = [instance.content]
            except ObjectDoesNotExist:
                # No profile attached, no need to index anything
                pass
        elif User == sender:
            try:
                sender = Content
                instances = instance.user_details.all()
            except ObjectDoesNotExist:
                # No profile attached, no need to index anything
                pass
        elif sender in [Like, Flag]:
            try:
                sender = Content
                instances = [instance.content]
            except ObjectDoesNotExist:
                # No profile attached, no need to index anything
                pass
        for content in instances:
            using_backends = self.connection_router.for_write(instance=content)
            for using in using_backends:
                try:
                    index = self.connections[using].get_unified_index().get_index(sender)
                    index.update_object(content, using=using)
                except Exception:
                    # TODO: Maybe log it or let the exception bubble?
                    pass

__author__ = 'sayone'

from django.core.management.base import NoArgsCommand

from applications.pin.models import ContentType, ContentTypeImage


class Command(NoArgsCommand):
    def handle_noargs(self, **options):
        self.assign_content_type_image_to_many()

    def assign_content_type_image_to_many(self):
        contents = ContentType.objects.all()
        for content in contents:
            ContentTypeImage.objects.create(image=content.image, content_type=content)
            print("reassigned contnet type images...")
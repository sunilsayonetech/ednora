from django.contrib import admin

# Register your models here.

from .models import Pin, Content, ContentType, Folder, Tag, Like, UserPinUploadTracking, Level, LearningObjective,\
    LearningObjectiveCode, Comment, ContentTypeImage
from .models import UserRecommendation, UserWeeklyEmailAlertRecommendation, UserPinUploadTrackingStat,Flag, MediaType


class ContentAdminModel(admin.ModelAdmin):
    search_fields = ['title', 'base_url', 'absolute_url', 'tags__name', 'content_type__name' , 'user__first_name']
    list_display = ['title', 'user', 'content_type', 'created', 'flag_count', 'show_resource_url']
    filter_horizontal = ['grade', 'subject', 'content_level', 'objective', 'content_learning_objective_code', 'tags']

    def flag_count(self, obj):
        return obj.get_user_flag.count()

    flag_count.admin_order_field = 'get_user_flag'

    def show_resource_url(self, obj):
        return '<a href="%s" target="_blank">View</a>' % (obj.get_absolute_url())
    show_resource_url.allow_tags = True


class FolderAdminModel(admin.ModelAdmin):
    search_fields = ['user__first_name', 'name']
    list_display = ['name', 'user']


class LevelAdminModel(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name', ]


class TagAdmin(admin.ModelAdmin):
    search_fields = ['name']


class PinAdmin(admin.ModelAdmin):
    list_display = ('content', 'folder')


class CommentAdmin(admin.ModelAdmin):
    list_display = ('user', 'comment_text', 'comment_date')

class ContentTypeImageInline(admin.StackedInline):
    model = ContentTypeImage
    can_delete = False
    extra = 3

    def get_form(self, request, obj=None, **kwargs):  # Just added this override
        form = super(ContentTypeImageInline, self).get_form(request, obj, **kwargs)
        form.widget.can_add_related = False
        return form

class ContentTypeAdmin(admin.ModelAdmin):

    fields = ('name',)
    list_display = ['name']
    readonly_fields = ('image_tag',)
    inlines = [
        ContentTypeImageInline,
    ]


class LikeAdmin(admin.ModelAdmin):
    list_display = ['user', 'content']


class UserWeeklyEmailAlertRecommendationAdmin(admin.ModelAdmin):
    list_display = ['user', 'is_email_send']


class LearningObjectiveAdmin(admin.ModelAdmin):

    search_fields = ['name', ]


class LearningObjectiveCodeAdmin(admin.ModelAdmin):

    search_fields = ['code', ]


admin.site.register(Pin, PinAdmin)
admin.site.register(Content, ContentAdminModel)
admin.site.register(ContentType, ContentTypeAdmin)
admin.site.register(Folder, FolderAdminModel)
admin.site.register(Tag, TagAdmin)
admin.site.register(Like, LikeAdmin)
admin.site.register(UserPinUploadTracking)
# admin.site.register(UserRecommendation)
admin.site.register(UserWeeklyEmailAlertRecommendation, UserWeeklyEmailAlertRecommendationAdmin)
admin.site.register(UserPinUploadTrackingStat)
admin.site.register(Level, LevelAdminModel)
admin.site.register(LearningObjective, LearningObjectiveAdmin)
admin.site.register(LearningObjectiveCode, LearningObjectiveCodeAdmin)
admin.site.register(MediaType)
admin.site.register(Flag)
admin.site.register(Comment, CommentAdmin)

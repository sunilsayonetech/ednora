from django import template
from django.utils.html import format_html

register = template.Library()


@register.assignment_tag(takes_context=True)
def get_counter_value_close_tags(context, obj):
    """
    to check the whether the content is liked or not

    :param context:
    :param obj:
    :return:
    """
    try:
        if int(obj) in [3, 6, 9]:
            return format_html("</tr></table><br/>")
        else:
             return " "

    except Exception as e:
        return " "

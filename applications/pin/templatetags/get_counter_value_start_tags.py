from django import template
from applications.pin.models import Like
from django.utils.html import format_html

register = template.Library()


@register.assignment_tag(takes_context=True)
def get_counter_value_start_tags(context, obj):
    """
    to check the whether the content is liked or not

    :param context:
    :param obj:
    :return:
    """
    try:
        if int(obj) in [1, 4, 7]:
            return format_html('<table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" data-start><tr>')
        else:
             return " "

    except Exception as e:
        return " "

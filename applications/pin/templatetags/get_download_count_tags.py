from django import template

from applications.api.ContentSerializers import ContentDetailSerializer
register = template.Library()


@register.simple_tag
def get_download_count(obj):
    return ContentDetailSerializer().get_download_count(obj)
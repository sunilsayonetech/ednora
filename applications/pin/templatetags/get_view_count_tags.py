from django import template
from applications.api.ContentSerializers import ContentDetailSerializer
register = template.Library()


@register.simple_tag
def get_view_count(obj):
    return ContentDetailSerializer().get_view_count(obj)
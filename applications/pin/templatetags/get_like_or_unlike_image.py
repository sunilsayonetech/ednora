from django import template
from applications.pin.models import Like,Flag

register = template.Library()


@register.assignment_tag(takes_context=True)
def get_like_or_unlike_image(context, obj):
    """
    to check the whether the content is liked or not

    :param context:
    :param obj:
    :return:
    """
    request = context['request']
    try:
        Like.objects.get(user=request.user, content=obj)
        return 'background:rgba(0,86,151,1); color:#fff!important; margin-top:2px'
    except Like.DoesNotExist:
        return ' '

@register.assignment_tag(takes_context=True)
def get_flag_or_unflag_image(context, obj):
    """
    to check the whether the content is flagged or not

    :param context:
    :param obj:
    :return:
    """
    request = context['request']
    try:
        obj = Flag.objects.filter(user=request.user, content=obj)
        if len(obj)>0:
        # return 'background-position:0 -41px;'
            return 1
        else:
            return ' '
    except Flag.DoesNotExist:
        return ' '


@register.simple_tag
def get_pin_order(counter, page):
    if not page:
        return counter
    return 9*(int(page)-1)+counter
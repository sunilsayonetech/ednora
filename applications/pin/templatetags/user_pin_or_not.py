from django import template
from applications.pin.models import Like

register = template.Library()


@register.assignment_tag(takes_context=True)
def user_pin_or_not(context, obj):
    """
    to check the whether the content is liked or not

    :param context:
    :param obj:
    :return:
    """
    request = context['request']
    folders_obj = request.user.get_folder.all()
    content_id_list = []
    for folder in folders_obj:
        pin_obj = folder.get_pin.all()
        for pin in pin_obj:
            pin_id = pin.content.id
            content_id_list.append(pin_id)
    if obj.id not in content_id_list:
        return False
    return True



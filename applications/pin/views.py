import json
import urllib
import re
import os
import shutil
import random
from itertools import chain
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
import time
from random import randint

from urllib.parse import urlparse
from haystack.forms import SearchForm
from haystack.query import SearchQuerySet, EmptySearchQuerySet
from sorl.thumbnail import get_thumbnail
from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.utils.encoding import smart_str
from django.views.generic import TemplateView
from django.views.generic import View
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from haystack.generic_views import SearchView
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.shortcuts import render,get_object_or_404, render_to_response
from django.contrib.sites.models import Site
from django.template import loader, Context
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.db.models import Q
from django.db.models import Count
from django.db.models import BooleanField, Case, When

from rest_framework.views import APIView
from tld import get_tld
from applications.accounts.utils import get_client_ip
from applications.accounts.models import UserDeviceTracking
from readability.read import readability
from .models import Folder, Pin, ContentType, Content, Tag, Like,Flag, LearningObjective, LearningObjectiveCode, Level, MediaType
from .models import UserPinUploadTracking,UserRecommendation,UserWeeklyEmailAlertRecommendation,UserPinUploadTrackingStat, Comment
from applications.pin.utils import (get_website_screenshot, get_pdf_screenshot,
                                    get_pdf_info,is_bot_request,get_file_screenshot, get_file_preview, get_url_preview)
from postr.settings import PROJECT_DIR

from applications.accounts.models import User
from applications.accounts.models import UserInvites as ui,UserFilterStat, Grade, Subject, State, District
from applications.accounts.models import UserShareTracking,UserSearchTracking,UserFilterTracking,UserWeeklyEmailAlert,UserTracking
from applications.accounts.views import LandingPageView

from .service import TagService

def verified_required(function):
    """
    to check the user is verified or not
    :param function:
    :return:
    """
    def wrapper(request, *args, **kw):
        user = request.user
        if user.verified:
            return function(request, *args, **kw)
        else:
            return HttpResponseRedirect(reverse('landing_page'))
    return wrapper


class CreateFolderView(View):

    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CreateFolderView, self).dispatch(*args, **kwargs)

    def post(self, request):
        ctx = {}
        post = request.POST.copy()
        folder_name = post.get('folder_name').strip()
        if folder_name != '':
            lenth_of_string = len(folder_name)
            if lenth_of_string >= 20:
                ctx['status'] = 'success'
                ctx['length'] = True
                return HttpResponse(json.dumps(ctx), content_type='application/json')
            try:
                Folder.objects.get(user=request.user, name=folder_name)
                ctx['status'] = 'success'
                ctx['exists'] = True
                return HttpResponse(json.dumps(ctx), content_type='application/json')
            except Folder.DoesNotExist:
                folder_obj = Folder.objects.create(user=request.user, name=folder_name)
                ctx['status'] = 'success'
                ctx['done'] = True
                ctx['slug'] = folder_obj.id

                return HttpResponse(json.dumps(ctx), content_type='application/json')
        ctx['status'] = 'success'
        ctx['empty'] = True
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class FolderDescriptionView(TemplateView):
    template_name = 'folder/folderview.html'

    @method_decorator(csrf_exempt)
    @method_decorator(verified_required)
    def dispatch(self, *args, **kwargs):
        return super(FolderDescriptionView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(FolderDescriptionView, self).get_context_data(**kwargs)
        slug = self.kwargs['slug']
        folder_obj = get_object_or_404(Folder, user=self.request.user, slug=slug)
        pins = Content.objects.filter(get_content__folder=folder_obj).order_by('-get_content__created')
        context['folder'] = folder_obj
        context['pins'] = pins
        context['pin_no'] = pins.count()
        return context

    def get(self, request, *args, **kwargs):
        super(FolderDescriptionView, self).get(request, *args, **kwargs)
        context = self.get_context_data(**kwargs)
        page_template = 'home/pagination_snippet_home.html'
        context.update({'page_template': page_template})
        if request.is_ajax():
            template = page_template
        else:
            template = self.template_name
        return render_to_response(template, context, context_instance=RequestContext(request))

    def post(self, request, slug):
        ctx = {}
        post = request.POST.copy()
        if post.get('ds') == 'delete':
            user_obj = User.objects.get(id=post.get('user_id'))
            #while deleting folder also delete resource if contains only in this folder
            pins = Pin.objects.filter(folder__slug=self.kwargs['slug'], folder__user=request.user)
            for pin in pins:
                try:
                    content_pin = Pin.objects.get(content_id=pin.content.id)
                    content = Content.objects.get(pk=content_pin.content.id, user=request.user)
                    content.delete()
                except Exception as e:
                    print(e)
            Folder.objects.filter(user=user_obj, slug=self.kwargs['slug']).delete()
            ctx['status'] = 'success'
            return HttpResponse(json.dumps(ctx), content_type='application/json')
        else:
            folder_name = post.get('folder_name').strip()
            if folder_name == '':
                ctx['status'] = 'success'
                ctx['empty'] = True
                return HttpResponse(json.dumps(ctx), content_type='application/json')
            length_of_string = len(folder_name)
            if length_of_string >= 20:
                ctx['status'] = 'success'
                ctx['length'] = True
                return HttpResponse(json.dumps(ctx), content_type='application/json')
            user_obj = User.objects.get(id=post.get('user_id'))
            name_exists = Folder.objects.filter(user=user_obj, name=folder_name).exists()
            if name_exists:
                ctx['status'] = 'success'
                ctx['name_exists'] = True
                return HttpResponse(json.dumps(ctx), content_type='application/json')
            Folder.objects.filter(user=user_obj, slug=self.kwargs['slug']).update(name=folder_name)
            ctx['status'] = 'success'
            ctx['done'] = True
            ctx['slug'] = self.kwargs['slug']
            return HttpResponse(json.dumps(ctx), content_type='application/json')


class RepinView(View):

    def dispatch(self, *args, **kwargs):
        return super(RepinView, self).dispatch(*args, **kwargs)

    def post(self, request):
            ctx = {}
            post = request.POST.copy()
            print (request.POST)
            content_id = post.get('content')
            folder = post.get('folder')
            tags = post.get('tags')
            folder_obj = Folder.objects.get(user=request.user, name=folder)
            content_obj = Content.objects.get(id=content_id)
            new_tags = []

            '''
            record re-pick activity
            '''
            if tags:
                tags = [x.strip() for x in tags.split(',')]
                for tag in tags:
                    if tag.strip():
                        tag_obj = Tag.objects.get_or_create(name=tag)
                        content_obj.tags.add(tag_obj[0].id)
                        new_tags.append(tag_obj[0].id)

            p, created = Pin.objects.get_or_create(folder=folder_obj, content=content_obj)
            tags = Tag.objects.filter(id__in=new_tags)
            if created:
                ctx['status'] = 'success'
                ctx['done'] = True
                ut = UserTracking()
                ut.user = request.user
                ut.type = 're-pick'
                ut.content = content_obj
                ut.save()
                ut.tags.add(*tags)
            else:
                ctx['status'] = 'success'
                ctx['msg'] = 'You have already saved this resource in this folder. Please select another folder.'
                ctx['done'] = True


            return HttpResponse(json.dumps(ctx), content_type='application/json')


class GetContentView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(GetContentView, self).dispatch(*args, **kwargs)

    def post(self, request):
        if 'pdf' in request.session:
            del request.session['pdf']
        if 'website' in request.session:
            del request.session['website']
        try:

            ctx = {}
            post = request.POST.copy()
            url = post.get('value')
            url = url.strip()
            if 'http' in url:
                pass
            else:
                url = "http://"+url

            '''
            if url has extension other than .pdf,.jpg,.jpeg,.png,.gif
            '''
            for i in ['.mov', '.flv', '.mp3', '.mp4', '.mkv', '.vob', '.wma']:

                if i in url:
                    try:
                        ext = i.split('.')[1]
                        ctx['status'] = 'success'
                        ctx['done'] = True
                        ctx['title'] = " "
                        ctx['content_text'] = ' '
                        return HttpResponse(json.dumps(ctx), content_type='application/json')
                    except:
                        pass

            '''
            if url is an image
            '''
            for i in ['.jpg', '.jpeg', '.png', '.gif']:
                if i in url:
                    ctx['status'] = 'success'
                    ctx['done'] = True
                    ctx['title'] = " "
                    ctx['content_text'] = ' '
                    return HttpResponse(json.dumps(ctx), content_type='application/json')

            '''
            if url is a pdf
            '''
            u = ""
            if '.pdf' in url:

                con = get_pdf_info(url)
                ctx['status'] = 'success'
                ctx['done'] = True
                if con['title']:
                    ctx['title'] = con['title']
                else:
                    ctx['title'] = " "

                if con['description']:
                    ctx['content_text'] = con['description'][:400]
                else:
                    ctx['content_text'] = " "

                return HttpResponse(json.dumps(ctx), content_type='application/json')

            """
            if url is a youtube url
            """

            title, article, image_list = readability(url)

            if article:
                article = article.replace('\n\n', '\n')
                article = article.lstrip()
                article = article.lstrip("\n")
                article = article.rstrip()
                article = article.rstrip("\n")

            try:
                regex = re.compile(
                    "^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$")
                if regex.match(url):

                    ctx['status'] = 'success'
                    ctx['done'] = True
                    if title:
                        ctx['title'] = title
                    else:
                        ctx['title'] = " "

                    if article:
                        ctx['content_text'] = article[:400]
                    else:
                        ctx['content_text'] = " "
                    return HttpResponse(json.dumps(ctx), content_type='application/json')

            except Exception as e: print(e)

            """
            readability extracted images and custom extracted images
            """

            ctx['status'] = 'success'
            ctx['done'] = True
            ctx['title'] = title
            # # Limit to 50 words
            # article = ' '.join(article.split()[:50])
            ctx['content_text'] = article
            return HttpResponse(json.dumps(ctx), content_type='application/json')
        except Exception as e:
              print(e)
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class GetTagtView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(GetTagtView, self).dispatch(*args, **kwargs)

    def post(self, request):
        try:
            ctx = {}
            post = request.POST.copy()
            url = post.get('value')
            url = url.strip()
            if 'http' in url:
                pass
            else:
                url ="http://"+ url
            '''
            if url is an pdf
            '''
            if '.pdf' in url:

                con = get_pdf_info(url)
                ctx['status'] = 'success'
                ctx['done'] = True
                if con['title']:
                    ctx['title'] = con['title']
                else:
                     ctx['title'] = ""
                if con['description']:
                    ctx['content_text'] = con['description'][:400]
                else:
                     ctx['content_text'] = ""


                service = TagService(con['title'], article=con['description'])
                tags = service.tags_selected
                ctx['tags'] = ', '.join(tags)
                return HttpResponse(json.dumps(ctx), content_type='application/json')

            title, article, image_list = readability(url)
            service = TagService(title, article=article)
            tags = service.tags_selected

            ctx['tags'] = ', '.join(tags)
            return HttpResponse(json.dumps(ctx), content_type='application/json')

        except Exception as e:
            return HttpResponse(json.dumps(ctx), content_type='application/json')


class GetContentImageView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(GetContentImageView, self).dispatch(*args, **kwargs)

    def url_is_https(self, url):
        parse_object = urlparse(url)
        scheme = parse_object.scheme
        if scheme == 'http' or not scheme:
            return False
        else:
            return True

    def post(self, request):
        preview = None
        ctx = {}
        url = request.POST.get('value')
        if not self.url_is_https(url):
            preview = get_url_preview(url)
        image_urls = []
        if preview:
            image_urls = [preview]
            ctx['status'] = 'success'
        else:
            ctx['status'] = 'fail'
        ctx['html'] = render_to_string('image_from.html', {'image_urls': image_urls, 'status': ctx['status']})
        return HttpResponse(json.dumps(ctx), content_type='application/json')

        # TODO remove if not using
        # if 'pdf' in request.session:
        #     del request.session['pdf']
        #
        # if 'website' in request.session:
        #     del request.session['website']
        #
        # post = request.POST.copy()
        # url = post.get('value')
        # url = url.strip()
        # if 'http' in url:
        #     pass
        # else:
        #     url = "http://" + url
        #
        # ctx = {}
        #
        # '''
        # if url has extension other than .pdf,.jpg,.jpeg,.png,.gif
        # '''
        # '''
        # video files
        # '''
        # for i in ['.mov', '.flv',  '.mp4', '.mkv', '.vob', '.wma']:
        #
        #     if i in url:
        #         try:
        #             ext = i.split('.')[1]
        #             image = '/static/assets/img/video.png'
        #             ctx['status'] = 'success'
        #             ctx['html'] = render_to_string('image_from.html', {'image_urls': [image]})
        #             return HttpResponse(json.dumps(ctx), content_type='application/json')
        #         except:
        #             pass
        #
        # '''
        # audio files files
        # '''
        # for i in ['.mp3']:
        #
        #     if i in url:
        #         try:
        #             ext = i.split('.')[1]
        #             image = '/static/assets/img/audio.png'
        #             ctx['status'] = 'success'
        #             ctx['html'] = render_to_string('image_from.html', {'image_urls': [image]})
        #             return HttpResponse(json.dumps(ctx), content_type='application/json')
        #         except:
        #             pass
        #
        # """
        # if url is an image
        # """
        # for i in ['.jpg', '.jpeg', '.png', '.gif']:
        #     if i in url:
        #         ctx['status'] = 'success'
        #         ctx['html'] = render_to_string('image_from.html', {'image_urls': [url]})
        #         return HttpResponse(json.dumps(ctx), content_type='application/json')
        #
        # '''
        # if url is a pdf
        # '''
        # u = ""
        # if '.pdf' in url:
        #     try:
        #         url = url.replace("https", "http")
        #         f = get_pdf_screenshot(url)
        #         if '/media' in f.name:
        #             u = "/media" + f.name.split("/media")[1]
        #         else:
        #             u = "/static" + f.name.split("/static")[1]
        #         request.session['pdf'] = f.name
        #     except Exception as e:
        #         print(e)
        #     ctx['status'] = 'success'
        #     ctx['html'] = render_to_string('image_from.html', {'image_urls': [u]})
        #     return HttpResponse(json.dumps(ctx), content_type='application/json')
        #
        # """
        # if url is a youtube url
        #
        # """
        #
        # try:
        #     youtube_url = ''
        #     regex = re.compile("^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$")
        #     if regex.match(url):
        #         if 'embed/' in url:
        #             youtube_url = url.split('embed/')[1].split('?')
        #         elif 'list' in url:
        #                 youtube_url = url.split('v=')[1].split('&')[0]
        #         else:
        #              youtube_url = url.split('v=')[1]
        #         if youtube_url:
        #             youtube_url = 'https://img.youtube.com/vi/'+youtube_url+'/maxresdefault.jpg'
        #         else:
        #             youtube_url = get_website_screenshot(url)
        #
        #         ctx['status'] = 'success'
        #         ctx['html'] = render_to_string('image_from.html', {'image_urls': [youtube_url]})
        #         return HttpResponse(json.dumps(ctx), content_type='application/json')
        # except Exception as e:
        #     print(e)
        #
        # try:
        #     f = get_website_screenshot(url)
        #     if f:
        #         scrnsht = "/media" + f.name.split("/media")[1]
        #         image_urls = [scrnsht]
        #         request.session['website'] = f.name
        #     ctx['status'] = 'success'
        #     ctx['html'] = render_to_string('image_from.html', {'image_urls': image_urls})
        #     return HttpResponse(json.dumps(ctx), content_type='application/json')
        #
        # except Exception as e:
        #     ctx['status'] = 'success'
        #     ctx['html'] = render_to_string('image_from.html', {'image_urls': {}})
        #     return HttpResponse(json.dumps(ctx), content_type='application/json')


class UploadPin(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UploadPin, self).dispatch(*args, **kwargs)

    def post(self, request):
        ctx = {}
        file_post = request.FILES.get('file')
        post = request.POST.copy()
        url = post.get('url')
        if url is not None:
            if 'http' in url:
                pass
            else:
                url ="http://"+ url
        user_id = post.get('user_id')
        tags = post.get('tags')
        objectives = post.get('objectives')
        objectives_code = post.get('objectives_code')
        title = post.get('title')
        # content_type = post.get('content_type')
        # media_type = post.get('media_type')
        folder = post.get('folder')
        image = post.get('image')
        subjects = post.get('subjects')
        subjects = subjects.split(',')
        grade = post.get('grades')
        grade_choices = grade.replace("'", "")
        grade_choices = grade_choices.replace(",", " ")
        grade_list = [s for s in grade_choices.split()]
        res_sub = []
        for sub in subjects:
            res_sub.append(int(sub))
        # levels = post.get('levels') #removing level
        # levels = levels.split(',')
        res_lev = []
        # for level in levels:
        #     res_lev.append(int(level))
        if url is not None:
            if str(image)=='undefined' or str(image)=='' or str(image)==' ' :
                image = '/static/assets/img/content-images/'+content_type+'.png'

        article = post.get('article')
        upload_pin_route = post.get('upload_route')
        ut = UserPinUploadTracking.objects.filter(user = request.user)
        if len(ut)>0 :
            if(upload_pin_route =='BB'):
                ut[0].pin_upload_using_get_browser_button = ut[0].pin_upload_using_get_browser_button+1

            if(upload_pin_route =='UP'):
                ut[0].pin_upload_using_upload_pin_popup = ut[0].pin_upload_using_upload_pin_popup+1
            ut[0].save()
        else:
            us = UserPinUploadTracking()
            us.user = request.user
            if upload_pin_route =='BB':
                us.pin_upload_using_get_browser_button=1
                us.pin_upload_using_upload_pin_popup= 0
            if upload_pin_route =='UP':
                us.pin_upload_using_upload_pin_popup=1
                us.pin_upload_using_get_browser_button=0
            us.save()

        if url is not None:
            img_temp = NamedTemporaryFile(delete=True)
            if image.startswith("/media"):
                img_temp.write(open(PROJECT_DIR+"/ednora"+image, "rb").read())
            elif image.startswith("/static"):
                img_temp.write(open(PROJECT_DIR+"/ednora"+image, "rb").read())
            else:
                if request.POST.get('url'):

                    if 'http' in image:
                            pass
                    else:
                        if 'http' in image:
                            pass
                        else :image ="http://"+ image
                    opener = urllib.request.FancyURLopener({})
                    opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0'),('Connection', 'keep-alive')]
                    img_temp.write(opener.open(image).read())

            img_temp.flush()
            image_actual = image
            IMAGE = image_actual.rsplit('/', 1)[1]
            base_url = ""
            baseurl = get_tld(url, as_object=True)

            if baseurl.subdomain:
               base_url = baseurl.subdomain+"."+str(baseurl)

            else:
                base_url = str(baseurl)

            # if 'www' in url:
            #    base_url = 'www.'+base_url

        # content_type_obj = ContentType.objects.get(name=content_type)
        first_subject = Subject.objects.get(id=res_sub[0])
        try:
            content_type_obj = ContentType.objects.get(name=first_subject.subject)
        except:
            content_type_obj = ContentType.objects.get(name='Other')
        tags = [x.strip() for x in tags.split(',')]
        if objectives:
            objectives = [x.strip() for x in objectives.split(',')]
        if objectives_code:
            objectives_code = [x.strip() for x in objectives_code.split(',')]
        # objectives_code,created = LearningObjectiveCode.objects.get_or_create(code=objectives_code)
        # media_type = MediaType.objects.get(name=media_type)
        grade_objs= Grade.objects.filter(grade__in=grade_list)
        subject_objs= Subject.objects.filter(id__in=res_sub)
        level_objs= Level.objects.filter(id__in=res_lev)
        user = self.request.user

        if url:
            content_obj = Content.objects.create(user=user, content_type=content_type_obj, base_url=base_url,absolute_url=url, title=title, text=article)
            content_obj.image.save(IMAGE, File(img_temp))
            content_obj.save()
            content_obj.grade.add(*grade_objs)
            content_obj.subject.add(*subject_objs)
            content_obj.content_level.add(*level_objs)
        else:
            content_obj = Content.objects.create(user=user, content_type=content_type_obj, file_upload=file_post, title=title, text=article)
            content_obj.save()
            content_obj.grade.add(*grade_objs)
            content_obj.subject.add(*subject_objs)
            content_obj.content_level.add(*level_objs)

            img_temp = NamedTemporaryFile(delete=True)
            if image.startswith("/media"):
                img_temp.write(open(PROJECT_DIR+"/ednora"+image, "rb").read())
            elif image.startswith("/static"):
                img_temp.write(open(PROJECT_DIR+"/ednora"+image, "rb").read())
            image_actual = post.get('image')
            IMAGE = 'default-thumb.png'
            if image_actual:
                try:
                    IMAGE = image_actual.rsplit('/', 1)[1]
                except:
                    pass
            content_obj.image.save(IMAGE, File(img_temp))
        # content_obj.image.save(IMAGE, File(img_temp))
        for tag in tags:
            if not tag == '':
                if tag.strip():
                    tag_obj, created = Tag.objects.get_or_create(name=tag)
                    content_obj.tags.add(tag_obj)

        if objectives:
            for objective in objectives:
                if not objective == '':
                    if objective.strip():
                        objective_obj, created = LearningObjective.objects.get_or_create(name=objective)
                        content_obj.objective.add(objective_obj)

        if objectives_code:
            for code in objectives_code:
                if not code == '':
                    if code.strip():
                        objectives_code_obj, created = LearningObjectiveCode.objects.get_or_create(code=code)
                        content_obj.content_learning_objective_code.add(objectives_code_obj)

        content_obj.save()
        folder_obj = Folder.objects.get(user=request.user, name=folder)
        Pin.objects.create(folder=folder_obj, content=content_obj)
        ut = UserTracking()
        ut.user = request.user
        ut.content = content_obj
        ut.type = 'pick'
        ut.save()
        ctx['status'] = 'success'
        ctx['validated'] = True
        if 'pdf' in request.session:
            os.remove(request.session['pdf'])

        # if 'website'  in request.session:
        #     os.remove(request.session['website'])

        return HttpResponse(json.dumps(ctx), content_type='application/json')


class GetUserFolder(View):

    def get(self, request):
        ctx = {}
        user_related_folders = self.request.user.get_folder.all().order_by('-created')
        ctx['status'] = 'success'
        ctx['html'] = render_to_string('folder/user_folders.html', {'folders': user_related_folders})
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class GetUserFolderForRepin(View):
    def get(self, request):
        ctx = {}
        user_related_folders = self.request.user.get_folder.all().order_by('-created')
        ctx['status'] = 'success'
        ctx['html'] = render_to_string('folder/user_folders_for_repin.html', {'folders': user_related_folders})
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class CheckUrlInFolder(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(CheckUrlInFolder, self).dispatch(*args, **kwargs)

    def post(self, request):
        ctx = {}
        url = request.POST.get('url')
        if url is not None:
            if 'http' in url:
                pass
            else:
                url ="http://"+ url
        folder = request.POST.get('folder')
        upload_type = request.POST.get('type')
        ctx['status'] = 'pass'
        if upload_type == 'url':
            try:
                if url:
                    content = Content.objects.filter(absolute_url=url, user=request.user)
                    if content:
                        for con in content:
                            pin = Pin.objects.filter(content=con, folder__name=folder)
                            if pin:
                                ctx['status'] = 'exist_in_folder'# change this to 'fail' because functionality is not existing now
                                break
            except:
                pass
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class GetUserFolderForHomeview(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(GetUserFolderForHomeview, self).dispatch(*args, **kwargs)

    def post(self, request):
        ctx = {}
        user_related_folders = self.request.user.get_folder.all().order_by('name')
        ctx['status'] = 'success'
        ctx['html'] = render_to_string('folder/user_folders_for_home.html', {'folders': user_related_folders})
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class CustomSearchView(SearchView):

    template = 'search/search.html'
    page_template = 'home/pagination_snippet_home.html'
    paginate_by = None

    @method_decorator(csrf_exempt)
    @method_decorator(verified_required)
    def dispatch(self, *args, **kwargs):
        self.record_user_search_track()
        return super(CustomSearchView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CustomSearchView, self).get_context_data(**kwargs)
        pins = self.get_queryset(**kwargs)
        context['pins'] = self._filter_based_on_user_preference(pins)
        context['page_template'] = self.page_template
        filter = self.request.GET.get('filter')
        if filter:
            context['by_filter'] = True
        if self.request.is_ajax():
            self.template_name = self.page_template
        else:
            self.template_name = self.template
        return context

    def get_queryset(self, **kwargs):
        query = self.request.GET.get('q')
        state = self.request.GET.get('state')
        district = self.request.GET.get('district')
        grade = self.request.GET.get('grade')
        subject = self.request.GET.get('subject')
        learning = self.request.GET.get('learning')
        learning_code = self.request.GET.get('code')
        level = self.request.GET.get('level')
        resource = self.request.GET.get('resource')
        media = self.request.GET.get('media')
        if not(query or state or district or grade or subject or learning or
                learning_code or level or resource or media):
            sqs = EmptySearchQuerySet()
        else:
            sqs = SearchQuerySet()
            sqs = sqs.models(Content)
            if query:
                # form = SearchForm(self.request.GET)
                search_query = urlparse(self.request.build_absolute_uri())
                self.request.session['search_query'] = search_query.query
                for search_term in query.split():
                    sqs = sqs.filter_or(content=search_term)
            if state:
                sqs = sqs.filter(state=state)
            if district:
                sqs = sqs.filter(district__exact=district)
            if subject:
                subject_ids = [Subject.objects.get(id=each).id for each in subject.split(",") if each]
                sqs = sqs.filter(subject__in=subject_ids)
            if grade:
                grade_ids = [Grade.objects.get(id=each).id for each in grade.split(",") if each]
                sqs = sqs.filter(grade__in=grade_ids)
            if learning:
                learning_objective_ids = [LearningObjective.objects.get(name=each).id for each in learning.split(",") if each]
                sqs = sqs.filter(objective__in=learning_objective_ids)
            if learning_code:
                learning_objective_code_ids = [LearningObjectiveCode.objects.get(code=each).id for each in learning_code.split(",") if each]
                sqs = sqs.filter(content_learning_objective_code__in=learning_objective_code_ids)
            if level:
                level = str(level).split(',')
                level.remove('')
                sqs = sqs.filter(content_level__in=level)
            if resource:
                resource_ids = [ContentType.objects.get(name=each).id for each in resource.split(",") if each]
                sqs = sqs.filter(content_type__in=resource_ids)
            if media:
                media_ids = [MediaType.objects.get(name=each).id for each in media.split(",") if each]
                sqs = sqs.filter(media_type__in=media_ids)
            self.record_filter_trackin(sqs.count())
        return sqs

    def _filter_based_on_user_preference(self, pins):
        user = self.request.user
        state = re.sub('[^a-zA-Z0-9 \n\.]', '', str(user.state))
        # district preference is on by default, no need of filter
        return pins.filter(Q(nation_preference=True) | Q(
            state_preference=True, state=state) | Q(
                district=user.district) | Q(user=user))\
            .order_by('flag_count_greater', '-like_count')

    def record_filter_trackin(self, count):
        filter_tracking = UserFilterTracking()
        filter_tracking.user = self.request.user
        current_filter = {}
        for key, value in self.request.GET.lists():
            if value != ['']:
                current_filter[key] = value
        filter_tracking.content_filter = current_filter
        filter_tracking.count = count
        filter_tracking.save()

    def record_user_search_track(self):
        if self.request.GET.get('q'):
            user_search_tracking_obj = UserSearchTracking()
            user_search_tracking_obj.user = self.request.user
            user_search_tracking_obj.search_keyword = self.request.GET.get('q')
            user_search_tracking_obj.save()


class UserLike(View):

    def post(self, request,  *args, **kwargs):

        ctx = {}
        print(request.POST)
        post = request.POST.copy()
        content_id = post.get('content_id')
        content_object = Content.objects.get(id=content_id)
        like_obj, created = Like.objects.get_or_create(user=request.user, content=content_object)
        like_count = content_object.get_user_like.count()
        if created:
            '''
            record unlike activity
            '''
            ut = UserTracking()
            ut.user = request.user
            ut.type = 'like'
            ut.content = content_object
            ut.save()
            ctx['liked'] = True
            ctx['count'] = like_count
            return HttpResponse(json.dumps(ctx), content_type='application/json')
        '''
        record unlike activity
        '''
        ut = UserTracking()
        ut.user = request.user
        ut.type = 'unlike'
        ut.content = content_object
        ut.save()

        like_obj.delete()
        ctx['liked'] = False
        ctx['count'] = like_count -1
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class UserFlag(View):
    def dispatch(self, *args, **kwargs):
        return super(UserFlag, self).dispatch(*args, **kwargs)

    def post(self, request,  *args, **kwargs):
        ctx = {}
        post = request.POST.copy()
        content_id = post.get('content_id')
        content_object = Content.objects.get(id=content_id)
        flag_obj, created = Flag.objects.get_or_create(user=request.user, content=content_object)
        flag_count = Flag.objects.filter(content=content_object).count()
        ut = UserTracking()
        ut.user = request.user
        ut.content = content_object
        if created:
            ctx['flagged'] = True
            ctx['count'] = flag_count
            ut.type = 'flag'
            ut.save()
            return HttpResponse(json.dumps(ctx), content_type='application/json')

        '''
        record unflagged activity
        '''
        ut.type = 'unflag'
        ut.save()

        flag_obj.delete()
        flag_count_after_delete = flag_count -1
        ctx['flagged'] = False
        ctx['count'] = flag_count_after_delete
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class UserDownload(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UserDownload, self).dispatch(*args, **kwargs)

    def post(self, request,  *args, **kwargs):
        ctx = {}
        post = request.POST.copy()
        content_id = post.get('content_id')
        dwnld_count =UserTracking.objects.filter(type='download',content = content_id).count()
        ctx['dwnld_count'] = dwnld_count + 1
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class MyLike(TemplateView):

    template_name = 'like/like.html'

    @method_decorator(verified_required)
    def dispatch(self, *args, **kwargs):
        return super(MyLike, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MyLike, self).get_context_data(**kwargs)
        pins = Content.objects.filter(get_user_like__user=self.request.user).order_by('-get_user_like__id')
        context['pins'] = pins
        context['mypin_count'] = pins.count()
        context['page_template'] = "like/paginate_snippet_like.html"
        return context

    def get(self, request, *args, **kwargs):
        super(MyLike, self).get(request, *args, **kwargs)
        context = self.get_context_data(**kwargs)
        page_template = 'like/paginate_snippet_like.html'
        context.update({'page_template': page_template})
        if request.is_ajax():
            template = page_template
        else:
            template = self.template_name
        return render_to_response(template, context, context_instance=RequestContext(request))

    def post(self, request):
        ctx = {}
        like_obj = get_object_or_404(Like, user=request.user, content__id=request.POST.get('content_id'))
        ut = UserTracking()
        ut.user = request.user
        ut.type = 'unlike'
        ut.content = Content.objects.get(id=request.POST.get('content_id'))
        ut.save()
        like_obj.delete()
        user_liked_objects = Content.objects.filter(get_user_like__user=self.request.user)
        ctx['status'] = 'success'
        ctx['count'] = user_liked_objects.count()
        ctx['html'] = render_to_string('like/paginate_snippet_like.html',
                                       {'pins': user_liked_objects, 'mypin_count': len(user_liked_objects)},
                                       context_instance=RequestContext(request))
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class UserLikeAjax(View):
    @method_decorator(verified_required)
    def dispatch(self, *args, **kwargs):
        return super(UserLikeAjax, self).dispatch(*args, **kwargs)




class DeleteContentFoder(View):
    @method_decorator(csrf_exempt)
    @method_decorator(verified_required)
    def dispatch(self, *args, **kwargs):
        return super(DeleteContentFoder, self).dispatch(*args, **kwargs)

    def post(self, request,  *args, **kwargs):
        ctx = {}
        post = request.POST.copy()
        content_id = post.get('pin_id')
        pin_object = Pin.objects.get(id=content_id)
        pin_object.delete()
        ctx['status'] = 'success'
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class Likeindetail(View):
    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(Likeindetail, self).dispatch(*args, **kwargs)

    def post(self, request,  *args, **kwargs):
        ctx = {}
        post = request.POST.copy()
        content_id = post.get('content_id')
        content_object = Content.objects.get(id=content_id)
        like_obj, created = Like.objects.get_or_create(user=request.user, content=content_object)
        if created:
            like_count = Like.objects.filter(content=content_object).count()
            ctx['liked'] = True
            ctx['count'] = like_count
            return HttpResponse(json.dumps(ctx), content_type='application/json')
        like_obj.delete()
        like_count_after_delete = Like.objects.filter(content=content_object).count()
        ctx['unliked'] = True
        ctx['count'] = like_count_after_delete
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class Flagindetail(View):
    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(Flagindetail, self).dispatch(*args, **kwargs)

    def post(self, request,  *args, **kwargs):
        ctx = {}
        post = request.POST.copy()
        content_id = post.get('content_id')
        content_object = Content.objects.get(id=content_id)
        flag_obj = Flag.objects.filter(user=request.user, content=content_object)
        # if created:
        #     flag_count = Flag.objects.filter(content=content_object).count()
        #     ctx['flagged'] = True
        #     ctx['count'] = flag_count
        #     return HttpResponse(json.dumps(ctx), content_type='application/json')
        if len(flag_obj)==0:
            Flag.objects.create(user=request.user, content=content_object)
            flag_count = Flag.objects.filter(content=content_object).count()
            ctx['flagged'] = True
            ctx['count'] = flag_count
            return HttpResponse(json.dumps(ctx), content_type='application/json')
        else:
            flag_obj.delete()
            flag_count_after_delete = Flag.objects.filter(content=content_object).count()
            ctx['unflagged'] = True
            ctx['count'] = flag_count_after_delete
            return HttpResponse(json.dumps(ctx), content_type='application/json')


class FlaginCheck(View):
    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(FlaginCheck, self).dispatch(*args, **kwargs)

    def post(self, request,  *args, **kwargs):
        ctx = {}
        post = request.POST.copy()
        content_id = post.get('content_id')
        content_object = Content.objects.get(id=content_id)
        try:
            flag_obj = Flag.objects.filter(user=request.user, content=content_object)[0]
            ctx['flag_status'] = True
        except:
                ctx['flag_status'] = False
        return HttpResponse(json.dumps(ctx), content_type='application/json')
class Downloadindetail(View):
    @method_decorator(verified_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(Downloadindetail, self).dispatch(*args, **kwargs)

    def post(self, request,  *args, **kwargs):
        ctx = {}
        post = request.POST.copy()
        content_id = post.get('content_id')
        dwnld_count =UserTracking.objects.filter(type='download',content = content_id).count()
        ctx['dwnld_count'] = dwnld_count + 1
        return HttpResponse(json.dumps(ctx), content_type='application/json')

from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from applications.accounts.forms import SignupForm, LoginForm


class UserInviteView(TemplateView):
    page_template = 'home/pagination_snippet_home.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UserInviteView, self).dispatch(*args, **kwargs)

    def get_template_names(self):
        if self.request.user.is_anonymous():
            return ['register/register.html']
        else:
            return ['home/home.html']

    def get_context_data(self, **kwargs):
        context = super(UserInviteView, self).get_context_data(**kwargs)

        if self.request.user.is_anonymous():
            invite_id = int(self.request.GET.get('p'))
            try:
                if not is_bot_request(self.request):
                    user_invite_objects = ui.objects.get(pk=invite_id)
                    user_invite_objects.is_invite_accepted = True
                    user_invite_objects.save()

                context['welcome_message_title'] = 'Invitation to join Ednora'
                context['welcome_message'] = str(user_invite_objects.user.first_name)+"has invited you to Ednora!"
                context['invited_user'] = str(user_invite_objects.user)
                context['no_warpper'] = True
                flag = self.request.session.get('data', None)
                if flag:
                    context['form'] = SignupForm(initial=flag)
                else:
                    context['form'] = SignupForm()
                context['login_form'] = LoginForm()
                return context
            except Exception as e:
                print(e)
        else:
            try:
                invite_id = int(self.request.GET.get('p'))
                user_invite_objects = ui.objects.get(pk=invite_id)
                user_invite_objects.is_invite_accepted = True
                user_invite_objects.save()
                context['welcome_message'] = str(user_invite_objects.user.first_name)+"  has invited you to Ednora!"
                context['invited_user'] = str(user_invite_objects.user.first_name)
                context['page_template'] = self.page_template
                udt = UserDeviceTracking.objects.filter(os=self.request.user_agent.os.family,device = self.request.user_agent.device.family,browser = self.request.user_agent.browser.family,ip=get_client_ip(self.request),user = self.request.user)

                if len(udt) == 0:
                    UDT = UserDeviceTracking()
                    UDT.user = self.request.user
                    UDT.device = self.request.user_agent.device.family
                    UDT.os = self.request.user_agent.os.family
                    UDT.browser = self.request.user_agent.browser.family
                    UDT.ip = get_client_ip(self.request)
                    UDT.save()

            except Exception as e:
                print(e)
            user = User.objects.get(slug=kwargs['slug'])
            contents_objs = Content.get_user_accessible_contents(user)
            context['pins'] = contents_objs
        return context

    def post(self, request, *args, **kwargs):
        ctx = {}
        post = request.POST.copy()
        content_type_id = post.get('id')
        my_folders = self.request.user.get_folder.all()
        my_pins = Content.objects.filter(content_type_id__in=content_type_id).distinct()
        ctx['status'] = 'success'
        ctx['html'] = render_to_string('folder/filter_folder.html', {'pins': my_pins, 'folder_obj': my_folders})
        return HttpResponse(json.dumps(ctx), content_type='application/json')




class UserInvites(TemplateView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
            return super(UserInvites, self).dispatch(*args, **kwargs)
    def post(self, request, *args, **kwargs):
          emails = request.POST.get('mail')
          channel = request.POST.get('channel')
          emails = emails.split(',')
          msg = ''
          num = 0
          invite_id = 0
          for email in emails:
              try:
                  invite_result = ui.objects.filter(user=request.user, email=email, channel=channel,is_invite_accepted = False)
                  invite_count = len(invite_result)
                  if invite_count == 0:
                          userobj = ui()
                          userobj.user = request.user
                          userobj.email = email
                          userobj.channel = channel
                          userobj.save()
                          invite_id =  userobj.id
                  else:
                      invite_id = invite_result[0].id

                  num = len(User.objects.filter(email = email))
                  if num >=1:
                        msg = 'View Picks'
                  else:
                        msg = 'Accept Invite'
                  slug  = request.user.slug
                  site = Site.objects.get_current()
                  url = str(site)+"/upv/"+str(slug)+'/?p='+str(invite_id)
                  user = request.user
                  p = Pin.objects.filter(folder__user =user ).select_related('content').values_list('content',flat = True)
                  pins =  Content.objects.filter(pk__in=p)
                  t = loader.get_template('email/invite_email_template.html')
                  c = Context({'user': user, 'link': url, 'site': site.name, 'msg' : msg, 'pins':pins[:3]})
                  mail = EmailMessage(request.user.first_name+' has invited you to Ednora', t.render(c), request.user.first_name + ' via Ednora <no-reply@ednora.com>', (email, ))
                  mail.content_subtype = 'html'
                  mail.send()
              except Exception as e:print(e)
          ctx={}
          ctx['done'] =  'True'
          return HttpResponse(json.dumps(ctx), content_type='application/json')


class SharedPinView(TemplateView):
    """
        to display pins associated for users
    """


    template_name = 'home/home.html'


    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(SharedPinView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SharedPinView, self).get_context_data(**kwargs)

        if self.request.user.is_anonymous():
            content_share_id = int(self.request.GET.get('p'))
            try:
                if not is_bot_request(self.request):
                    content_share_objects = UserShareTracking.objects.get(pk=content_share_id)
                    content_share_objects.is_visited = True
                    content_share_objects.save()

                context['welcome_message_title'] = 'Picks from around the web for kids'
                context['welcome_message'] = str(content_share_objects.user.first_name)+"  has shared a pick with you!"
            except Exception as e:
                print(e)

            if kwargs['slug']:
                content_type_objects = Content.objects.all().distinct().order_by('-created')
                content_type_objects = content_type_objects.filter(slug=kwargs['slug'])
                content_type_obj = ContentType.objects.all()
                if '/resource/' in self.request.path:
                    dwnld_count = UserTracking.objects.filter(type='download', content=content_type_objects[0]).count()
                    view_count = UserTracking.objects.filter(type='cte', content=content_type_objects[0]).count()
                    context['dwnld_count'] = dwnld_count
                    context['view_count'] = view_count
                context['pins'] = content_type_objects
                context['content_type'] = content_type_obj
                if len(content_type_objects) ==0:
                    context['no_slug'] = 'Oops!! No Pick Found'
                return context

        else:
            content_share_id = int(self.request.GET.get('p'))
            try:
                content_share_objects = UserShareTracking.objects.get(pk=content_share_id)
                content_share_objects.is_visited = True
                content_share_objects.save()
                context['welcome_message'] = str(content_share_objects.user.first_name)+"  has shared a pick with you!"
                if kwargs['slug']:
                    content_type_objects = Content.objects.filter(slug=kwargs['slug'])
                    content_type_obj = ContentType.objects.all()
                    if '/resource/' in self.request.path:
                        dwnld_count = UserTracking.objects.filter(type='download', content=content_type_objects[0]).count()
                        view_count = UserTracking.objects.filter(type='cte', content=content_type_objects[0]).count()
                        context['dwnld_count'] = dwnld_count
                        context['view_count'] = view_count
                    context['pins'] = content_type_objects
                    context['content_type'] = content_type_obj
                    if len(content_type_objects) ==0:
                        context['no_slug'] = 'Oops!! No Pick Found'
                    return context

            except Exception as e: print(e)


        """
        pass all pins in backend as context and filter
        """

        try:
            content_name = self.request.GET.get('q', '')

            if content_name:
                user_filter_tracking_obj_list = UserFilterTracking.objects.filter(content_filter = content_name)
                if len(user_filter_tracking_obj_list)>0 :
                    user_filter_tracking_obj_list[0].count =  user_filter_tracking_obj_list[0].count+1
                    user_filter_tracking_obj_list[0].save()
                else:
                    user_filter_tracking_obj = UserFilterTracking()
                    user_filter_tracking_obj.content_filter = content_name
                    user_filter_tracking_obj.count = 1
                    user_filter_tracking_obj.save()

            udt = UserDeviceTracking.objects.filter(os=self.request.user_agent.os.family, device = self.request.user_agent.device.family,browser = self.request.user_agent.browser.family,ip=get_client_ip(self.request),user = self.request.user)

            if len(udt)==0:
                UDT = UserDeviceTracking()
                UDT.user = self.request.user
                UDT.device = self.request.user_agent.device.family
                UDT.os = self.request.user_agent.os.family
                UDT.browser = self.request.user_agent.browser.family
                UDT.ip = get_client_ip(self.request)
                UDT.save()
            if content_name:
                content_type_obj = ContentType.objects.all()
                my_folders = self.request.user.get_folder.all().order_by('name')
                content_obj = ContentType.objects.get(slug=content_name)
                ur = UserRecommendation.objects.get(user = self.request.user)
                my_pins = Content.objects.filter(pk__in=ur.recommended_pins .values_list('id',flat=True),content_type =content_obj )
                mypin_count = len(my_pins)
                if mypin_count == 0:
                    context['mypin_count'] = mypin_count
                    context['content_name'] = content_name
                    context['folder_obj'] = my_folders
                    context['pins'] = my_pins
                    context['content_type'] = content_type_obj
                context['folder_obj'] = my_folders
                context['pins'] = my_pins
                context['content_type'] = content_type_obj
                if '/resource/' in self.request.path:
                    dwnld_count = UserTracking.objects.filter(type='download', content=content_type_objects[0]).count()
                    view_count = UserTracking.objects.filter(type='cte', content=content_type_objects[0]).count()
                    context['dwnld_count'] = dwnld_count
                    context['view_count'] = view_count
                return context
            if kwargs['slug']:
                my_folders = self.request.user.get_folder.all().order_by('name')
                content_type_objects = Content.objects.all().distinct().order_by('-created')
                content_type_objects = content_type_objects.filter(slug=kwargs['slug'])
                content_type_obj = ContentType.objects.all()
                context['pins'] = content_type_objects
                context['content_type'] = content_type_obj
                context['folder_obj'] = my_folders
                if '/resource/' in self.request.path:
                    dwnld_count = UserTracking.objects.filter(type='download', content=content_type_objects[0]).count()
                    view_count = UserTracking.objects.filter(type='cte', content=content_type_objects[0]).count()
                    context['dwnld_count'] = dwnld_count
                    context['view_count'] = view_count
                if len(content_type_objects) ==0:
                    context['no_slug'] = 'Oops!! No Pick Found'
                return context

            my_folders = self.request.user.get_folder.all().order_by('name')
            content_type_obj = ContentType.objects.all()
            ur = UserRecommendation.objects.get(user = self.request.user)
            context['pins'] = content_type_objects = my_pins = Content.objects.filter(pk__in=ur.recommended_pins .values_list('id',flat=True) ).distinct().order_by('-created')
            context['content_type'] = content_type_obj
            context['folder_obj'] = my_folders

        except Exception as e:print(e)
        return context

    def get(self, request, *args, **kwargs):
        super(SharedPinView, self).get(request, *args, **kwargs)
        context = self.get_context_data(**kwargs)
        page_template = 'home/pagination_snippet_home.html'
        context.update({'page_template': page_template})
        if request.is_ajax():
           template = page_template
        else:
            template = self.template_name
        return render_to_response(template, context, context_instance=RequestContext(request))

    def post(self, request, *args, **kwargs):
        ctx = {}
        post = request.POST.copy()
        content_type_id = post.get('id')
        my_folders = self.request.user.get_folder.all()
        my_pins = Content.objects.filter(content_type_id__in=content_type_id).distinct()
        ctx['status'] = 'success'
        ctx['html'] = render_to_string('folder/filter_folder.html', {'pins': my_pins, 'folder_obj': my_folders})
        return HttpResponse(json.dumps(ctx), content_type='application/json')

from applications.accounts.models import UserTracking, User, UserSearchTracking


from .serializers import UserSerializer, UserSearchTrackingSerializer, ContentSerializer, UserTrackingSerializer


class GetJSONUserTracking(View):
    @method_decorator(staff_member_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(GetJSONUserTracking, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        directory = 'user_tracking'
        ''' for user activity'''

        if not os.path.exists(directory):
            os.makedirs(directory)
        users = User.objects.filter(is_active=True).exclude(is_superuser=True)

        user_activity_dir = 'user_tracking/user_activity'
        if not os.path.exists(user_activity_dir):
            os.makedirs(user_activity_dir)

        for user in users:
            if user.is_superuser:
                continue
            if  not user.is_active:
                continue
            ua = UserTracking.objects.filter(user=user)
            filename = user_activity_dir + '/' + str(user.id)+'.json'
            file = open(filename, 'w+')
            data = ''
            for u in ua:
                user_tracking_info = UserTrackingSerializer(instance=u).data
                data += json.dumps(user_tracking_info)
                data += '\n'
            file.write(data)
            file.close()
            data = ''

        ''' for content'''
        filename = directory + '/contents.json'
        file = open(filename, 'w+')
        contents = Content.objects.all()
        data=''
        sr = ''
        for  content in contents:
            content_info = ContentSerializer(instance=content).data
            data += json.dumps(content_info)
            data += '\n'
        file.write(data)
        file.close()

        ''' for users'''
        filename = directory + '/users.json'
        user_file = open(filename, 'w+')
        data = ''
        for user in users:
            user_info = UserSerializer(instance=user).data
            data += json.dumps(user_info)
            data += '\n'

        user_file.write(data)
        user_file.close()

        ''' for users search tracking'''
        user_search_dir = 'user_tracking/user_searches'
        if not os.path.exists(user_search_dir):
            os.makedirs(user_search_dir)

        for user in users:
            data = ''
            filename = user_search_dir + '/%s.json' %(str(user.id))
            user_file = open(filename, 'w+')

            user_searches = UserSearchTracking.objects.filter(user=user)
            for search_entry in user_searches:
                search_info = UserSearchTrackingSerializer(instance=search_entry).data
                data += json.dumps(search_info)
                data += '\n'

            user_file.write(data)
            user_file.close()


        f = shutil.make_archive('user_tracking', 'zip', directory)
        fileop = open(f,'rb')
        # shutil.rmtree(directory)
        response = HttpResponse(fileop, content_type='application/zip')
        response['Content-Disposition'] = 'attachment; filename=%s' % smart_str('user_tracking.zip')

        return response


class UserShareEmail(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UserShareEmail, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        pin_slug = request.POST.get('pinslug', '')
        useremail = request.POST.get('email', '')
        content = Content.objects.filter(slug=pin_slug)[0]
        try:
            ut = UserTracking()
            ut.type = 'cts'
            ut.user = request.user
            ut.content = content
            ut.save()
            obj = UserShareTracking(user=request.user, content=content, email=useremail)
            obj.save()
            site = Site.objects.get_current()
            url = str(site)+"/resource/"+str(pin_slug)+"/?p="+str(obj.pk)
            user = request.user
            t = loader.get_template('email/share_pin_email_template.html')
            c = Context({'user': user, 'link': url, 'site': site.name, 'content': content})
            mail = EmailMessage(request.user.first_name+' has shared a pick with you on Ednora', t.render(c), request.user.first_name + ' via Ednora<no-reply@ednora.com>', (useremail, ))
            mail.content_subtype = 'html'
            mail.send()
        except Exception as e:print(e)



        ctx={}
        ctx['status'] = 'success'
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class ViewPinDetails(TemplateView):
    """
        to display pins associated for users
    """


    template_name = 'home/home.html'


    @method_decorator(csrf_exempt)
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ViewPinDetails, self).dispatch(*args, **kwargs)


    def get_context_data(self, **kwargs):
        """
        pass all pins in backend as context and filter
        """
        context = super(ViewPinDetails, self).get_context_data(**kwargs)
        redirect_url = self.request.GET.get('r', '/home/')
        context['redirect_url'] = '/home/' if 'resource' in redirect_url else redirect_url
        content_name = self.request.GET.get('q')
        if content_name:
            content_type_obj = ContentType.objects.all()
            my_folders = self.request.user.get_folder.all().order_by('name')
            content_obj = ContentType.objects.get(slug=content_name)
            content_obj_in_string = str(content_obj.id)
            my_pins = Content.objects.filter(content_type__id=content_obj_in_string).distinct().order_by('-created')
            mypin_count = Content.objects.filter(content_type__id=content_obj_in_string).distinct().count()
            if mypin_count == 0:
                context['mypin_count'] = mypin_count
                context['content_name'] = content_name
                context['folder_obj'] = my_folders
                context['pins'] = my_pins
                context['content_type'] = content_type_obj
            context['folder_obj'] = my_folders
            context['pins'] = my_pins
            context['content_type'] = content_type_obj
            return context
        if kwargs['slug']:
            my_folders = self.request.user.get_folder.all().order_by('name')
            content_type_objects = Content.objects.all().distinct().order_by('-created')
            content_type_objects = content_type_objects.filter(slug=kwargs['slug'])
            if '/resource/' in self.request.path:
                dwnld_count = UserTracking.objects.filter(type='download', content=content_type_objects[0]).count()
                view_count = UserTracking.objects.filter(type='cte', content=content_type_objects[0]).count()
                context['dwnld_count'] = dwnld_count
                context['view_count'] = view_count
            content_type_obj = ContentType.objects.all()
            context['pins'] = content_type_objects
            context['content_type'] = content_type_obj
            context['folder_obj'] = my_folders
            if len(content_type_objects) ==0:
                context['no_slug'] = 'Oops!! No Pick Found'
            return context




        return context

    def get(self, request, *args, **kwargs):
        super(ViewPinDetails, self).get(request, *args, **kwargs)
        context = self.get_context_data(**kwargs)
        page_template = 'home/pagination_snippet_home.html'
        context.update({'page_template': page_template})
        if request.is_ajax():
           template = page_template
        else:
            template = self.template_name
        return render_to_response(template, context, context_instance=RequestContext(request))

    def post(self, request, *args, **kwargs):
        ctx = {}
        post = request.POST.copy()
        content_type_id = post.get('id')
        my_folders = self.request.user.get_folder.all()
        my_pins = Content.objects.filter(content_type_id__in=content_type_id).distinct()
        ctx['status'] = 'success'
        ctx['html'] = render_to_string('folder/filter_folder.html', {'pins': my_pins, 'folder_obj': my_folders})
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class UserRecommendations(View):
    @method_decorator(staff_member_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UserRecommendations, self).dispatch(*args, **kwargs)
    def post(self, request, *args, **kwargs):
        resultfile  = request.FILES.get('jsonfile')
        UserRecommendation.objects.all().delete()
        file = resultfile.readlines()
        for line in file:
            try:
                  row = json.loads(line.decode())
                  uid = row['UID']
                  content_ids = row['Recommendations']
                  contents = Content.objects.filter(pk__in = content_ids)
                  user = User.objects.filter(pk = uid)
                  ur = UserRecommendation()
                  ur.user = user[0]
                  ur.save()
                  ur.recommended_pins.add(*contents)

            except Exception as e :
                   print(e)
                   pass


        ctx={}
        ctx['html'] = 'true'
        return HttpResponse(json.dumps(ctx), content_type='application/json')

class UserEmailRecommendations(View):
    @method_decorator(staff_member_required)
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(UserEmailRecommendations, self).dispatch(*args, **kwargs)
    def post(self, request, *args, **kwargs):
        resultfile  = request.FILES.get('emailjsonfile')
        UserWeeklyEmailAlertRecommendation.objects.all().delete()
        file = resultfile.readlines()
        for line in file:
            try:
                  row = json.loads(line.decode())
                  uid = row['UID']
                  content_ids = row['Recommendations']
                  contents = Content.objects.filter(pk__in = content_ids)
                  user = User.objects.filter(pk = uid)
                  ur = UserWeeklyEmailAlertRecommendation()
                  if UserWeeklyEmailAlert.objects.get(user=user[0]).is_subscribed==False:
                     continue
                  ur.user = user[0]
                  ur.save()
                  ur.recommended_pins.add(*contents)

            except Exception as e :
                   print(e)
                   pass


        ctx={}
        ctx['html'] = 'true'
        return HttpResponse(json.dumps(ctx), content_type='application/json')


from datetime import datetime
class SummaryView(TemplateView):
      template_name = 'SummaryView.html'

      @method_decorator(staff_member_required)
      @method_decorator(csrf_exempt)
      def dispatch(self, *args, **kwargs):
            return super(SummaryView, self).dispatch(*args, **kwargs)

      def get(self, request, *args, **kwargs):
        return render(request, self.template_name,{'slug': request.user.slug})

      def get(self,request,*args,**kwargs):
          ctx = {}
          date1 = request.GET.get('date1')
          date2 = request.GET.get('date2')
          if request.GET.get('date1'):
              date1 = datetime.combine(datetime.strptime(date1, "%Y-%m-%d"), datetime.min.time())
              date2 = datetime.combine(datetime.strptime(date2, "%Y-%m-%d"), datetime.max.time())
              active_users_from_device = UserDeviceTracking.objects.filter(created_time__range=(date1, date2)).values_list('user',flat=True).distinct()
              active_users = UserTracking.objects.filter(created_time__range=(date1, date2)).values_list('user',flat=True).distinct()
              result_active_users_list = list(set(list(active_users_from_device) + list(active_users)))
              number_of_active_users = len(result_active_users_list)
              number_of_invite_send = ui.objects.filter(created_time__range=(date1, date2)).count()
              number_of_users_registered = User.objects.filter(created_time__range=(date1, date2),verified = True).count()
              number_of_search = UserSearchTracking.objects.filter(created_time__range=(date1, date2)).count()
              number_of_invites_accepted = ui.objects.filter(created_time__range=(date1, date2), is_invite_accepted = True).count()
              number_share_send_using_facebook = UserShareTracking.objects.filter(email='facebook', created_time__range=(date1, date2)).count()
              number_share_send_using_google = UserShareTracking.objects.filter(email='google', created_time__range=(date1, date2)).count()
              number_share_send_using_twitter = UserShareTracking.objects.filter(email='twitter', created_time__range=(date1, date2)).count()
              number_of_share_using_email = UserShareTracking.objects.filter(created_time__range=(date1,date2)).exclude(email__in=['facebook','gmail','twitter','facebook messenger']).count()
              number_of_share_accepted_facebook  = UserShareTracking.objects.filter(created_time__range=(date1, date2), email = 'facebook', is_visited = True).count()
              number_of_share_accepted_google  =  UserShareTracking.objects.filter(created_time__range=(date1, date2), email = 'google', is_visited = True).count()
              number_of_share_accepted_twitter  =  UserShareTracking.objects.filter(created_time__range=(date1, date2), email = 'twitter', is_visited = True).count()
              number_of_share_accepted_facebook_messenger =  UserShareTracking.objects.filter(created_time__range=(date1, date2), email = 'facebook messenger', is_visited = True).count()
              number_of_share_using_facebook_messanger =  UserShareTracking.objects.filter(email='facebook messenger', created_time__range=(date1, date2)).count()
              number_of_share_accepted = UserShareTracking.objects.filter(created_time__range=(date1, date2), is_visited=True).count()
              number_of_share_accepted_email = UserShareTracking.objects.filter(created_time__range=(date1, date2), is_visited=True).exclude(email__in=['facebook','gmail','twitter','facebook messenger']).count()
              number_of_invites_accepted_via_facebook = ui.objects.filter(created_time__range=(date1, date2), is_invite_accepted = True,email='facebook',channel='facebook').count()
              number_of_invites_send_via_facebook = ui.objects.filter(created_time__range=(date1, date2), email = 'facebook').count()
              number_of_share_send = UserShareTracking.objects.filter(created_time__range=(date1, date2)).count()
              number_of_invites_accepted_via_gmail = ui.objects.filter(created_time__range=(date1, date2), is_invite_accepted = True,channel='gmail').count()
              number_of_invites_accepted_via_outlook = ui.objects.filter(created_time__range=(date1, date2), is_invite_accepted = True,channel='outlook').count()
              number_of_invites_accepted_via_email = ui.objects.filter(created_time__range=(date1, date2), is_invite_accepted = True,channel='email').count()
              number_of_invites_send_via_gmail = ui.objects.filter(created_time__range=(date1, date2), channel='gmail').count()
              number_of_invites_send_via_outlook = ui.objects.filter(created_time__range=(date1, date2), channel='outlook').count()
              number_of_invites_send_via_email = ui.objects.filter(created_time__range=(date1, date2), channel='email').count()


              number_of_pin_uploads_using_popup  = 0
              number_of_pin_uploads_using_button = 0
              content = UserFilterStat.objects.filter(created_time__range=(date1, date2))
              fltr = {}

              for i in content:
                  if i.content_filter in  fltr:
                     fltr[i.content_filter] = fltr[i.content_filter]+i.count
                  else:
                       fltr[i.content_filter] = i.count

              for i in UserPinUploadTrackingStat.objects.filter(created__range=(date1,date2)):
                  number_of_pin_uploads_using_popup = number_of_pin_uploads_using_popup+i.pin_upload_using_upload_pin_popup
                  number_of_pin_uploads_using_button = number_of_pin_uploads_using_button+i.pin_upload_using_get_browser_button

              ctx['number_of_invite_send'] = number_of_invite_send
              ctx['number_of_invites_accepted'] = number_of_invites_accepted
              ctx['number_share_send_using_facebook'] = number_share_send_using_facebook
              ctx['number_share_send_using_google'] = number_share_send_using_google
              ctx['number_share_send_using_twitter'] = number_share_send_using_twitter
              ctx['number_of_share_using_email'] = number_of_share_using_email
              ctx['number_of_pin_uploads_using_popup'] = number_of_pin_uploads_using_popup
              ctx['number_of_pin_uploads_using_button'] = number_of_pin_uploads_using_button
              ctx['number_of_share_using_facebook_messanger'] = number_of_share_using_facebook_messanger
              ctx['number_of_share_accepted'] = number_of_share_accepted
              ctx['number_of_share_accepted_facebook'] = number_of_share_accepted_facebook
              ctx['number_of_share_accepted_google'] = number_of_share_accepted_google
              ctx['number_of_share_accepted_email'] = number_of_share_accepted_email
              ctx['number_of_share_accepted_facebook_messenger'] = number_of_share_accepted_facebook_messenger
              ctx['number_of_share_accepted_twitter'] = number_of_share_accepted_twitter
              ctx['number_of_invites_accepted_via_facebook'] = number_of_invites_accepted_via_facebook
              ctx['number_of_invites_send_via_email'] = number_of_invites_send_via_email
              ctx['number_of_invites_send_via_facebook'] = number_of_invites_send_via_facebook
              ctx['number_of_share_send'] = number_of_share_send
              ctx['number_of_users_registered'] = number_of_users_registered
              ctx['number_of_search'] = number_of_search
              ctx['number_of_active_users'] = number_of_active_users
              ctx['number_of_invites_accepted_via_gmail'] = number_of_invites_accepted_via_gmail
              ctx['number_of_invites_accepted_via_outlook'] = number_of_invites_accepted_via_outlook
              ctx['number_of_invites_accepted_via_email'] = number_of_invites_accepted_via_email
              ctx['number_of_invites_send_via_gmail'] = number_of_invites_send_via_gmail
              ctx['number_of_invites_send_via_outlook'] = number_of_invites_send_via_outlook
              ctx['fltr'] = fltr

          return render(request, self.template_name,ctx)

def GeneralPinView(request, *args,**kwargs):
        if request.GET.get('p'):
            return SharedPinView.as_view()(request, *args, **kwargs)
        else:
            return LandingPageView.as_view()(request, *args, **kwargs)



class DeleteShareTracking(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(DeleteShareTracking, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        try:
            share_id = request.POST.get('share_id')
            UserShareTracking.objects.filter(pk=share_id).delete()
        except :pass

        ctx= {}
        ctx['status'] = True
        return HttpResponse(json.dumps(ctx), content_type='application/json')

class DeleteInviteTracking(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(DeleteInviteTracking, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        try:
            share_id = request.POST.get('invite_id')
            ui.objects.filter(pk=share_id).delete()
        except :pass

        ctx= {}
        ctx['status'] = True
        return HttpResponse(json.dumps(ctx), content_type='application/json')

import socialshares


class  DeleteShareTracking(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(DeleteShareTracking, self).dispatch(*args, **kwargs)

    def post(self,request,*args,**kwargs):
        print(request.POST)
        try:
            share_id = request.POST.get('share_id')
            url = request.POST.get('url')
            time.sleep(5)
            counts = socialshares.fetch('https://pickks.com/pick/201612041501554283970000/?p=89', platforms=['google','facebook'],attempts=4)
            print(counts['google'])
            print(counts['facebook']['share_count'])
            platform = request.POST.get('platform')
            # ui.objects.filter(pk=share_id).delete()
        except Exception as e : print(e,'kkkkkkkkk')

        ctx= {}
        ctx['status'] = True
        return HttpResponse(json.dumps(ctx), content_type='application/json')


class PdfScreenshotPreview(APIView):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(PdfScreenshotPreview, self).dispatch(*args, **kwargs)

    def post(self, request):

        if 'pdf' in request.session:
            del request.session['pdf']

        if 'website' in request.session:
            del request.session['website']

        post = request.POST.copy()
        file = post.get('value')
        url = file.strip()
        if 'http' in url:
            pass
        else:
            url = "http://" + url

        ctx = {}
        count = Content.objects.filter(absolute_url=url)
        if len(count) == 0:
            if url.endswith('/'):
                count = Content.objects.filter(absolute_url=url[:-1])

        if len(count) >= 1:
            if count[0].absolute_url:
                ctx['status'] = 'failed'
                return HttpResponse(json.dumps(ctx), content_type='application/json')

        '''
        if url has extension other than .pdf,.jpg,.jpeg,.png,.gif
        '''
        '''
        image files
        '''
        for i in ['.mov', '.flv', '.mp4', '.mkv', '.vob', '.wma']:

            if i in url:
                try:
                    ext = i.split('.')[1]
                    image = '/static/assets/img/video.png'
                    ctx['status'] = 'success'
                    ctx['html'] = render_to_string('image_from.html', {'image_urls': [image]})
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                except:
                    pass

        '''
        audio files files
        '''
        for i in ['.mp3']:

            if i in url:
                try:
                    ext = i.split('.')[1]
                    image = '/static/assets/img/audio.png'
                    ctx['status'] = 'success'
                    ctx['html'] = render_to_string('image_from.html', {'image_urls': [image]})
                    return HttpResponse(json.dumps(ctx), content_type='application/json')
                except:
                    pass

        """


         if url is an image
        """
        for i in ['.jpg', '.jpeg', '.png', '.gif']:
            if i in url:
                ctx['status'] = 'success'
                ctx['html'] = render_to_string('image_from.html', {'image_urls': [url]})
                return HttpResponse(json.dumps(ctx), content_type='application/json')

        '''
        if url is an pdf
        '''
        u = ""
        if '.pdf' in url:
            try:
                url = url.replace("https", "http")
                f = get_pdf_screenshot(url)
                if '/media' in f.name:
                    u = "/media" + f.name.split("/media")[1]
                else:
                    u = "/static" + f.name.split("/static")[1]
                request.session['pdf'] = f.name
            except Exception as e:
                print(e)
            ctx['status'] = 'success'
            ctx['html'] = render_to_string('image_from.html', {'image_urls': [u]})
            return HttpResponse(json.dumps(ctx), content_type='application/json')


        try:
            f = get_website_screenshot(url)
            if f:
                scrnsht = "/media" + f.name.split("/media")[1]
                image_urls = [scrnsht]
                request.session['website'] = f.name
            ctx['status'] = 'success'
            ctx['html'] = render_to_string('image_from.html', {'image_urls': image_urls})
            return HttpResponse(json.dumps(ctx), content_type='application/json')

        except Exception as e:
            ctx['status'] = 'success'
            ctx['html'] = render_to_string('image_from.html', {'image_urls': {}})
            return HttpResponse(json.dumps(ctx), content_type='application/json')


class ActivityView(View):

    template_name = 'register/activities.html'

    def get(self, request, *args, **kwargs):
        data = UserTracking.objects.filter(content__user=request.user).order_by('-created_time')

        return render(request, self.template_name, {'data': data})


class GetFileScreenshotImage(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(GetFileScreenshotImage, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        ctx = {}
        data = request.FILES.get('file_1')
        image_urls = []
        if not data:
            # if no file sent
            ctx['status'] = 'fail'
            ctx['message'] = 'No data in the file'
            ctx['html'] = render_to_string('image_from.html', {'image_urls': {}})
            return HttpResponse(json.dumps(ctx), content_type='application/json')

        if not os.path.exists('media/tmp'):
            os.makedirs('media/tmp')
        rand_filename = str(randint(9999, 99999))+data.name
        f = open('media/tmp/'+rand_filename, 'wb+')
        for chunk in data.chunks():
            f.write(chunk)
        os.chmod(f.name, 0o666)
        filename, file_extension = os.path.splitext(f.name)
        # if uploaded file is an image
        image_extension_list = ['.jpg', '.jpeg', '.png', '.gif']
        if file_extension.lower() in image_extension_list:
            image_urls = "/"+f.name
            ctx['status'] = 'success'
            ctx['html'] = render_to_string('image_from.html', {'image_urls': [image_urls], 'status': ctx['status']})
            return HttpResponse(json.dumps(ctx), content_type='application/json')
        elif file_extension.lower() in ['.docx', '.pptx', '.xlsx', '.dotx', '.doc', '.ppt', '.xls', '.pdf']:
            try:
                full_path = get_file_preview("tmp/"+rand_filename)
                thumbnail = get_thumbnail(full_path, "480x612", upscale=False, quality=99)
                image_urls = [thumbnail.url]
                ctx['status'] = 'success'
            except Exception as e:
                ctx['status'] = 'fail'
                print(e)
        ctx['html'] = render_to_string('image_from.html', {'image_urls': image_urls, 'status': ctx['status']})
        return HttpResponse(json.dumps(ctx), content_type='application/json')

# TODO need to remove if everything works with preview_generator
""""
os.chmod(f.name, 0o664)
url1 = str(Site.objects.get_current())+"/media/tmp/"+data.name
build_url = "http://docs.google.com/viewer?url="+url1+"&embedded=true"
# build_url = "http://docs.google.com/viewer?url="+url1
# build_url = 'https://docs.google.com/viewer?url=http://52.38.25.87/media/demodocx.docx&embedded=true'
url = build_url
# url = 'https://docs.google.com/viewer?url='+url+'&embedded=true'
# url = 'https://docs.google.com/viewer?url=http://52.38.25.87/media/btech.doc&embedded=true'

try:
    f = get_file_screenshot(url)
    if f:
        path = f.name.split("/media/")[1]
        scrnsht = "/media" + path
        _file = File(open(settings.MEDIA_ROOT+path))
        thumbnail = get_thumbnail(_file, "480x612", crop="center", quality=99)
        image_urls = [thumbnail.url]
        # request.session['website'] = f.name
    ctx['status'] = 'success'
    ctx['html'] = render_to_string('image_from.html', {'image_urls': image_urls})
    return HttpResponse(json.dumps(ctx), content_type='application/json')

except Exception as e:
    print(e)
    ctx['status'] = 'success'
    ctx['html'] = render_to_string('image_from.html', {'image_urls': {}})
    return HttpResponse(json.dumps(ctx), content_type='application/json')
"""

class GetResourceTypeImage(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(GetResourceTypeImage, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        ctx = {}
        data = request.POST.get('content_type') # now this will be subject id
        try:
            subject = Subject.objects.get(id=data)
            resource_type = ContentType.objects.get(name=subject.subject)
        except:
            resource_type = ContentType.objects.get(name='Other')
        ctx['status'] = 'success'
        # thumbnail = get_thumbnail(resource_type.image, "480x612", crop="center", quality=99)
        # image_urls = [thumbnail.url]
        images_obj_list = resource_type.content_type_images.all()
        img_list = []
        for i in images_obj_list:
            img_list.append(i.image)
        if len(img_list) !=0:
            image = random.choice(img_list)
            ctx['html'] = render_to_string('image_from.html', {'image_urls': [image.url]})
        else:
            image_urls = '/static/assets/img/content-images/Other.png'
            ctx['html'] = render_to_string('image_from.html', {'image_urls': [image_urls]})
        return HttpResponse(json.dumps(ctx), content_type='application/json')

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django_extensions.db import fields
# Create your models here.
from applications.accounts.models import User
from django.utils import timezone


class UserActivity(models.Model):
    """
    Saving user activities eg: Like,Share etc

    """
    ACTIVITY_TYPE = (
        ('ClickToExpand', 'Click To Expand'),
        ('ClickToFollowURL', 'Click To Follow URL'),
        ('Like', 'Like'),
        ('Share', 'Share'),
        ('Pin', 'Pin'),
    )
    user = models.ForeignKey(User, verbose_name=_('User'), related_name="user_activity")
    content_id = models.PositiveIntegerField(verbose_name=_('ContentID'), default=0)
    activity_type = models.CharField(verbose_name=_('Activity Type'), max_length=100, choices=ACTIVITY_TYPE, default='ClickToExpand')
    created = fields.CreationDateTimeField(verbose_name=_('Created'))

    class Meta:
        verbose_name = _('User Activity')
        verbose_name_plural = _('User Activities')

    def __str__(self):
        return self.activity_type


class EmailConfirmation(models.Model):
    user = models.ForeignKey(User)
    key = models.CharField(max_length=255, verbose_name=_('Hash Key'))
    created_time = models.DateTimeField("Created Date", auto_now_add=True)
    link_expired = models.BooleanField(default=False, verbose_name=_('Link Expired'))

    def __str__(self):
        return self.user.email
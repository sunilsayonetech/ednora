from django.contrib import admin

# Register your models here.
from .models import UserActivity, EmailConfirmation


class EmailConfirmationAdmin(admin.ModelAdmin):
    readonly_fields = ('created_time',)


class UserActivityAdmin(admin.ModelAdmin):
    list_display = ['user', 'activity_type']

admin.site.register(UserActivity, UserActivityAdmin)
admin.site.register(EmailConfirmation, EmailConfirmationAdmin)
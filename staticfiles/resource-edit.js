
//----------------------resource edit -----------------//
$(document).on('click', '#edit_button', function () {
    var $slug =$(this).data('folder');
    ChangeUrl('view_content',window.location.pathname);
    $('#pin_detail1').modal('hide');
    var content_id = $('#id_slug').val();
    var folder = $("#folder_name_hidden").val();
//    $('.popup-delete').attr('data-pin-id', content_id);
    var data = new FormData();
    data.append("csrf_middleware_token", $('#csrf_token').val());
    data.append("content_id", content_id);
    $.ajax({
        data: data,
        type: "POST",
        url: "/edit-content/",
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false,
        success: function (data) {
            var source = data.source;
            var type = data.type;
            if(type == 'file'){
                $('#added-file').val(source);
                $("#added-file").attr("disabled", true);
                $('#added-file').show();
                $("#edit_pin_url_id").val('').hide()
            }if(type == 'url'){
                $('#edit_pin_url_id').val(source);
                $("#edit_pin_url_id").attr("disabled", true);
                $("#edit_pin_url_id").show();
                $('#added-file').hide();
            }
            $('#edit_content_titile_id').val( data.title);
            $('#edit_content_text_id').val(data.text);
            $("#edit_grade_id").val(data.grade).select2({

            });
            $('#edit_subject_id').val(data.subject).select2({

            });
//            $('#edit_level_id').val(data.level).select2({
//                tags: true
//            });
//            var objective = data.objective.toString();
//            objective = objective.replace(/,/g, ", ");
//            $('#edit_objective_id').val(objective);
//            var learning_objective_code = data.learning_objective_code.toString();
//            learning_objective_code = learning_objective_code.replace(/,/g, ", ");
//            $('#edit_objectives_code_id').val(learning_objective_code);

            var tags = data.tags.toString();
            tags = tags.replace(/,/g, ", ");
            $('#edit_tags_id').val(tags);
//            $('#edit_drop_content_id_up').val(data.content_type);
//            $('#edit_drop_media_id_up').val(data.media_type);
            $('#edit_dop_folder_id_up').val(folder).prop('selected', true);
            $('#edit_fetched_image').html("<img src ='"+data.thumbnail+"'; style='max-width: 100%'> ");
            $('#edit_pin_submit').attr({'data-folder':$slug})
        }

    });

});

$(document).on('click', '#edit_dop_folder_id_up', function () {
    if ($('#edit_dop_folder_id_up').val() == 'Create Folder') {
        $('#createfolder').modal('show');
    }
});
$('#edit-resource-modal').on('shown.bs.modal', function (e) {
    $('body').css('overflow','hidden');
});

$('#edit-resource-modal').on('hidden.bs.modal', function (e) {
    str = window.location;
    if (String(str).slice(-1) == '#') {
        history.go(-2);
    }
    else {
        history.go(-1);
    }
    $(this).find("input,textarea,select").val('')
            .end();
    $(this).find("p").hide().end();
    var folder = $("#folder_name_hidden").val();
    $('.exceed-characters-error').hide();
    $('.choose-another-folder').attr("style", "display:none");
    $(".multiselect").select2();
//    $('#edit_drop_content_id_up').val('Resource Type*');
//    $('#edit_drop_media_id_up').val('Media Type*');
    $('body').css('overflow','auto');
    $('#edit_dop_folder_id_up').val(folder).prop('selected', true);

});

//save editted content

    $(document).on('click', '#edit_pin_submit', function () {
        //check user is still logged in
        if (document.cookie.indexOf("loggedin") == -1) {window.location.href = window.location.href;}

        var $status = 1; // a variable to set to 1 if all validation are true
        var content_id = $('#id_slug').val();
        var content_title = $('#edit_content_titile_id').val();
        var content_text = $('#edit_content_text_id').val();
        var subject = $('#edit_subject_id').val();
        var level = $('#edit_level_id').val();
//        var learning_objectives = $('#edit_objective_id').val();
//        var valid_learning_objectives = learning_objectives.replace (/[" ' , ]/g, "");
//        var objectives_code = $('#edit_objectives_code_id').val();
        var tags = $('#edit_tags_id').val();
//        var content_type = $('#edit_drop_content_id_up').val();
//        var media_type = $('#edit_drop_media_id_up').val();
        var grade = $('#edit_grade_id').val();
        var folder = $('#edit_dop_folder_id_up').val().trim();
        var current_folder = $('#folder_name_hidden').val().trim();
        var redirect_url = $(this).data('folder');
        $('.edit-error').text('');
        if (content_title.length == 0) {
                // checking whether title is empty or not
                $status = 0;
                // set the message to show on the span
                $('#edit-title-required').text('Required field');
        }else {
            var size_val = limit_title_characters(content_title);
            if (!size_val) {
                $status = 0;
            }
        }
        if (content_text.length == 0) {
                // checking whether description is empty or not
                $status = 0;
                // set the message to show on the span
                $('#content-desc-required').text('Required field');
            }
//        if (valid_learning_objectives.length == 0) {
//                // checking whether first name is empty or not
//                $status = 0;
//                $('#objective_id_required').text('Required field');
//            }
//        if (content_type.match('Resource Type*')) {
//                // checking whether first name is empty or not
//                $status = 0;
//                $('#content-type-required').text('Required field');
//            }
//        if (media_type.match('Media Type*')) {
//                // checking whether first name is empty or not
//                $status = 0;
//                $('#media_type_required').text('Required field');
//            }
        if (grade== null) {
                // checking whether first name is empty or not
                $status = 0;
                $('#grade-required').text('Required field');
            }
        if (subject== null) {
                // checking whether first name is empty or not
                $status = 0;
                $('#subject-required').text('Required field');
            }
//        if (level== null) {
//                // checking whether first name is empty or not
//                $status = 0;
//                $('#level-required').text('Required field');
//            }
        if (folder.match('Select Folder*')) {
                // checking whether first name is empty or not
                $status = 0;
                $('#folder_required').text('Required field');
            }
        var url  = $('#edit_pin_url_id').val();
        var resource_data = new FormData();
        resource_data.append("content_title", content_title);
        resource_data.append("content_text", content_text);
        resource_data.append("grade", grade);
        resource_data.append("subject", subject);
//        resource_data.append("level", level);
//        resource_data.append("learning_objectives", learning_objectives.replace(/^[,]+|[,]+$/g,'').trim());
//        resource_data.append("objectives_code", objectives_code.replace(/^[,]+|[,]+$/g,'').trim());
        resource_data.append("tags", tags.replace(/^[,]+|[,]+$/g,'').trim());
//        resource_data.append("content_type", content_type);
//        resource_data.append("media_type", media_type);
        resource_data.append("folder", folder);
        resource_data.append("content_id", content_id);
        resource_data.append("current_folder", current_folder);
        resource_data.append("csrf_middleware_token", setcsrf('csrftoken'));
        if (url.length != 0){
            if(current_folder != folder){
                var fol_data = new FormData();
                fol_data.append("url", url);
                fol_data.append("folder", folder);
                fol_data.append("type", 'url');
                $.ajax({
                    data: fol_data,
                    type: "POST",
                    url: "/check-url-in-folder/",
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false,
                    success: function (data) {
                        if (data.status == 'fail') {
                            $(".choose-another-folder").attr("style", "display:block");
                        } else {
                            if ($status==1) {
                                save_content(resource_data, redirect_url);
                            }
                        }
                    }
                });
            }else{
                if ($status==1) {
                    save_content(resource_data, redirect_url);
                }
            }
        }else{
            $(".choose-another-folder").attr("style", "display:none");
            if ($status==1) {
                save_content(resource_data, redirect_url)
            }
        }

    });

function save_content(data, redirect_url){
    var folder_slug = $("#folder_slug").val();
    $.ajax({
        data: data,
        type: "POST",
        url: "/save-content/",
        dataType: 'json',
        processData: false, // Don't process the files

        contentType: false,
        success: function (data) {
            if (data.pass == true) {
                $('#edit_success_id').text('Resource Saved successfully');
                setTimeout(function () {
                    window.location.href = $(location).attr('protocol') + "//" + $(location).attr('host') +'/folder/' + folder_slug;

                }, 1000);
            }

        }

    });
}

$(document).on('click', '.popup-delete', function () {

    var id = $(this).attr("data-pin-id");
    var folder = $('#folder_name_hidden').val();
    var folder_slug = $("#folder_slug").val();
    var data = new FormData();
    data.append("csrf_middleware_token", setcsrf('csrftoken'));
    data.append("id", id);
    data.append("folder", folder);
    $.ajax({
        data: data,
        type: "POST",
        url: "/check-lastfolder/",
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false,
        success: function (data) { // on success..
           if (data.status == "success") {
               $('#modal_message').text('The resource will be permanently deleted from Ednora. Do you want to proceed?')
           }
           if (data.status == "fail") {
               $('#modal_message').text('The resource will be deleted from the folder. Do you want to proceed?')
           }
        }
    });

    $('#confirm_delete_resource').click(function (e) {
        $("#resource_delete").modal("hide");//to avoid multiple tracking entries,close popup here
        var folder = $('#folder_name_hidden').val();
        var data = new FormData();
        data.append("csrf_middleware_token", setcsrf('csrftoken'));
        data.append("id", id);
        data.append("folder", folder);
        $.ajax({
            data: data,
            type: "POST",
            url: "/delete-pin/",
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false,
            success: function (data) { // on success..
                if (data.status == "success") {
                    $("#edit-resource-modal").modal("hide");
                    setTimeout(function(){
                    window.location.href = $(location).attr('protocol') + "//" + $(location).attr('host') +'/folder/' + folder_slug;
                }, 1000);
                }
                return false;
            }
        });


    });
    $('#cancel_delete_resource').click(function (e) {
        $("#resource_delete").modal("hide")
    })
});



//----------------------end resource edit --------------//
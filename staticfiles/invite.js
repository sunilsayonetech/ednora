/**
 * Created by sayone on 28/8/16.
 */
$(document).ready(function(){
    $('#goog').click(function(){
        $('body').click();
        $("#connections").empty();
        auth();
        $('#invitechannel').val('gmail');
        $('#contact_logo').attr('src',"/static/assets/img/gmail.jpeg");
    });
    $('#goog1').click(function(){
        $("#connections").empty();
        $('body').click();
        auth();
        $('#invitechannel').val('gmail');
        $('#contact_logo').attr('src',"/static/assets/img/gmail.jpeg");

    });
});

function auth() {
    var config = {
      'client_id': '609710246949-652gpovcs63djdjrau652o8848agjkr7.apps.googleusercontent.com',
      'scope': 'https://www.googleapis.com/auth/contacts.readonly'
    };
    gapi.auth.authorize(config, function() {
      fetch(gapi.auth.getToken());
    });
}

function logout_google(){
    var token = gapi.auth.getToken();
    if (token) {
      var accessToken = gapi.auth.getToken().access_token;
      if (accessToken) {
          $.ajax({
                url: 'https://accounts.google.com/o/oauth2/revoke?token=' + accessToken,
                async: false,
                contentType: "application/json",
                dataType: 'jsonp',
              success: function(result){
                }});
      }
    }
    gapi.auth.setToken(null);
    gapi.auth.signOut();
}

function fetch(token) {
    token['g-oauth-window'] = null;
    $.ajax({
        url: "https://www.google.com/m8/feeds/contacts/default/thin?alt=json&max-results=500&v=3.0",
        dataType: 'jsonp',
        data: token
    }).done(function(data)
    {
        $('#invitechannel').val('outlook');
        var output ="";

        output = google_contact_sort(data);
        $("#connections").empty();
        $('#invite_text').empty();
        $('#invitechannel').val('gmail');
        $('#invite_text').html( "<p> Found "+output.count+ " contacts</p>");
        $("#connections").html(""+output.html);
        $('#invte').modal('show');
        logout_google();
    });
}

//for mobile header
$( document ).ready(function() {
    //live.com api
    function wl_login(){
        WL.login({
            scope: ["wl.basic", "wl.contacts_emails"]
        }).then(function (response) {
            WL.api({
                path: "me/contacts?$top=50",
                method: "GET"
            }).then(function (response) {
                    output = outlook_contact_sort(response.data)
                    $("#connections").empty();
                    $('#invite_text').empty();
                    $('#invite_text').html( "<p> Found "+output.count+ " contacts</p>");
                    if (typeof output.html != 'undefined'){
                      $("#connections").html(""+output.html);
                    }
                    $('#invitechannel').val('outlook');
                    $('#invte').modal('show');
                    output = '';
//                    WL.logout();
                },
                function (responseFailed) {
                    console.log(responseFailed);
                }
            );
        },
        function (responseFailed)
        {
            console.log("Error signing in: " + responseFailed.error_description);
        });
    }
    var output ="";
    $('.outlook-invite').click(function(e) {
        $('body').click();
        tt = 'wl_auth';
        $.removeCookie(tt);

        e.preventDefault();
        $('#contact_logo').attr('src',"/static/assets/img/outlook.png");
        $("#connections").empty();

        output ="";
        var count=0;
        WL.init({
            client_id: 'deadc23e-f6b1-4884-b051-33a1900b4296',
            redirect_uri: "https://www.ednora.com",
            scope: ["wl.basic", "wl.contacts_emails"],
            response_type: "token"
        }).then(function (response) {
            if (response.status == "connected") {
                WL.logout().then(function (response) {
                    wl_login()
                });
            }else{
                wl_login()
            }
        });

    });
});

function invite() {
    var mailArray = new Array();
    $("input[name*=mail]").each(function () {
        if ($(this).prop('checked')) {
            mailArray.push($(this).val());
        }
    });

    var data  = new FormData();
    if (mailArray.length>0){

        $("#invitations_success").show();
        setTimeout(function() {
            $('#invte').modal('hide');
        }, 2000);
        $('#listidval').hide();
        data.append('mail', mailArray);
        data.append('channel',$('#invitechannel').val());
        $.ajax({
            data: data,
            type: "POST",
            url: "/user-invites/",
            processData: false,
            contentType: false,

        });
    }
    else{
        $('#listidval').text('Select at least one email address');
        $('#listidval').show();
    }

}

function inviteusingemailid(){
       var $email = "";
       $email = $("#emailid").val();
       var mailArray = new Array();
       if ($email != ""){
           mailArray.push($email);
           var data = new FormData();
           data.append('mail', mailArray);
           data.append('channel', 'email');
           $('#emailidsuccess').text('Invite Sent');
           var myVar = setTimeout(function () {
               $('#inviteviamail').modal('hide');
           }, 1000);

           $.ajax({
               data: data,
               type: "POST",
               url: "/user-invites/",
               processData: false,
               contentType: false
           });
       }
}

$(document).ready(function(){
    $('#emailid').keyup(function(){
        var text =$('#emailid').val();
        text = text.trim();
        if (text.length==0){
           $('#emailidval').text('Required Field*');
           $('#emailidval').show();
           $('#sendInvite2').attr('disabled','disabled');
           return false;
        }else if (!validateEmail(text)){
            $('#emailidval').text('Enter valid email');
            $('#emailidval').show();
            $('#sendInvite2').attr('disabled','disabled');

            return false;
        }
        else
        {
           $('#emailidval').hide();
           $('#sendInvite2').removeAttr('disabled')
        }
    });
    $('#emailinvite').click(function () {
        $('#inviteviamail').modal('show');
    })
    $('#emailinvite1').click(function () {
        $('#inviteviamail').modal('show');
    })

});

$(document).on('show.bs.modal','#inviteviamail', function () {
    $('#inviteviamail').find('.modal-body').click();
    $('#emailid').val('');
    $('#emailidval').hide();
    $('#emailidsuccess').text('');

});


$(document).on('shown.bs.modal','#inviteviamail', function () {
    $('#emailid').focus();

});

$(document).on('shown.bs.modal','#invte', function () {

     $('#invte').find('.modal-body').click();

});
$(document).on('show.bs.modal','#invte', function () {
    $("#invitations_success").hide();
    $('#listidval').hide();

});

$(document).on('hide.bs.modal','#invte', function () {
    $('#invitechannel').val('');
});


function google_contact_sort(contacts) {
    var obj = contacts.feed.entry
    var count = 0;
    var output;
    var arr_name = [];
    var arr_email = [];
    var arr_sort = [];
    var output = "";
    var content = "";
    var fullname="";

    for (var property in obj)
    {
        for (var email in obj[property].gd$email)
        {

            if (JSON.stringify(obj[property].gd$email[email].address) != undefined)
            {

                if (JSON.stringify(obj[property].gd$name)) {

                    fullname = obj[property].gd$name.gd$fullName.$t;
                }

                var email = obj[property].gd$email[email].address;
                email = email.replace(/undefined/g, '');
                if (fullname) {
                    name_email = fullname + ":" + email;
                    arr_name.push(name_email)
                    fullname = '';
                }
                else {
                    arr_email.push(email)
                }

            }
            
            fullname="";

        }

    }
    arr_name.sort(ignoreCase);
    arr_email.sort(ignoreCase);
    arr_sort= arr_name.concat(arr_email);
    for (count=0;count<arr_sort.length;count++)
	{
        if (arr_sort[count].indexOf(":") !== -1){

            var contact =  arr_sort[count].split(":");
            content = "<tr>  <td><input type='checkbox' name=mail[] value="+contact[1] +" id='id_"+count+"'></td><td><label for='id_"+count+"'>"+contact[0]+"&nbsp;&lt;"+contact[1]+"&gt;</label></td></tr>";
        }else{

            content = "<tr>  <td><input type='checkbox' name=mail[] value="+arr_sort[count] +" id='id_"+count+"'> </td> <td><label for='id_"+count+"'>"+arr_sort[count]+"</label></td></tr>";
        }
        output = output + content;

	}

    function ignoreCase(a,b) {
        return (''+a).toUpperCase() < (''+b).toUpperCase() ? -1 : 1;
    }
    return {html: output, count: arr_sort.length};

}


function outlook_contact_sort(contacts){
    var obj = contacts
    var count = 0;
    var output;
    var arr_name = [];
    var arr_email = [];
    var arr_sort = [];
    var content = "";
        $.each(obj, function (key, value)
        {
            if (value.emails.preferred != null)
            {
                count++;
                if (value.name)
                {
                    name_email= value.name + ":" + value.emails.preferred;
                    arr_name.push(name_email)
                }
                else
                {
                    arr_email.push(value.emails.preferred)
                }
                content = '';
            }
        });

        arr_name.sort(ignoreCase)
        arr_email.sort(ignoreCase)
        arr_sort = arr_name.concat(arr_email);
    for (count=0;count<arr_sort.length;count++){
        if (arr_sort[count].indexOf(":") !== -1){
            var contact =  arr_sort[count].split(":");
            content = "<tr>  <td><input type='checkbox' name=mail[] value="+contact[1] +" id='id_"+count+"'></td><td><label for='id_"+count+"'>"+contact[0]+"&nbsp;&lt;"+contact[1]+"&gt;</label></td></tr>";
        }else{

            content = "<tr>  <td><input type='checkbox' name=mail[] value="+arr_sort[count] +" id='id_"+count+"'> </td> <td><label for='id_"+count+"'>"+arr_sort[count]+"</label></td></tr>";
        }
        output = output + content;
    }

    function ignoreCase(a, b){
        return ('' + a).toUpperCase() < ('' + b).toUpperCase() ? -1 : 1;
    }

    return {html: output, count: arr_sort.length };

}

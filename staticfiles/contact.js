// script for contact popup

$(document).ready(function () {

    $('#id_contact_name').on('keydown', function (e) {
         if (e.keyCode == 32) return false;
        }
    );

    $("#contact_popup").on("hidden.bs.modal", function(){
        $(".help-block").css('display', 'none');
        $('#contact_popup .form-control').val('');
        $('#bodyshow').css('display', 'none')
        $('body').css('overflow','auto')
    });

    $('#contact-submit').click(function (e) {
        e.preventDefault();
        var status =true;
        var $id_contact_name = $('#id_contact_name');
        var $id_email_contact_person = $('#id_email_contact_person');
        var $id_contact_message = $('#id_contact_message');
        var conatact_person_name = $id_contact_name.val().replace(/\s\s+/g, ' ');
        var $id_contact_name_validation = $('#id_contact_name_validation');
        if(conatact_person_name.length == 0)
        {
            status = false;
            $id_contact_name_validation.show();
        }if(conatact_person_name.length>30){
            status = false;
            $id_contact_name_validation.text('Ensure this value has at most 30 characters').show();
        }
        var email = $id_email_contact_person.val();
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(regex.test(email) == false){
            status = false;
            $('#id_email_contact_person_validation').show();
        }
        if(email.length == 0)
        {
            status = false;
            $('#id_email_contact_person_validation').show();
        }
        var message = $id_contact_message.val();
        if(message.length == 0)
        {
            status = false;
            $('#id_contact_message_validation').text('Required field').show();
        }if(message.length>1000){
            status = false;
            $("#exceed-characters-error").show();
        }

        if(status==true)
        {
            $('#contact-submit').attr("disabled", true);
            $.ajax({
                type: "POST",
                url: "/contact-us/",
                data: {
                    'name': conatact_person_name,
                    'email':email,
                    'message':message// from form
                },
                success: function (response) {
                    $('#bodyshow').show();
                    setTimeout(function() {
                        $('#contact_popup').modal('hide');
                    }, 2000);
                    $('#contact-submit').attr("disabled", false);
                }

            });
        }
    });

    $(document).on('show.bs.modal','#contact_popup', function () {
        $('#bodyhide1').show();
        $('#feedbacksuccess').hide();
        $('#conteact_hide').show();
        $('body').css('overflow','hidden')

    });
    $("#id_contact_name").keyup(function () {
        $("#id_contact_name_validation").hide();

    });
    $("#id_email_contact_person").keyup(function () {
        $("#id_email_contact_person_validation").hide();

    });
    $("#id_contact_message").keyup(function () {
        $("#id_contact_message_validation").hide();

    });

    $("#id_email_contact_person").change(function () {

        if(!validateEmail($("#id_email_contact_person").val()))
        {
            $("#id_email_contact_person_validation").text('Enter a valid email').show();
        }
        else{
             $("#id_email_contact_person_validation").hide();
        }

    });
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return email.match(re);
    }
    $(document).on('keyup', '#id_contact_message', function (e) {
        var wordcount = $('#id_contact_message').val().length;
        if (wordcount > 1000){
            $("#exceed-characters-error").show();
        }else {
            $("#exceed-characters-error").hide();
        }

    });

});
var selected_image = "";
var old_image ="";
$(window).load(function () {
    var imageLoader = document.getElementById('myFile1');
    imageLoader.addEventListener('change', handleImage, false);
    function handleImage(e) {
        var $user_dp_image = $('#user_dp_image');
        var $user_dp = $('#user_dp');
        old_image = $user_dp_image.attr('src');

        if( imageLoader.files.length!=0)
        {
            $user_dp_image.attr('src', '/static/assets/img/loader_dp.gif');
            $user_dp.attr('src', '/static/assets/img/loader_dp.gif');
        }

        var reader = new FileReader();
        reader.onload = function (event) {
            selected_image = event.target.result;
        };
        reader.readAsDataURL(e.target.files[0]);
        var profile_image = e.target.files[0];
        var re = /(\.jpg|\.jpeg|\.png)$/i;
        if(!re.exec($('#myFile1').val())){
            $('#image_validation').text('File format not supported').show();
            $user_dp_image.attr('src', old_image);
            $user_dp.attr('src', old_image);
        }
        else if (Math.round(profile_image.size/1024/1024)>2)
        {
            $('#image_validation').text('Image size must be less than 2Mb ').show();
            $user_dp_image.attr('src', old_image);
            $user_dp.attr('src', old_image);
        }
        else {
            $('#image_validation').hide();
            var data = new FormData();
            data.append("user_image", profile_image);
            setTimeout(function () {
                $.ajax({
                    type: "POST",
                    url: "/change-user-dp/",
                    processData: false,
                    data: data,
                    dataType: 'json',
                    contentType: false,
                    success: function (data) {
                        var $id_to_change_user_dp = $('#id_to_change_user_dp');
                        $id_to_change_user_dp.replaceWith(data.html);
                        $('#user_dp').attr('src', data.profile_image_url)
                        $('.change-pro-pic').text('Change Picture');
                        $('#delete_user_dp').removeAttr('style');
                        setTimeout(function(){
                            var imageWidth = $("#user_dp_image").width();
                            var imageHeight = $("#user_dp_image").height();
                            if(imageWidth>imageHeight){
                               $("#user_dp_image").addClass("fixToHeight");
                                $("#user_dp").addClass("fixToHeight");
                            }
                            else{ $("#user_dp_image").addClass("fixToWidth");
                                $("#user_dp").addClass("fixToWidth");
                            }

                        },100);


                    }
                });

            }, 2000);
        }

    }

});


$(document).ready(function(){

    var imageWidth = $("#user_dp_image").width();
    var imageHeight = $("#user_dp_image").height();
    if(imageWidth>imageHeight){
        $("#user_dp_image").addClass("fixToHeight");
        $("#user_dp").addClass("fixToHeight");
    }else{
        $("#user_dp_image").addClass("fixToWidth");
        $("#user_dp").addClass("fixToWidth");
    }
});

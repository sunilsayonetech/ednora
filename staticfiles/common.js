/**
 * Created by sayone on 25/7/17.
 */

$(document).on('click','#confirm_unflag_pin', function(){
            var data = {'content_id': $('#id_slug').val(), 'csrfmiddlewaretoken': setcsrf('csrftoken')}
            $('#unflag_pin').modal('show')
            process_flagging(data, false)
});

function reply_to_comment(id) {
    if (document.cookie.indexOf("loggedin") == -1) {
            window.location.href = window.location.href;
        }
        $('.reply-field').hide();
        $('.reply-btn').hide();
        $('#comment_reply_text_id' + id).show();
        $('#comment_reply_text_id' + id).val('');
        $('#comment_reply_button_id' + id).attr("style", "display: block");

        $('#comment_text_id').focus(function(){
            $('#comment_reply_text_id'+id).hide();
            $('#comment_reply_button_id'+id).hide();
        })
    }


function reply_back(id) {
        var comment_post = $('#comment_reply_text_id' + id).val();
       
        
        var model_id = id;
        var model = 'comment';
        var data = new FormData();
        var user_dp = $('#user_dp').attr('src');
        var user_name='';
        $(".user_name_title i").each(function(){
            user_name =  $(this).text();
        });
        if( comment_post != ''){
            data.append("csrfmiddlewaretoken",setcsrf('csrftoken'));
            data.append("comment_text", comment_post);
            data.append("model_id", model_id);
            data.append("model", model);
            $.ajax({
                data: data,
                async: false,
                type: "POST",
                url: "/api/comment-post/",
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false,
                success: function (data) {
                    $('#comment_reply_text_id' + id).hide();
                    $('#comment_reply_button_id' + id).hide();
                    elm ='[data-comment="xxx"]'.replace('xxx',id)
                    $(elm +" br").first().after(data.html);
                    $('.comment-section').click()
                },

                error: function (data) {
                }
            });
        }
    }


$(document).on('click', '#loadMore', function(){
    $('#showLess').show();
    $('.reply-comment').show();
    if($(this).attr('data-page')) {
        data = {'page': $(this).attr('data-page'), 'content_id': $('#id_slug').val()}
        $.get(load_more_comments_api, data, function (data, status) {
            $('#comment_lists').append(data.html);
            if (data.next_url) {
                $('#loadMore').show();
                $('#loadMore').attr('data-page', data.page+1 )
            }
            else {
                $('#loadMore').hide();
                $(this).removeAttr('data-page')
            }
        });
    }
} );

$(document).on('click', '#showLess', function () {
    page = $('.comment-page').length-1
    $('#loadMore').attr('data-page',page)
    var div_to_remove = $('#page_'+page)
    div_to_remove.remove()
    if($('.comment-page').length==1)
    {
        $(this).hide();
        $('.reply-comment').hide();
    }
     if($('.comment-page').length>0)
    {
        $('#loadMore').show();
    }
});


// comment

$(document).on('click', '#comment_button_id', function(){

    if (document.cookie.indexOf("loggedin") == -1) {
            window.location.href = window.location.href;
        }
    
    text = $('#comment_text_id').val();

    if(text.trim().length>0)
    {
            var comment_post = $('#comment_text_id').val();
            var model_id = $('#id_slug').val();
            var model = 'content';
            var csrftoken = setcsrf('csrftoken');
            if (comment_post != '') {
                var data = new FormData();
                //data.append("csrfmiddlewaretoken", $('#csrf_token').val());
                data.append("csrfmiddlewaretoken", csrftoken);
                data.append("comment_text", comment_post);
                data.append("model_id", model_id);
                data.append("model", model);
                $.ajax({
                    data: data,
                    async: false,
                    type: "POST",
                    url: '/api/comment-post/',
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false,
                    beforeSend: function () {
                        $('#comment_button_id').prop('disabled', true);
                        $('#comment_text_id').val('');
                    },
                    headers: {'X-CSRFToken': csrftoken },
                    success: function (data) {
                        $('#comment_button_id').prop('disabled', false); // enable button
                        if($("#myList").find(".comment-page").length>0)
                        {
                            $("#myList").find(".comment-page").first().prepend(data.html)
                        }
                        else{
                            div_to_append = '<div id="page_1" class="comment-page"> </div>'
                            $("#myList").find("#comment_lists").append(div_to_append);
                             $("#myList").find(".comment-page").first().prepend(data.html)

                        }


                    },

                    error: function (data) {
                    }
                });
            }
    }

});


//$(document).on('click', '.reply-btn', function(){
//    $('#comment_text_id').focus();
//});#focus to the comment box after reply is not needed

$(document).on('click', '.related-resouce-populate', function () {
    // check the user is still logged in
        if (document.cookie.indexOf("loggedin") == -1) {
            window.location.href = window.location.href;
        }
        ChangeUrl('', currenct_url)
        changeurl = '/resource/' + $(this).attr('data-href').trim() + '/';
        ChangeUrl('view_content', changeurl);
        populate_resource_detail_popup($(this).attr('data-href'))
         $("#edit_button_ul").hide();
         $("#id_delete1").hide();

});




$(document.body).on('hide.bs.modal', function () {
    $('body').css('padding-right','0');
});
$(document.body).on('hidden.bs.modal', function () {
    $('body').css('padding-right','0');
});



$(document).keyup(function(e) {

    objs =[];
     if (e.keyCode == 27) {
         $('.modal').each(function () {

             if ($(this).is(':visible')) {
                 objs.push({'id': this.id, 'index': $(this).css('z-index')})
             }
         });
         objs.sort(function (a, b) {
             var nameA = a.index, nameB = b.index
             if (nameA < nameB) //sort string ascending
                 return -1
             if (nameA > nameB)
                 return 1
             return 0 //default return value (no sorting)
         })
         if(objs.length>0)
         {
              $('#'+objs[objs.length-1]['id']).modal('hide');
         }

     }

});

$('#repin_tag').keyup(function(e){
    if(e.which==13)
    {
        $('#repin_save').trigger('click');
    }

});


$(document).on('click', '#id_show_detailed_text', function(){
    $('#cont_detail_more').show();
    $('#cont_detail').hide();
    $(this).hide()
})

 $('#repin_popup').on('show.bs.modal', function (e) {
     $('#drop-repin').val('Select Folder*').prop('selected', true);
 });

//login
$(document).on('click', '.modal-sign-in', function(){

            var email = $('#username').val();
            var password = $('#password').val();
            var next = window.location.pathname;
            var csrftoken = setcsrf('csrftoken');

                var data = new FormData();
                //data.append("csrfmiddlewaretoken", $('#csrf_token').val());
                data.append("csrfmiddlewaretoken", csrftoken);
                data.append("email", email);
                data.append("password", password);
                data.append("next", next);
                $.ajax({
                    data: data,
                    async: false,
                    type: "POST",
                    url: '/login-via-model/',
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false,
                    success: function (data) {
                        if(data.status==true)
                        {
                            window.location = data.next;
                        }
                        else{
                            $('#loginerror').html(data.status)
                        }
                    },

                    error: function (data) {
                    }
                });



});
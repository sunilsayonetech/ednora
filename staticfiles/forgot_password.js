// js for forgot password section.
$(document).ready(function () {
    $("#forgot-pwd").submit(function (event) {

        function isValidEmailAddress(emailAddress) {
            var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
            return pattern.test(emailAddress);
        }
        var address = $('#emailInput').val();
        if(address.length==0)
        {
            $('#message').html("<h2 class='reset-pwd-error' >Required Field</h2>").show();
            return false;
        }
        if (!isValidEmailAddress(address)) {
            $('#message').html("<h2 class='reset-pwd-error' >Enter valid email address</h2>").show();
            return false;
        }
        else {

            $('#loader').show();
            $('#submit-reset').hide();

            $.ajax({
                type: "POST",
                url: "/api/password_reset/",
                data: {
                    'email': address // from form
                },
                success: function (response) {
                    if (response['error_status'] == "Not a valid email") {
                        $('#loader').hide();
                        $('#submit-reset').show();
                        $('#message').html("<h2 class='reset-pwd-error' >Enter registered email address</h2>").show();

                    }
                    else if (response['success_status'] == "email has been sent") {

                        $('#loader').hide();
                        $('#submit-reset').hide();
                        $('#message').html("<h2 class='reset-pwd-success'>" +
                                               "Password reset link has been sent to your email.</h2>").show();

                    }

                }
            });
            return false;
        }
    });

    $("#emailInput").focus();

    $('#emailInput').keydown(function () {
        $('#message').hide();

    });

});
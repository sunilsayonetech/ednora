
$(document).ready(function () {
    $('#datamessage').fadeIn().delay(3000).fadeOut();
    $(document).on('show.bs.modal','#pwdModal', function () {
        $('#loader').hide();
        $('.change-pwd-error').hide();
        $(this).find("input")
        .val('')
        .end();
        var $id_old_password = $('#id_old_password');
        $id_old_password.focus();
        $('#deactivate').removeAttr('disabled');
    });

    $('#id_rest_password_submit').click(function () {
        var all_fields_filled = true;
        var good = /^(?=.*[a-z])(?=.*[A-Z])(?=.*?[0-9]|.*?[#?!@$%^&*-])[a-zA-Z\d]{8,}/g;
        //Must contain at least one upper case letter, one lower case letter and (one number and one special char).
        var better = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/g;
        var $old_password = $('#id_old_password').val();
        var $id_old_password_required = $('#id_old_password_validation');
        var $id_new_password_validation = $('#id_new_password_validation');
        var $id_retype_new_validation = $('#id_retype_new_validation');
        var $weakpassword = $('#weakpassword');
        $('.change-pwd-error').hide();

        //to check old password length is zero
        if($old_password.length == 0 )
        {
            $id_old_password_required.text('Required field').show();
            all_fields_filled = false;
        }
        var $new_password = $('#id_new_password').val();
        // to check newly typed password length is zero
        if (($new_password.match(good) ) || ($new_password.match(better) ))
        {
            all_fields_filled = true;
        }
        else
        {
            all_fields_filled = false;
            if($new_password.length!=0)
            if(!$new_password.match(good) && !$new_password.match(better) ) {
                $weakpassword.text('New Password is weak');
                $weakpassword.show();
            }
        }

        if($new_password.length == 0 )
        {
            $id_new_password_validation.text('Required field').show();
            all_fields_filled = false;
        }
        var retyped_new_password = $('#id_retype_new').val();
        //to check new password length is zero
        if(retyped_new_password.length == 0 )
        {
            $id_retype_new_validation.text('Required field').show();
            all_fields_filled = false;
        }
        if(all_fields_filled == true) {
            // submit form
            var $id_old_not_correct = $('#id_old_not_correct');
            var $id_pass_mismatch = $('#id_pass_mismatch');
            var id_pass_success = $('#id_pass_success');
            $('#loader').show();
            $('#id_rest_password_submit').hide();
            $('#id_rest_password_cancel').hide();
            $.ajax({
                type: "POST",
                url: "/password-in-profile/",
                data: {
                    'old_password': $old_password,
                    'new_password': $new_password,
                    're_type_new_password': retyped_new_password
                },
                success: function (data) {
                    $('#id_rest_password_submit').show();
                    $('#id_rest_password_cancel').show();
                    $('#loader').hide();
                    //old password does not match
                    if(data.old_password_mismatch == true)
                    {
                        $id_old_not_correct.text('Old password does not match').show();
                        $id_pass_mismatch.hide();

                    }
                    if(data.new_password_mismatch == true)
                    {
                        $id_pass_mismatch.text('New password does not match').show();
                        $id_old_not_correct.hide();
                    }
                    if(data.reset_success == true)
                    {
                        id_pass_success.text('Your password is successfully changed').show();
                        $id_old_not_correct.hide();
                        $id_pass_mismatch.hide();
                        setTimeout(function(){
                            $("#pwdModal").modal('toggle');

                        }, 2000);
                    }

                }
            });
            return false;
            //
        }

    });
    $('#id_new_password').on('keyup', function (e) {
         if (e.keyCode != 32)
            check_for_password_strength($(this));
         }
    );
    $('#id_new_password').on('keydown', function (e) {
         if (e.keyCode == 32) return false;
        }
    );
    $('#id_retype_new').on('keydown', function (e) {
         if (e.keyCode == 32) return false;
        }
    );
    pass="";

    $('#other').click(function () {
        $('#other_reason').show();
    });

    $(document).on('hidden.bs.modal','#deactivate_user', function () {
        $("input[name*='reason']").each(
            function (){
                $(this).prop('checked','');
            }
        );

    });

    $('#id_state').change(function(){
        var state = $('#id_state').val();
        $("#id_district").val('');
        $("#id_school").val('');
        if ((state.length == 0) || (state==null))
        {
            $('#id_district').prop("disabled", true);
            $('#id_school').prop("disabled", true);

        }
        else {
            $.ajax({
                type: 'GET',
                url: '/district-populate?state=' + state,
                success: function (data) {
                    var districts = data;
                    var options = '<option value="" selected="selected">Select State*</option>';
                    for (var obj = 0; obj < districts.length; obj++) {
                        options += '<option value="' + districts[obj]['id'] + '">' + districts[obj]['name'] + '</option>';
                    }

                    $('#id_district').prop("disabled", false);
                    $('#id_school').prop("disabled", true);
                    $('#id_district').html(options);
                }
            });
        }
    });

    $('#id_district').change(function(){
        var district = $('#id_district').val();
        var state = $('#id_state').val();
        $("#id_school").val('');
        if (((district.length != 0)||(state.length != 0) || (state==null)) && (district.length != 0))
        {
            $('#id_school').prop("disabled", false);
            populate_school(state, district)

        }else{
            $('#id_school').prop("disabled", true);
        }
    });

    function populate_school(state, district){
        $('#id_school').autocomplete({
            delay: 100,
            source: function (request, response) {
                var query = request.term;
                // Suggest URL
                var suggestion_url = '/school-populate?state='+state+'&district='+district+'&query='+query+'&on=profile';

                // JSONP Request
                $.ajax({
                    type: 'GET',
                    url: suggestion_url
                }).success(function(data){
                        response(data);
                });
            }
        });
    }


    $('#id_school').click(function(){
        var district = $('#id_district').val();
        var state = $('#id_state').val();
        $('#id_school').autocomplete({
        delay: 100,
        source: function (request, response) {
            var query = request.term;
            // Suggest URL
            var suggestion_url = '/school-populate?state='+state+'&district='+district+'&query='+query+'&on=profile';

            // JSONP Request
            $.ajax({
                type: 'GET',
                url: suggestion_url
            })
            .success(function(data){
                response(data);
            });
        }
        });
    });
    $('#id_old_password').keydown(function(){
       $('#id_old_password_validation').hide();

    });

    $('#id_new_password').keydown(function(){
       $('#id_new_password_validation').hide();
    });

    $('#id_retype_new').keydown(function(){
       $('#id_retype_new_validation').hide();

    });

    $('#id_email_preference').change(function(){
        if ($(this).prop('checked')==true){
            $(this).val(1)

        }else{
            $(this).val(0)
        }

    });

     $('#form_reset').on('click',function(){
         $('#profileFieldHeight')[0].reset();
         $('#profileFieldHeight').find("select").trigger("change");
         $(window).scrollTop(0);
         $('#form_reset').blur();
     });

    $('#delete_user_dp_confirm').click(function ()
     {
          //check user is still logged in
        if (document.cookie.indexOf("loggedin") == -1) {window.location.href = window.location.href;}
        $("#delete_dp_confirm").modal('hide');
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/user-profile-dp-delete/",
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.status == 'success') {

                    $('#user_dp').attr('src', data.image);
                    $('#user_dp_image').attr('src',data.image);
                    $('#delete_user_dp').hide();
                }


            },
            error: function (data) {
            }
        });
     })

});

$('input[data-toggle="popover"]').popover({
    placement: 'bottom',
    trigger: 'focus'
});
// handle the toggle between deactivate reason.
$(function(){
   $('input').change(function() {
    if (!$('#other').is(':checked')) {
        $('#other_reason').hide();

    }
    });

});
// deatcivate user account from user profile
function deactivateMyaccount(){
    var $reason = '';
    if($('#other_reason').val().length>0)
        $reason=$('#other_reason').val();
    else{
        $("input[name*='reason']").each(
        function (){

            if( $(this).prop('checked'))
            $reason = $(this).val();
        }
        );
    }

    if($reason.length>0){
        $('#deactivate').attr('disabled','disabled');
        var data = new FormData();
        data.append('reason', $reason);
        $("#deactivate_user").modal('hide');
        $("#deactivate_user_success").modal('show');//close the popup here to show the warning message
        $.ajax({
            data: data,
            type: "POST",
            dataType: 'json',
            url: "/user-deactivate/",
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.status == 'success')
                {
                    var timer = setTimeout(function () {
                        window.location.href = '/logout/';
                    }, 3000);
                }
            },
            error: function (data) {
            }
        });

    }
    else
    {
        $('#id_error').show();
    }

}

$(document).on('change', '#id_state_preference', function () {
    if ($(this).prop('checked')==false){
        $('#id_nation_preference').prop('checked', false);
    }
});
$(document).on('change', '#id_nation_preference', function () {
    if ($(this).prop('checked')==true){
       $('#id_state_preference').prop('checked', true); //checked
    }
});


    $(window).load(function(){
        if(window.location.href.match('/resource')){
            $('#invite_user_existing').modal('show');
            $('#display_message').html("Please login or register to www.ednora.com to access the Resource");

        }
        var state = $('#id_state').val();
        var district = $('#id_district').val();
        var school = $('#id_skool').val();
        if ((state.length == 0) || (state==null))
        {
            $('#id_district').prop("disabled", true);
            $('#id_skool').prop("disabled", true);
        }else {
            $.ajax({
            type: 'GET',
            url: '/district-populate?state=' + state,
            success: function (data) {
                var districts = data;
                var options = '<option value="" selected="selected">Select District*</option>';
                for (var obj = 0; obj < districts.length; obj++) {
                    if (districts[obj]['id'] == district){
                        options += '<option value="' + districts[obj]['id'] + '"'+'selected="selected">' + districts[obj]['name'] + '</option>';
                    }else{
                        options += '<option value="' + districts[obj]['id'] + '">' + districts[obj]['name'] + '</option>';
                    }

                }

                $('#id_district').prop("disabled", false);
                $('#id_district').html(options);
                $('#id_skool').val(school);
                populate_school(state, district)
                }
            });

        }

    });

    $('#id_state').change(function(){
        var state = $('#id_state').val();
        $("#id_district").val('');
        $("#id_skool").val('');
        if ((state.length == 0) || (state==null))
        {
            $('#id_district').prop("disabled", true);
            $('#id_skool').prop("disabled", true);

        }
        else {
            $.ajax({
                type: 'GET',
                url: '/district-populate?state=' + state,
                success: function (data) {
                    var districts = data;
                    var options = '<option value="" selected="selected">Select State*</option>';
                    for (var obj = 0; obj < districts.length; obj++) {
                        options += '<option value="' + districts[obj]['id'] + '">' + districts[obj]['name'] + '</option>';
                    }

                    $('#id_district').prop("disabled", false);
                    $('#id_skool').prop("disabled", true);
                    $('#id_district').html(options);
                }
            });
        }
    });

    $('#id_district').change(function(){
        var district = $('#id_district').val();
        var state = $('#id_state').val();
        $("#id_skool").val('');
        if (((district.length != 0)||(state.length != 0) || (state==null)) && (district.length != 0))
        {
            $('#id_skool').prop("disabled", false);
            populate_school(state, district)

        }else{
            $('#id_skool').prop("disabled", true);
        }
    });

    function populate_school(state, district){
        $('#id_skool').autocomplete({
            appendTo: '#custom-auto-width',
            delay: 100,
            source: function (request, response) {
                var query = request.term;
                // Suggest URL
                var suggestion_url = '/school-populate?state='+state+'&district='+district+'&query='+query+'&on=signup';

                // JSONP Request
                $.ajax({
                    type: 'GET',
                    url: suggestion_url
                }).success(function(data){
                    response(data);
                });
            }
        });
    }
    $(document).ready(function(){
        $('.alert-success').fadeIn().delay(3000).fadeOut();
        $('#id_password').on('keyup', function (e) {
            var password = $(this);
            if (e.keyCode != 32)
                check_for_password_strength(password)
        });
        pass="";
        $('input[data-toggle="popover"]').popover({
            placement: 'bottom',
            trigger: 'focus'
        });
        $('#id_password').on('keydown', function (e) {
             if (e.keyCode == 32) return false;
            }
        );
        $('#id_password1').on('keydown', function (e) {
             if (e.keyCode == 32) return false;
            }
        );
    });

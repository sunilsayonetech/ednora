// custom js for ednora
var cid  =0;
var title = ''
var get_content_info_url = '/api/get-content-info/';
var like_url = '/user-like/';
var like_ajax_url = '/my-likes/';
var create_folder_url = '/create-folder/';
var repin_url = '/re-pin/';
var flag_url = '/user-flag/'
var currenct_url ='';
var download_url = '/download-file?xxx'
var user_tracking_url  = '/user-tracking/'
var load_more_comments_api ='/api/load-more-comments/';
var popup_url = '';
var file_request = undefined;
var url_request = undefined;
var secured = "False";
var previous_buttion ='<ol class="carousel-indicators-top">\
                <a class="left carousel-control" href="#" data-slide="prev"><span class="lnr lnr-chevron-up"></span></a>\
            </ol>'
var next_buttion = '<ol class="carousel-indicators">\
                <a class="right carousel-control" href="#" data-slide="next"><span class="lnr lnr-chevron-down"></span></a>\
            </ol>'

function ChangeUrl(page, url) {
    if (typeof (history.pushState) != "undefined") {
        var obj = {Page: page, Url: url};
        history.pushState(obj, obj.Page, obj.Url);
    } else {
        alert("Browser does not support HTML5.");
    }
}

function CheckItemInArray(item, _array){
    return $.inArray(item, _array)!=-1;
}

// ----For adjusting profile pic----//

$(document).ready(function(){

    var imageWidth = $("#user_dp").width();
    var imageHeight = $("#user_dp").height();
    if(imageWidth>imageHeight){
        $("#user_dp").addClass("fixToHeight");
    }else{
        $("#user_dp").addClass("fixToWidth");
    }

});
// ----End for adjusting profile pic----//

var good = /^(?=.*[a-z])(?=.*[A-Z])(?=.*?[0-9]|.*?[#?!@$%^&*-])[a-zA-Z\d]{8,}/g;
//Must contain at least one upper case letter, one lower case letter and (one number and one special char).
var better = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/g;

function check_for_password_strength(password) {
    var pass = password.val();
    var stength = 'Weak';
    var pclass = 'danger';
    if (pass.match(better)) {
        stength = 'Strong';
        pclass = 'success';
    }
    else if (pass.match(good)) {
        stength = 'Fair';
        pclass = 'warning';
    }
    else {
        stength = 'Weak';
        pclass = 'danger';
    }
    var popover = password.attr('data-content', stength).data('bs.popover');
    popover.setContent();
    popover.$tip.addClass(popover.options.placement).removeClass('danger success info warning primary').addClass(pclass);

}

function facebook() {
    var inviteid = 0;
    var slug = 0;
    $('body').click();

    $.ajax({

        type: "POST",
        async: false,
        url: "/user-invite-facebok/",
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false,
        success: function (data) {
            inviteid = data.id;
            slug = data.slug
        }
    });

    FB.init({
        appId: '2003948349837814',
        status: true,
        cookie: true,
        xfbml: true
    });
    var link = 'https://www.ednora.com/upv/'+slug+'/?p=' + inviteid;
    FB.ui({
            app_id: '2003948349837814',
            method: 'send',
            name: "",
            link: link,
            to: "",
            display: 'dialog',
            mobile_iframe: true,
            message: 'Ednora'
        },
        function (response) {
            if (response && !response.error_message) {
                i = 0;

            } else {
                var data = new FormData();
                data.append("invite_id", inviteid);
                $.ajax({
                    data: data,
                    async: false,
                    type: "POST",
                    url: "/user-invite-delete/",
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false,
                    success: function (data) { // on success..

                    }
                });
            }
        });
    }
// ---------track sharing activity-------- //

function UserTracking(slug,media){
    var data = new FormData();
    data.append("content_slug", slug);
    data.append("type", 'cts');
    data.append("media", media);
    $.ajax({
        data: data,
        async: false,
        type: "POST",
        url: "/user-tracking/",
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false,
        success: function (data) { // on success..
            cid = data.id;
            title = data.title;
        }
    });
}

function facebookMessenger(slug,cid){
    FB.init({
        appId: '2003948349837814',
        status: true,
        cookie: true,
        xfbml: true
    });
    FB.ui({
        app_id:'2003948349837814',
        method: 'send',
        name: "",
        link: 'https://www.ednora.com/resource/' +slug.trim()+"/?p="+cid ,
        to:"",
        message: 'Ednora',
        mobile_iframe: true,
        display: 'dialog'
    },
    function(response) {
        if (response && !response.error_message) {
            i =0;

        } else{
            var data = new FormData();
            data.append("share_id", cid);
            $.ajax({
                data: data,
                async: false,
                type: "POST",
                url: "/user-share-delete/",
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false,
                success: function (data) { // on success..


                }
            });
        }
    }
)}

function popupwindow(url, title,  w, h){
   var y = window.top.outerHeight / 2 + window.top.screenY - ( h / 2)
   var x = window.top.outerWidth / 2 + window.top.screenX - ( w / 2)
   return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+y+', left='+x);
}
// --------- end track sharing activity-------- //
// validate email for all cases

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return email.match(re);
}
// check for valid url and return true or false
function validateUrl(value) {
    var regex = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    var match = value.match(regex);
    if (match)
        return true;
    else
        return false;
}

function createCookie(name, value, days) {
    var date, expires;
    if (days) {
        date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

// set csrf token
function setcsrf(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


// resend confirmation link to user
$('#resend-confirmation').click(function () {
    //check user is still logged in
    if (document.cookie.indexOf("loggedin") == -1) {
        window.location.href = window.location.href;
    }
    $('#verify_output_id_success').hide();
    $('#loader').attr('src', '/static/assets/img/loader.gif').show();
    var data = new FormData();
    var csrf = setcsrf('csrftoken');
    data.append("csrfmiddlewaretoken", csrf);
    data.append("user_id", $('#user_id').val());

    $.ajax({
        data: data,
        type: "POST",
        url: "/user-verify-link/",
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false,
        success: function (data) { // on success..
            if (data.status == "success") {

                if (data.done == true) {
                    $('#output_id').hide();
                    $('#loader').hide();
                    $('#verify_output_id_success').html("Successfully sent").show();

                }
            }
        }
    });
    return false;
});


// upload resource scripts

function func_url() {
    $('#fetched_image').empty();
    $('#upload_pin_file_id').val('');
    $('#upload_pin_file_id').hide();
    $('#upload_pin_url_id').show();
    $('#upload_pin_url_id').focus();
    $('#content_file_id').hide();
}
function func_file() {
    $('#fetched_image').empty();
    $('#upload_pin_url_id').val('');
    $('#upload_pin_url_id').hide();
    $('#upload_pin_file_id').show();
    $('#upload_pin_file_id').focus();

    $('#content_url_id').hide();

}
function prop_slected(values) {

    var list = [];
    for (var i = 0; i < values.options.length; i++) {
        if (values.options[i].selected) {
            list.push(values.options[i].value);
        }
    }
    return list;
}
// check the character count of resource title
function limit_title_characters(text) {
    var wordcount = text.length;
    if (wordcount > 255) {
        $(".exceed-characters-error").show();
        return false;
    } else {
        $(".exceed-characters-error").hide();
        return true
    }
}

$(document).ready(function () {

    // -------------------- upload section ---------------//
    $('.up-reslnk').click(function (e) {
        if (document.cookie.indexOf("loggedin") == -1) {
            window.location.href = window.location.href;
        }
        $('#content_text_id').val("");
        func_url();
//        $('#check2').prop('checked', false);
//        $('#check1').prop('checked', true);
        $('#fetched_image').empty();
        $('body').css('overflow','hidden');
        if (document.getElementById('dop_folder_id_up').options[0].text != "Select Folder*") {
            $("#dop_folder_id_up").prepend("<option value selected>Select Folder*</option>");
        }
        else {
            document.getElementById('dop_folder_id_up').selectedIndex = "0";
        }
        $('#upload_pin_url_id').focus();

    });
    $('.content-title').on('keyup', function (e) {
        limit_title_characters($(this).val())
    });

    $('#check1').click(function () {
        if ($(this).is(':checked')) {
            $('#check2').prop('checked', false);
        } else {
            $('#check2').prop('checked', true);
            func_file();
        }
    });
    $('#check2').click(function () {
        if ($(this).is(':checked')) {
            $('#check1').prop('checked', false);
        } else {
            $('#check1').prop('checked', true);
            func_url();
        }
    });

    // ------------------------------upload resource submit-----------------------------//
    $('#upload_pin_submit').click(function () {
        var send = true;
        var grades = document.getElementById("grade_id");
        var $grades_array = prop_slected(grades);
        //to validate grade field
        var $grade_error = $('#grade_field_error_id');
        if (!$grades_array.length > 0) {
            send = false;
            $grade_error.html('Required field').show();
        } else {
            $grade_error.hide();
        }

        var subjects = document.getElementById("subject_id");
        var $subjects_array = prop_slected(subjects);
        //to validate subject field
        var $subject_error = $('#subject_field_error_id');
        if (!$subjects_array.length > 0) {
            send = false;
            $subject_error.html('Required field').show();
        } else {
            $subject_error.hide();
        }

//        var levels = document.getElementById("level_id");
//        var $levels_array = prop_slected(levels);
        //to validate subject field
//        var $level_error = $('#level_field_error_id');
//        if (!$levels_array.length > 0) {
//            send = false;
//            $level_error.html('Required field').show();
//        } else {
//            $level_error.hide();
//        }

        // to validate the folder field
        var $folder_name = $('#dop_folder_id_up option:selected');
        var folder_name = $folder_name.text();
        var $folder_name_error = $('#content_output_folder_id');
        if (folder_name.match('Select Folder*')) {
            send = false;
            $folder_name_error.show();
            $folder_name_error.html('Required field');
        } else {
            $folder_name_error.hide();
        }

        //validate url or file content title
        if ($('#check1').is(':checked')) {
            var $url_id = $('#upload_pin_url_id');
            var url_value = $.trim($url_id.val());
            var $url_error = $('#content_url_id');
            if (url_value == '') {
                send = false;
                $url_error.html('Required field').show();
            }
            var url_check = validateUrl(url_value);
            if (url_check == false) {
                send = false;
            }

            if (url_check == true) {
                if (url_value.match(/ednora.com/gi)) {
                    send = false;
                }
            }
        }
        else {
            var $file_id = $('#upload_pin_file_id');
            var file_value = $.trim($file_id.val().split('.').pop());
            var $file_error = $('#content_file_id');
            if (file_value == '') {
                send = false;
                $file_error.html('Required field').show();
            }
        }
        //to validate the title field
        var $title = $('#content_titile_id');
        var title_value = $.trim($title.val());
        var $title_error = $('#content_output_tilte_id');
        if (title_value == '') {
            send = false;
            $title_error.html('Required field').show();
        } else {
            var size_val = limit_title_characters($title.val());
            if (!size_val) {
                send = false;
            }
            $title_error.hide();
        }
        //to validate the description field
        var $content_text = $('#content_text_id');
        var $content_text_value = $.trim($content_text.val());
        var $desc_error = $('#content_output_desc_id');
        if ($content_text_value == '') {
            send = false;
            $desc_error.html('Required field').show();
        } else {
            $desc_error.hide();
        }
        // check the content upload by which route
        var upload_route = '';
        if ($('#upload_pin_id').prop('disabled') == true) {
            upload_route = 'BB';
        } else {
            upload_route = 'UP';
        }

        if (!$('#check1').is(':checked')) {
            var file_data = $('#upload_pin_file_id').prop("files")[0];
        }

        //to validate the learning objective field
//        var $objective = $('#objective_id');
//        var objective_value = $.trim($objective.val()).replace (/[" ' ,]/g, "");
//        var $user_objective_error = $('#content_output_objective_id');
//        if (objective_value == '') {
//            send = false;
//            $user_objective_error.html('Required field').show();
//        } else {
//            $user_objective_error.hide();
//        }
        //to validate the content type field
//        var $content_type = $('#drop_content_id_up option:selected');
//        var content_type_value = $content_type.text();
//        var $content_type_error = $('#content_output_type_id');
//        if (content_type_value.match('Resource Type*')) {
//            send = false;
//            $content_type_error.html('Required field').show();
//        } else {
//            $content_type_error.hide();
//        }

        //to validate the media type field
//        var $media_type = $('#drop_media_id_up option:selected');
//        var media_type_value = $media_type.text();
//        var $media_type_error = $('#media_output_type_id');
//        if (media_type_value.match('Media Type*')) {
//            send = false;
//            $media_type_error.html('Required field').show();
//        } else {
//            $media_type_error.hide();
//        }
        // check for the existence of content in the same folder
        var fol_data = new FormData();
        fol_data.append("url", $('#upload_pin_url_id').val());
        fol_data.append("folder", folder_name);
        if ($('#check1').is(':checked')) {
            fol_data.append("type", 'url')
        } else {
            fol_data.append("type", 'file')
        }
        var $tag = $('#tags_id');
//        var $objective_code = $('#objectives_code_id');
        $.ajax({
            data: fol_data,
            type: "POST",
            url: "/check-url-in-folder/",
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false,
            success: function (data) {
                if (data.status == 'fail') {
                   $(".choose-another-folder").attr("style", "display:block");
                   send = false;
                } else {
                    $(".choose-another-folder").attr("style", "display:none");
                    if (send == true) {
                        $('#upload_pin_submit').hide();
                        $('#upload_cancel').hide();
                        var $image_url = $('#upload_loader');
                        $image_url.attr('src', '/static/assets/img/loader.gif');
                        $image_url.show();
                        // send ajax request to upload the content to the backend
                        //ga('send', 'event', 'Pin', 'Upload', 'Success');

                        var data = new FormData();
                        if ($('#check1').is(':checked')) {
                           data.append("url", $url_id.val());
                        } else {
                           data.append("file", file_data);
                        }
                        data.append("upload_route", upload_route);
                        data.append("csrf_middleware_token", setcsrf('csrftoken'));
                        data.append("user_id", $('#user_id').val());
                        data.append("title", $title.val());
                        data.append("grades", $grades_array);
                        data.append("subjects", $subjects_array);
//                        data.append("levels", $levels_array);
                        data.append("tags", $tag.val().replace(/^[,]+|[,]+$/g,'').trim());
//                        data.append("objectives", $objective.val().replace(/^[,]+|[,]+$/g,'').trim());
                        data.append("objectives", "") //passing dummy
//                        data.append("objectives_code", $objective_code.val().replace(/^[,]+|[,]+$/g,'').trim());
                        data.append("objectives_code", "") //passing dummy
                        data.append("image", $('input[name="image"]:checked').val());
//                        data.append("content_type", $content_type.text());
//                        data.append("media_type", $media_type.text());
                        data.append("folder", $folder_name.text());
                        data.append('article', $content_text_value);
                        $folder_name_error.hide();
                        $.ajax({
                            data: data,
                            type: "POST",// method
                            url: "/upload-pin/",// url for uploading pin
                            dataType: 'json',
                            processData: false, // Don't process the files
                            contentType: false,
                            success: function (data) {
                                if (data.validated == true) {
                                    $image_url.hide();
                                    $title_error.hide();
                                    $desc_error.hide();
//                                    $user_objective_error.hide();
//                                    $content_type_error.hide();
//                                    $media_type_error.hide();
                                    $folder_name_error.hide();
                                    $('#select_image').hide();
                                    var $success = $('#content_output_success_id');
                                    $success.html('Resource Saved Successfully');
                                    $success.show();
                                    setTimeout(function () {
                                      $("#upload-edit-modal").modal("hide");
                                      window.location.reload();

                                        }, 1000);

                                }

                            }

                        });
                    }
                }
            }

        });


    });
    // ------------------------------end upload resource submit-----------------------------//

    // ------------------ resource upload url section ------------------//

    var timer = null;
    $('#upload_pin_url_id').change(function (e) {
        var value = $('#upload_pin_url_id').val();
        var status = validateUrl(value);
        msg = '';
        if (status == false) {
            msg = 'Enter a valid url';
            $('#content_url_id').html(msg);
            $('#content_url_id').show();
        }

        if (status == true) {
            $('#content_url_id').hide();
            if (value.match(/ednora.com/gi)) {
                msg = 'Use Ednora to save other contents';
                $('#content_url_id').html(msg).show();
                return true;
            }
            clearTimeout(timer);
            timer = setTimeout(doStuff, 0)
        }

    });

    function doStuff() {
        //check user is still logged in
        if (document.cookie.indexOf("loggedin") == -1) {
            window.location.href = window.location.href;
        }
        if(file_request != undefined){
            file_request.abort();
        }
        value = $('#upload_pin_url_id').val();
//        var data = new FormData();
//        data.append("value", value);
//        data.append("user_id", $('#user_id').val());
//        $('#content_output_tilte_id').hide();
        if (!value.match("https://")) {

//            secured = "False";

            $('#fetched_image').empty();
            $('#fetched_image').attr('style', 'min-height:383px;');
//            $('#fetched_image').append("<img src =/static/assets/img/loader.gif height=50px; width=50px;  style='margin:40% 47% 48% 44%'> ");
//            $('#upload_pin_submit').attr('disabled', 'disabled');
            // new changes
            secured = "True"

        }
        else{
            secured = "True"
            $('#fetched_image').empty();

        }
        var subjects_slected = document.getElementById("subject_id");
        var $subjects_slected_array = prop_slected(subjects_slected);
        if ($subjects_slected_array.length){
            $('#subject_id').change()
        }
        // fetch content title
//        $.ajax({
//            data: data,
//            type: "POST",
//            url: "/get-content/",
//            dataType: 'json',
//            processData: false, // Don't process the files
//            contentType: false,
//            success: function (data) {
//                if (data.done == true) {
//                    $('#content_url_id').hide();
//                    $('#content_titile_id').val((data.title).substring(0, 70));
//                    if (data.title == ' ') {
//                        $('#content_titile_id').val("");
//                        $('#content_url_id').html("No content title is available from this url");
//                        $('#content_text_id').focus();
//                    }
//
//                    $('#content_titile_id').show();
//                    if (!$('#content_titile_id').val().length == 0) {
//                        $('#content_text_id').focus();
//                    }
//
//                }
//
//            }
//        });

        // fetch content image
        if (secured === "False") {
            url_request = $.ajax({
                                     data: data,
                                     type: "POST",
                                     url: "/get-content-image/",
                                     dataType: 'json',
                                     processData: false, // Don't process the files
                                     contentType: false,
                                     success: function (data) {
                                         if (data.status == 'success') {
                                             $('#fetched_image').replaceWith(data.html);
                                             $('#upload_pin_submit').removeAttr('disabled');
                                         }
                                         if (data.status == 'fail') {
                                             $('#fetched_image').replaceWith(data.html);
                                             $('#upload_pin_submit').removeAttr('disabled');
                                             var resource_text = $('#drop_content_id_up').val();
                                             if (!resource_text.match('Resource Type*')) {
                                                 $('#drop_content_id_up').change();
                                             }
                                         }

                                     }, error: function (data) {
                    $('#upload_pin_submit').removeAttr('disabled');
                    $('loader').hide()
                }
                                 });
        }


    }

    // ------------------ end resource upload url section ------------------//

    // ---------------------- folder section --------------------------//
        // Create folder option selection
        $('#dop_folder_id_up').click(function (e) {

            if ($('#dop_folder_id_up').val() == 'Create Folder') {
                $('#createfolder').modal('show');
            }

        });
    // -------------------end folder section -----------------------------//


        //--------------------upload resource for file section -------------------------//

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('fetched_image').empty();
                    $('#fetched_image').attr('src', '/static/assets/img/loader.gif');
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        function fetchFromContent(resource_text){
            $('#fetched_image').empty();
            $('#fetched_image').append("<img id='this_laoder' src =/static/assets/img/loader.gif height=50px; width=50px;  style='margin:40% 47% 48% 44%'> ");
            var data = new FormData();
            data.append('content_type', resource_text);
            if (!resource_text.match('Resource Type*')) {
                $.ajax({
                    data: data,
                    type: "POST",
                    url: "/get-resource-image/",
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false,
                    mimeType: "multipart/form-data",
                    success: function (data) {
                        $('#fetched_image').replaceWith(data.html);
                        $('#upload_pin_submit').removeAttr('disabled');
                    }, error: function (data) {
                        $('#upload_pin_submit').removeAttr('disabled');
                        $('#this_laoder').hide()
                    }
                });

            }else{
                $('#this_laoder').hide()
            }
        }

        $("#upload_pin_file_id").change(function () {
            readURL(this);
            $('#fetched_image').empty();
            var dummy_title = $('#upload_pin_file_id').val().replace(/C:\\fakepath\\/i, '');
            var file_title = $('#upload_pin_file_id').val().replace(/C:\\fakepath\\/i, '').replace(/\.[^/.]+$/, "");
            var extension = dummy_title.replace(/^.*\./, '');
            var blocked_extensions = ["exe", "pif", "application", "gadget", "msi", "msp", "com", "scr", "hta", "cpl", "msc",
                "jar", "bat", "cmd", "vb", "vbs", "vbe", "js", "jse", "ws", "wsf", "wsc", "wsh", "ps1", "ps1xml", "ps2", "ps2xml", "psc1",
                "psc2", "msh", "msh1", "msh2", "mshxml", "msh1xml", "msh2xml", "scf", "lnk", "inf", "reg"];
            var google_supported_extensions = ['docx', 'pptx', 'xlsx', 'dotx', 'doc', 'ppt', 'png', 'jpeg', 'xls', 'jpg', 'pdf', 'gif'];
            var blocked = CheckItemInArray(extension, blocked_extensions);
            var supported = CheckItemInArray(extension.toLowerCase(), google_supported_extensions);
            if(url_request != undefined){
                url_request.abort();
            }
            $('#content_file_id').text('');
            // If there is no dot anywhere in filename, we would have extension == filename,
            // so we account for this possibility now
            if (extension == dummy_title) {
                extension = '';
            } else {
                // if there is an extension, we convert to lower case
                // (N.B. this conversion will not effect the value of the extension
                // on the file upload.)
                extension = extension.toLowerCase();
            }
            if (blocked) {
                $('#upload_pin_file_id').val("");
                $('#content_file_id').html("Format not supported");
                $('#content_file_id').show()
            } else if (!supported) {
//                var resource_text = $('#drop_content_id_up').val();
//                if (!resource_text.match('Resource Type*')) {
//                    $('#drop_content_id_up').change();
//                }

                var subjects_slected = document.getElementById("subject_id");
                var $subjects_slected_array = prop_slected(subjects_slected);
                if ($subjects_slected_array.length){
                    $('#subject_id').change()
                }
            }
            else {
                $('#content_file_id').hide();
                var data = new FormData();
                data.append("file_1", $('#upload_pin_file_id').prop("files")[0]);
                $('#fetched_image').append("<img src =/static/assets/img/loader.gif height=50px; width=50px;  style='margin:40% 47% 48% 44%'> ");
                $('#upload_pin_submit').attr('disabled', 'disabled');
                file_request =$.ajax({
                    data: data,
                    type: "POST",
                    url: "/get-file-image/",
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false,
                    mimeType: "multipart/form-data",
                    success: function (data) {
                        if (data.status == 'success') {
                            $('#fetched_image').replaceWith(data.html);
                            $('#upload_pin_submit').removeAttr('disabled');
                        }if (data.status == 'fail') {
                            $('#fetched_image').replaceWith(data.html);
                            $('#upload_pin_submit').removeAttr('disabled');
                            var resource_text = $('#drop_content_id_up').val();
                            if (!resource_text.match('Resource Type*')) {
                                $('#drop_content_id_up').change();
                            }
                        }

                    }, error: function (data) {
                        $('#upload_pin_submit').removeAttr('disabled');
                        $('loader').hide()
                    }
                });

            }

        });
//        $('#drop_content_id_up').change(function () {
//            var sent_req = true;
//            if(file_request != undefined && file_request.state() === 'pending'){
//                sent_req = false;
//            }if(url_request != undefined && url_request.state() === 'pending'){
//                sent_req = false;
//            }
//            if(sent_req){
//                console.log('yes going')
//                var dummy_title = $('#upload_pin_file_id').val().replace(/C:\\fakepath\\/i, '');
//                var file_title = $('#upload_pin_file_id').val().replace(/C:\\fakepath\\/i, '').replace(/\.[^/.]+$/, "");
//                var extension = dummy_title.replace(/^.*\./, '');
//                var google_supported_extensions = ['docx', 'pptx', 'xlsx', 'dotx', 'doc', 'ppt', 'png', 'jpeg', 'xls', 'jpg', 'pdf', 'gif'];
//                var supported = CheckItemInArray(extension.toLowerCase(), google_supported_extensions);
//                var resource_text = $('#drop_content_id_up').val();
//                var api_status = $('#api-status').val();
//                if (!supported && extension != '') {
//                    fetchFromContent(resource_text);
//                }if(supported){
//                    if (api_status != 'success'){
//                        fetchFromContent(resource_text);
//                    }
//                }
//                var is_it_url = $('#upload_pin_url_id').val();
//                if(is_it_url){
//                    if (api_status != 'success'){
//                        fetchFromContent(resource_text);
//                    }
//                }
//            }
//
//        });

// ------------default image changed to subjcet change-------------//

        $('#subject_id').change(function () {
            var sent_req = true;
            if(file_request != undefined && file_request.state() === 'pending'){
                sent_req = false;
            }if(url_request != undefined && url_request.state() === 'pending'){
                sent_req = false;
            }
            if(sent_req){
                console.log('yes going')
                var dummy_title = $('#upload_pin_file_id').val().replace(/C:\\fakepath\\/i, '');
                var file_title = $('#upload_pin_file_id').val().replace(/C:\\fakepath\\/i, '').replace(/\.[^/.]+$/, "");
                var extension = dummy_title.replace(/^.*\./, '');
                var google_supported_extensions = ['docx', 'pptx', 'xlsx', 'dotx', 'doc', 'ppt', 'png', 'jpeg', 'xls', 'jpg', 'pdf', 'gif'];
                var supported = CheckItemInArray(extension.toLowerCase(), google_supported_extensions);
//                var resource_text = $('#drop_content_id_up').val();
                var resource_text = 'Resource Type*'
                var subjects_slected = document.getElementById("subject_id");
                var $subjects_slected_array = prop_slected(subjects_slected);
                if ($subjects_slected_array.length){
                    if ($subjects_slected_array.length>1){
                        resource_text = '';
                    }else{
                        resource_text = $subjects_slected_array[0]
                    }

                }
                var api_status = $('#api-status').val();
                if (!supported && extension != '') {
                    fetchFromContent(resource_text);
                }if(supported){
                    if (api_status != 'success'){
                        fetchFromContent(resource_text);
                    }
                }
                var is_it_url = $('#upload_pin_url_id').val();
                if(is_it_url){
                    if (api_status != 'success' && resource_text != ''){
                        fetchFromContent(resource_text);
                    }
                }
            }

        });
        //--------------------end upload resource for file section -------------------------//

        // ---------------------end upload section---------------------------//

        $('#createfolder').on('hidden.bs.modal', function (e) {
            $('#folder_input_id').val('');
            $('#output_id_create').hide();
            $('#output_id_success_create').hide();
            $('#create_folder_id').attr('disabled', false);
            if (document.getElementById('drop-repin').value == "Create Folder") {
                $('#drop-repin').val('Select Folder*').prop('selected', true);
            }
            if (document.getElementById('dop_folder_id_up').value == "Create Folder") {
                $('#dop_folder_id_up').val('Select Folder*').prop('selected', true);
            }

        });
        $('#createfolder').on('shown.bs.modal', function () {
            $('#folder_input_id').focus();
        });

        $('#create_folder_id').click(function (e) {
            //check user is still logged in
            if (document.cookie.indexOf("loggedin") == -1) {
                window.location.href = window.location.href;
            }
            $('#create_folder_id').attr('disabled', 'disabled');
            var $fol_name = $('#folder_input_id').val();
            var data = new FormData();
            data.append("folder_name", $fol_name);
            data.append("csrf_middleware_token", setcsrf('csrftoken'));
            data.append("user_id", $('#user_id').val());
            $.ajax({
                data: data,
                type: "POST",
                url: "/create-folder/",
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false,
                success: function (data) { // on success..
                    if (data.status == "success") {
                        if (data.empty == true) {
                            $('#output_id_create').html("Required field").show();
                            $('#create_folder_id').attr('disabled', false);
                            return false;
                        }
                        if (data.exists == true) {
                            $('#output_id_success_create').hide();
                            $('#output_id_create').html("Folder already exists").show();
                            $('#create_folder_id').attr('disabled', false);
                            return false;
                        }
                        if (data.done == true) {
                            $('#output_id_create').hide();
                            $('#output_id_success_create').html("Successfully created").show();
                            update_folder_list();
                            setTimeout(function () {
                                $('#createfolder').modal('toggle');

                            }, 1000);

                        }
                        if (data.length == true) {
                            $('#output_id_create').html("Name should be less than 20 characters").show();
                            $('#create_folder_id').attr('disabled', false);
                            return false;
                        }

                    }
                }
            });
        });

        function update_folder_list(){
            $.ajax({
                type: "GET",
                url: "/user-folders/",
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false,
                success: function(data) {
                    if (data.status == "success") {

                        $('.folder-list').each(function(){
                            $(this).html('');
                            $(this).append(data.html)
                        })

                    }

                }

            });
        }


        $('.my-folders-list').click(function () {
            //check user is still logged in
            if (document.cookie.indexOf("loggedin") == -1) {
                window.location.href = window.location.href;
            }
            if ($(".fol-menu").attr("class") == "open")
                $(".fol-menu").removeClass("open");
            else {
                var data = new FormData();
                data.append("csrf_middleware_token", setcsrf('csrftoken'));
                $.ajax({
                    data: data,
                    type: "POST",
                    url: "/user-folders-home/",
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false,
                    success: function (data) { // on success..
                        if (data.status == "success") {
                            $('inner_menu').html(data.html);
                            $(".fol-menu").addClass("open");
                            $("#dLabel1").attr("aria-expanded", "true");

                        }
                    }

                });
            }
            return false;
        });

        $('#dLabel').click(function () {
            //check user is still logged in
            if (document.cookie.indexOf("loggedin") == -1) {
                window.location.href = window.location.href;
            }
            if ($("#loc1").attr("class") == "open")
                $("#loc1").removeClass("open");
            else {
                var data = new FormData();
                data.append("csrf_middleware_token", $('#csrf_token').val());

                $.ajax({
                    data: data,
                    type: "POST",
                    url: "/user-folders-home/",
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false,
                    success: function (data) { // on success..
                        if (data.status == "success") {
                            $('#folder1').html(data.html);
                            $("#loc1").addClass("open");
                            $("#dLabel").attr("aria-expanded", "true");


                        }
                    }

                });
            }
            return false;
        });

        $('#dLabel1').click(function () {
            //check user is still logged in
            if (document.cookie.indexOf("loggedin") == -1) {
                window.location.href = window.location.href;
            }
            if ($("#loc").attr("class") == "open")
                $("#loc").removeClass("open");
            else {
                var data = new FormData();
                data.append("csrf_middleware_token", $('#csrf_token').val());

                $.ajax({
                    data: data,
                    type: "POST",
                    url: "/user-folders-home/",
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false,
                    success: function (data) { // on success..
                        if (data.status == "success") {
                            $('#folder').html(data.html);
                            $("#loc").addClass("open");
                            $("#dLabel1").attr("aria-expanded", "true");


                        }
                    }

                });
            }
            return false;
        });
   });

    $(document).on('keydown', '#dop_folder_id_up', function (e) {
            if ($('#dop_folder_id_up').val() == 'Create Folder'&&e.keyCode == 13) {
                $('#createfolder').modal('show');
            }
    });
    $(document).on('keydown', '#edit_dop_folder_id_up', function (e) {
            if ($('#edit_dop_folder_id_up').val() == 'Create Folder'&&e.keyCode == 13) {
                $('#createfolder').modal('show');
            }
    });
    $(document).on('keydown', '#upload_pin_url_id', function (e) {
        if (e.keyCode == 32) return false;
    });

    $(document).on('hidden.bs.modal', '#upload-edit-modal', function (e) {
        $(this).find("input,textarea,select").val('')
            .end();
        $(this).find("p").hide().end();
        $('.exceed-characters-error').hide();
        $('.choose-another-folder').attr("style", "display:none");
        $(".multiselect").select2();
        $('#drop_content_id_up').val('Resource Type*');
        $('#drop_media_id_up').val('Media Type*');
        $('body').css('overflow','auto')
        if (document.getElementById('dop_folder_id_up').options[0].text != "Select Folder*") {
            $("#dop_folder_id_up").prepend("<option value selected>Select Folder*</option>");
        }
        else {
            document.getElementById('dop_folder_id_up').selectedIndex = "0";
        }

    });


    //--------------enter key press events ----------------------//
    $(document).on('keypress', '#folder_input_id', function (e) {
        if(e.keyCode == 13) {
            $('#create_folder_id').click();
        }
    });
    $(document).on('keypress', '#folder_input_id_in_upload', function (e) {
        if(e.keyCode == 13) {
            $('#create_folder_id_in_upload').click();
        }
    });
    //---------------end enter key press events -------------------//


    //-------------- resource detail popup --------------------------------//

    $(document).on('click', '.original_image', function () {

        // check the user is still logged in
        if (document.cookie.indexOf("loggedin") == -1) {
            window.location.href = window.location.href;
        }
        currenct_url = window.location.pathname;
        popup_url = window.location.pathname;
        changeurl = '/resource/' + $(this).attr('data-url').trim() + '/';
        ChangeUrl('view_content', changeurl);
        populate_resource_detail_popup($(this).attr('data-url'))
    });

    var populate_resource_detail_popup = function (slug) {
        if (document.cookie.indexOf("loggedin") !== -1) {
            $('#pin_detail1').modal('show');
        }
        $('#related-resource').html('');
        $("#cont_detail_more").text("");
        $('#id_show_detailed_text').hide();
        var folder  = $('#folder_name_hidden').val();
        $('#edit_button').attr({'data-folder': window.location.href});
        data = {'slug': slug, 'csrfmiddlewaretoken': setcsrf('csrftoken')};

        $.post(get_content_info_url, data, function (data, status) {
            $('#headtitle').text(data.title);
            $('#id_state').text(data.state);
            $('#id_district').text(data.district);
            $('#id_grade').text(String(data.grade))
            $('#id_objective').text(String(data.objective))
            $('#id_learning_objective').text(String(data.content_learning_objective_code))
            $('#id_subjects').text(String(data.subject))
            $('#id_level').text(data.content_level)
            $('#content_type').text(data.content_type)
            $('#media_type').text(data.media_type)
            $('#id_tags').text(String(data.tags))
            $('#id_dp_url').attr('src', data.user_image)
            $('#id_posted_by').text(data.user)
            $('#id_viewno').text(data.view_count)
            $('#downloadcountinpd').text(data.download_count)
            $('#likecountinpd').text(data.like_count)
            $('#image_pr').attr('src', data.image);
            $('#id_slug').val(data.id);
            $('#save-pin-detail-view').attr('data-slug', data.id);
            $('#comment_text_id').val('')
            $('#comment_lists').html("");
            $('#comment_lists').append(data.comments);
            $('.reply-comment').hide()
            $('#showLess').hide();
            $('.popup-delete').attr('data-pin-id', data.id);
            $('#facebook').attr('data-url', data.slug);
            $('#twitter').attr('data-url', data.slug);
            $('#google').attr('data-url', data.slug);
            $('#Messenger').attr('data-url', data.slug);
            $('#emailshare').attr('data-url', data.slug);
            var created_at = new Date(data.created);
            $('#id_created_on').text(dateFormat(created_at, "mmm d, yyyy"))


            if (data.related_resources)
            {
                if($('#related-resource').hasClass('slick-initialized'))
                {
                    $('#related-resource').slick('unslick');
                }
                $('#related_resource').show();
                $('#related-resource').html(data.related_resources);
                $('#related-resource').not('.slick-initialized').slick({vertical:true,slidesToShow: 2, slidesToScroll:2, prevArrow:previous_buttion, nextArrow:next_buttion});
                $(window).resize();
                $('[data-slide="next"]').trigger('click')
            }
            else{
                $('#related_resource').hide();
            }

            if (data.show_more_comments)
            {
                $('#loadMore').show();
                $('#loadMore').attr('data-page',1);
            }
            else{
                $('#loadMore').hide();
                $('#loadMore').removeAttr('data-page');
            }

            if(data.is_flagged)
            {
                $('#user_flag_status').attr('class','unflag_red')
            }
            else{
                $('#user_flag_status').attr('class','')
            }

            if (data.is_liked) {

                $('#user_liked_sattus').attr('style', "background:rgba(0,86,151,1); color:#fff!important;");
            }
            else {
                $('#user_liked_sattus').removeAttr('style')
            }

            if (data.is_file) {
                $('#id_viewcount_modal').hide();
                $('#image_pr').removeClass('view-url');
                $('#link').attr('href',"javascript:void(0);");
                $('#link').removeAttr('target')
                $('#image_pr').attr('data-url','')
                $('#resourceLinkId').attr('href', data.file_upload)
                var resourceId=($('#id_slug').val());
                $('#dwnld_count').attr('href', '/download-file/'+resourceId)
                $('#id_download_modal').show();
            }
            else {
                $('#link').attr('href',data.absolute_url);
                $('#link2').attr('href',data.absolute_url);
                $('#link3').attr('href',data.absolute_url);
                $('#image_pr').addClass('view-url');
                $('#image_pr').attr('data-url',data.absolute_url);
                $('#resourceLinkId').attr('href', data.absolute_url);
                $('#link').attr('target','_blank');
                $('#link2').attr('target','_blank');
                $('#link3').attr('target','_blank');
                $('#id_viewcount_modal').show();
                $('#id_download_modal').hide()
            }

            $('#cont_detail .text').text(data.trimmed_content);
            $('#cont_detail').show();

            if(data.is_content_long)
            {
                 $("#cont_detail_more").hide()
                $("#cont_detail_more").text(data.text);
                $('#id_show_detailed_text').show();
            }
            else{

                 $('#id_show_detailed_text').hide();
            }
            var pin_owner = "#pin_id_"+data.id;
            var edit_show = $(pin_owner).val();
            if(edit_show ==1 && folder != undefined){
                $('#edit_button_ul').show();
                $('#id_delete1').hide();
            }
            else if(edit_show ==0 && folder != undefined){
                $('#edit_button_ul').hide();
                $('#id_delete1').show();
            }

            data = {'content_id':data.id, 'type':'cte' , 'csrfmiddlewaretoken':setcsrf('csrftoken')};
            $.post(user_tracking_url, data, function(data, status){
            })
        });

    };

    $('#pin_detail1').on('hidden.bs.modal', function (e) {
        var resource_view = $('#in_home_page').val();
        if (resource_view != 1){
            str = window.location;
            if (String(str).slice(-1) == '#') {
                history.go(-2);
            }
            else {
                history.go(-1);
            }
        }else{
            window.location.href = '/home/'
        }
    });


    //----------------------- end  resource detail popup ------------------------------//

    //------------------------ like for home /folder /like  sections--------//

    $(document).on('click', '.like_image', function (e) {

        // check the user is still logged in
        if (document.cookie.indexOf("loggedin") == -1) {
            window.location.href = window.location.href;
        }

        if ($(this).attr('style') == undefined) {
            $(this).removeAttr('style');
            $(this).attr('style', "background:rgba(0,86,151,1); color:#fff!important;");

        }
        else if ($(this).attr('style').trim().length == 0) {
            $(this).removeAttr('style');
            $(this).attr('style', "background:rgba(0,86,151,1); color:#fff!important;");
        }
        else {
            $(this).removeAttr('style');
        }

        id = $(this).attr('id').split('_')[1]

        data = {'content_id': id, 'csrfmiddlewaretoken': setcsrf('csrftoken')}

        if (window.location.pathname.split("/")[1] == "folder" || window.location.pathname.split("/")[1] == "home"|| window.location.pathname.split("/")[1] == "search") {
            // restricting the script for home and folder

            $.post(like_url, data, function (data, status) {
                $('#liked_count_lke_' + id).text(data.count);
            });

        }
        else {
            $.post(like_ajax_url, data, function (data, status) {
                $('#my-gallery-container').html(data.html);
                if (data.count == 0) {
                    $('#no-pin-liked').removeAttr('style');
                }
                $('.mpmansory-applied').unwrap();
                $("#my-gallery-container").mpmansory(
                    {
                        childrenClass: 'item', // default is a div
                        columnClasses: 'padding', //add classes to items
                        breakpoints: {
                            lg: 4,
                            md: 4,
                            sm: 6,
                            xs: 12
                        },
                        distributeBy: {order: false, height: false, attr: 'data-order', attrOrder: 'asc'}, //default distribute by order, options => order: true/false, height: true/false, attr => 'data-order', attrOrder=> 'asc'/'desc'
                        onload: function (items) {
                            items.addClass('mpmansory-applied');
                        }
                    }
                );

            });
        }
    });
    //------------------------ end  like for home /folder /like  sections--------//

    //------------------------ like resource section  sections--------//

    $(document).on('click', '#user_liked_sattus', function (e) {
        if (document.cookie.indexOf("loggedin") == -1) {window.location.href = window.location.href;}
        data = {'content_id': $('#id_slug').val(), 'csrfmiddlewaretoken': setcsrf('csrftoken')}

        if ($(this).attr('style') == undefined) {
            $(this).removeAttr('style');
            $(this).attr('style', "background:rgba(0,86,151,1); color:#fff!important;");

        }
        else if ($(this).attr('style').trim().length == 0) {
            $(this).removeAttr('style');
            $(this).attr('style', "background:rgba(0,86,151,1); color:#fff!important;");
        }
        else {
            $(this).removeAttr('style');
        }
        if (currenct_url.match("folder") || currenct_url.match("home")||currenct_url.match('resource')||currenct_url.match('search')) {
            // restricting the script for home and folder

            $.post(like_url, data, function (data, status) {
                $('#likecountinpd').text(data.count);
                if(data.liked==true)
                {
                    $('#lke_' + $('#id_slug').val()).attr('style', "background:rgba(0,86,151,1); color:#fff!important;");
                    $('#liked_count_lke_'+$('#id_slug').val()).text(data.count)
                }
                else{
                    $('#lke_' + $('#id_slug').val()).attr('style', "");
                    $('#liked_count_lke_'+$('#id_slug').val()).text(data.count)
                }
          })
        }
        else if (currenct_url.match('my-likes')) {
            $('#lke_' + $('#id_slug').val()).trigger('click');
            $('#pin_detail1').modal('hide');

        }

    })

    //------------------------ end like resource section  sections--------//


    //-------------------------------------re-pin in listing ----------------------//

    $(document).on('click', '.share123', function (e) {

        //check user is still logged in
        if (document.cookie.indexOf("loggedin") == -1) {
            window.location.href = window.location.href;
        }
        var slug = $(this).attr('id').split('_')[1];
        $('#repin-slug').val(slug);
        $('#image_repin').attr('src', $(this).parents('.white-panel-new').find('.original_image').data('src'))
        $('#repin_popup').modal('show');
    });


    $(document).ready(function () {
        $('#drop-repin').change(function (e) {
            if ($('#drop-repin').val() == 'Create Folder') {
                $('#createfolder').modal('show')
            }
        });
    });


    $('#folder_input_id_in_re_pin').keyup(function (e) {
        if (e.keyCode == 13) {
            create_folder_in_repin_popup();
        }
    });

    $('#create_folder_id_in_re_pin').click(function () {
        create_folder_in_repin_popup();
    });

    var create_folder_in_repin_popup = function () {

        if ($('#folder_input_id_in_re_pin').val().trim().length == 0) {
            $('#output_id_in_re_pin').html("Required field");
        }
        else {
            $('#output_id_in_re_pin').empty();
            data = {'folder_name': $('#folder_input_id_in_re_pin').val(), 'csrfmiddlewaretoken': setcsrf('csrftoken')}
            $.post(create_folder_url, data, function (data, status) {
                if (data.status == "success") {
                    if (data.empty == true) {
                        $('#output_id_in_re_pin').html("Required field");
                        $('#output_id_in_re_pin').show();
                        return false;
                    }
                    if (data.exists == true) {
                        $('#output_id_in_re_pin').html("Folder already exists");
                        $('#output_id_in_re_pin').show();
                        return false;
                    }
                    if (data.done == true) {
                        $('#drop-repin').prepend('<option value="' + data.slug + '">' + $('#folder_input_id_in_re_pin').val() + '</option>')
                        $('#drop-repin option[value=' + data.slug + ']').attr('selected', 'selected');

                        $('#output_id_success_in_re_pin').html("Folder created successfully").show();
                        ;
                        setTimeout(function () {
                            $('#to_test2').hide();
                            $('#re_pin_popo_up_div').show();
                        }, 1000);
                    }
                    if (data.length == true) {

                        $('#output_id_in_upload').html("Name should be less than 20 characters");
                        $('#output_id_in_upload').show();
                        return false;
                    }

                }
            })
        }
    }


    $('#repin_popup').on('hidden.bs.modal', function (e) {

        $("#drop-repin").val("Select Folder*")
        $('#repin_tag').val('');
        $('#folder_input_id_in_re_pin').val('');
        $('#repin_output_validated').text('').hide();
        $('#repin_output').text('').hide()

    });

    $('#upload_cancel_re_pin').click(function () {
        //check user is still logged in
        if (document.cookie.indexOf("loggedin") == -1) {
            window.location.href = window.location.href;
        }
        $('#to_test2').hide();
        $('#re_pin_popo_up_div').show();
        if (document.getElementById('drop-repin').options[0].text != "Select Folder*") {
            $("#drop-repin").prepend("<option value selected>Select Folder*</option>");
        }
        else {
            $('#drop-repin').val('Select Folder*').prop('selected', true);
        }
    });


    //re pin save
    $('#repin_save').click(function () {
        //check user is still logged in
        if (document.cookie.indexOf("loggedin") == -1) {
            window.location.href = window.location.pathname;
        }
        var status = true;
        if ($('#drop-repin option:selected').text() == 'Select Folder*') {
            $('#repin_output_re_pin').show();
            $('#repin_output_re_pin').html("Required field");
            $('#repin_output_validated').html('')
            status = false;
        }
        if (status == true) {
            $('#repin_output_re_pin').html('').hide();
            $('#repin_output_validated').hide();
            folder = $('#drop-repin option:selected').val();
            content = $('#repin-slug').val()
            tags = $('#repin_tag').val()
            data = {'folder': folder, 'content': content, 'tags': tags, 'csrfmiddlewaretoken': setcsrf('csrftoken')}
            $.post(repin_url, data, function (data, status) {
                if (data.done == true) {
                    if (data.msg) {
                        $('#repin_output_validated').show();
                        $('#repin_output_validated').html("" + data.msg);
                    }

                    else {
                        $('#repin_output').show();
                        $('#repin_output').html("Resource Saved Successfully");
                        setTimeout(function () {
                            $("#repin_popup").modal('hide');
                        }, 1000);
                    }
                }
            })
        }
    })
   //-------------------------------------end re-pin in listing -----------------------//


    //-------------------- repin in  detail popup -----//
    $('#save-pin-detail-view').click(function () {
        if (document.cookie.indexOf("loggedin") == -1) {window.location.href = window.location.href;}
        $('#image_repin').attr('src', $('#image_pr').attr('src'));
        slug = $(this).attr('data-slug');
        $('#repin-slug').val(slug)
        $("#repin_popup").modal('show');
    });
    //------------------------------end reping in detail popup//


    //------------------- user content flagging----------------- //

    $(document).on('click', '#user_flag_status', function(){
        if (document.cookie.indexOf("loggedin") == -1) {window.location.href = window.location.href;}
        if($(this).hasClass('unflag_red'))
        {
            // unflag resouce
            $('#unflag_pin').modal('show')
        }
        else{
            // to flag a resource
        $('#flag_pin').modal('show');

        }


    });



    $(document).on('click', '#confirm_flag_pin', function(){
       var data = {'content_id': $('#id_slug').val(), 'csrfmiddlewaretoken': setcsrf('csrftoken')}
        process_flagging(data, true)
    });

    var process_flagging  = function(data, flag){
        $.post(flag_url, data, function(data, status){
            if(data.flagged==true)
            {
                $('#user_flag_status').prop('title', 'Unlflag');
                $('#user_flag_status').attr('class', 'unflag_red');
            }
            else{
                $('#user_flag_status').prop('title', 'Unlflag');
                $('#user_flag_status').attr('class', '');
            }
        });

        if(flag)
        {
            $('#flag_pin').modal('hide')
        }
        else{
             $('#unflag_pin').modal('hide')
        }
    }

    //-------------------------------download file------------------------------//


$(document).on('click','#dwnld_count', function(e){
    if (document.cookie.indexOf("loggedin") == -1) {window.location.href = window.location.href;}
    $('#download_file_link_iframe').remove();
    var url = download_url.replace('xxx', $('#id_slug').val())
    $(this).next('span').text(parseInt($(this).next('span').text())+1)
    $('#download_count_'+ $('#id_slug').val()).text(parseInt($('#download_count_'+ $('#id_slug').val()).text())+1)
     $("body").append("<iframe id='download_file_link_iframe' src='"+url+"' style='display: none;'></iframe>");
});

$(document).on('click', '.download_button', function(){
    $(this).next('span').text(parseInt($(this).next('span').text())+1)

});

$(document).on('click','.view-url', function(e){

    $('#id_viewno').text(parseInt($('#id_viewno').text())+1);
    $('#view_count_'+$('#id_slug').val()).text(parseInt($('#view_count_'+$('#id_slug').val()).text())+1)

    data = {'content_id':$('#id_slug').val(), 'type':'cfu' , 'csrfmiddlewaretoken':setcsrf('csrftoken')}

    $.post(user_tracking_url, data, function(data, status){
    })
});

$(document).on('click','#link', function(){

     if(!$(this).attr('href').match('javascript:void(0)')){

    $('#id_viewno').text(parseInt($('#id_viewno').text())+1);
    $('#view_count_'+$('#id_slug').val()).text(parseInt($('#view_count_'+$('#id_slug').val()).text())+1)
    data = {'content_id':$('#id_slug').val(), 'type':'cfu' , 'csrfmiddlewaretoken':setcsrf('csrftoken')}
    $.post(user_tracking_url, data, function(data, status){
    })
     }
});
$(document).on('click','#apply-filter', function(){
    var q = document.getElementById("query-search").value;
    if(q==undefined)
        q="";
    var state = $('#filter_state_id').find(":selected").val();
    if(state == "Select State") {
        state = ""
    }
    var district = $('#filter_district_id').find(":selected").val();
    if(district == "Select District") {
        district = ""
    }
    var grade = $("#filter_grades option:selected");
    var message_grade = "";
    grade.each(function () {
        message_grade += $(this).val() + ",";
    });
    var subject = $('#filter_subjects').find(":selected");
    var message_subject = "";
    subject.each(function () {
        message_subject += $(this).val() + ",";
    });
    var learning = $('#filter_learning').find(":selected");
    if (learning.first().val()==''&&learning.length<=1){
        delete learning.first()
        learning.length=0;
        var message_learning = "";
    }
    else {
        var message_learning = "";
        learning.each(function () {
            if ($(this).val()!='') {
                message_learning += $(this).val() + ",";
            }
        })
    };
    var code = $('#filter_code_id').find(":selected");
    if (code.first().val()==''&&code.length<=1){
        delete code.first()
        code.length=0;
        var message_code = "";
    }
    else {
        var message_code = "";
        code.each(function () {
            if ($(this).val()!='') {
                message_code += $(this).val() + ",";
            }
        })
    };
    var level = $('#filter_level_id').find(":selected");
    var message_level = "";
    level.each(function () {
        message_level += $(this).val() + ",";
    });
    var resource = $('#filter_resource').find(":selected");
    var message_resource = "";
    resource.each(function () {
        message_resource += $(this).val() + ",";
    });
    var media = $('#filter_media').find(":selected");
    var message_media = "";
    media.each(function () {
        message_media += $(this).val() + ",";
    });
    if(state.length||district.length||grade.length||subject.length||learning.length||code.length||level.length||resource.length||media.length!=0) {
        window.location.href = "/search" + "?q=" + q + "&state=" + state + "&district=" + district + "&grade=" + message_grade + "&subject=" + message_subject + "&learning=" + message_learning + "&code=" + message_code + "&level=" + message_level + "&resource=" + message_resource + "&media=" + message_media + "&filter=true";
    }
    else{
        $('#span_error').text("Please select at least one filter")
    }

 });


$(document).on('change','#filter_state_id', function(){
    var state = $('#filter_state_id').val();
    $("#filter_district_id").val('');
    if ((state.length == 0) || (state==null)||(state=='Select State'))
    {
        $('#filter_district_id').val('Select District');
        $('#filter_district_id').prop("disabled", true);

    }
    else {
        populatedistrict(state);
    }
});

//populate filter district
function populatedistrict(state) {
   $.ajax({
       type: 'GET',
       url: '/district-populate?state=' + state,
       success: function (data) {
           var districts = data;
           var selected_district = $('#selected_district').text();
           var options = '<option selected>Select District</option>';
           for (var obj = 0; obj < districts.length; obj++) {
               if(selected_district==districts[obj]['name']){
                   options += '<option value="' + districts[obj]['name'] + '" selected>' + districts[obj]['name'] + '</option>';
               }
               else {
                   options += '<option value="' + districts[obj]['name'] + '">' + districts[obj]['name'] + '</option>';
               }
           }
           $('#filter_district_id').prop("disabled", false);
           $('#filter_district_id').html(options);
       }
   })
}
 $(document.body).on('click', '.fa-sh-sv', function(){
    var dropmenu = $(this).next('.dropdown-menu');
    dropmenu.css({
        visibility: "hidden",
        display: "block"
    });
    // Necessary to remove class each time so we don't unwantedly use dropup's offset top
    dropmenu.parent().removeClass("dropup");

    dropmenu.removeClass("no-top");



    // Determine whether bottom of menu will be below window at current scroll position
    if (dropmenu.offset().top + dropmenu.outerHeight() > $(window).innerHeight() + $(window).scrollTop()){
        dropmenu.parent().addClass("dropup");
         $(this).next('.dropdown-menu').addClass("no-top");


    }

    // Return dropdown menu to fully hidden state
    dropmenu.removeAttr("style");
    dropmenu.attr('style','right: 0em');
});

//populate filter form with query string.
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
$(document).ready(function () {
    if (window.location.pathname=='/search' || window.location.pathname=='/search/'){
        var state = getParameterByName('state');
        var district = getParameterByName('district');
        var grade = getParameterByName('grade');
        var subject = getParameterByName('subject');
        var learning = getParameterByName('learning');
        var codes = getParameterByName('code');
        var level = getParameterByName('level');
        var resource = getParameterByName('resource');
        var media = getParameterByName('media');

        if(state){
            $('#filter_state_id').val(state);
            $('#filter_district_id').prop("disabled", false);
            populatedistrict(state);
        }if(district){
            $('#selected_district').text(district);
            $("#filter_district_id option:selected").remove();
            $('#filter_district_id').val(district).prop({selected: true});
        }if(grade){
            var grade_array = grade.split(',');
            $('#filter_grades').val(grade_array).select2();
        }if(subject){
            var subj_array = subject.split(',');
            $('#filter_subjects').val(subj_array).select2();
        }if(learning){
            var learning_array = learning.replace(/,\s*$/, "").split(',');
            $('#filter_learning').val(learning_array).select2();
        }if(codes){
            var codes_array = codes.replace(/,\s*$/, "").split(',');
            $('#filter_code_id').val(codes_array).select2();
        }if (level){
            var level_array = level.split(',');
            $('#filter_level_id').val(level_array).select2();
        }if(resource){
            var resource_array = resource.split(',');
            $('#filter_resource').val(resource_array).select2();
        }if(media){
            var media_array = media.split(',');
            $('#filter_media').val(media_array).select2();
        }
    }
    $('#clear-filter').click(function(){
        $('#filter-form .multiselect').val('').select2();
        $('#filter_learning').select2("val", "");
        $('#filter_district_id').val('Select District');
        $('#filter_district_id').prop("disabled", true);
        $('#filter_state_id').val('Select State');


    })

});

//----------------- social share---------------------//

$(document).on('click','.sharepinonfacebook', function(e){
    e.preventDefault();

    UserTracking($(this).attr('data-url'),'facebook');
    if (cid) {
        var url_facebook = 'https://www.ednora.com/resource/' + $(this).attr('data-url').trim()+"/?p="+cid;
        var facebook_popup = popupwindow('https://www.facebook.com/share.php?src=bm&u='+encodeURIComponent(url_facebook)+'&t=picks Pin&v=3&caption=VIEW MORE ON EDNORA.COM','Login to your facebook account to share the Pick',400 ,300);
        var timer_facebook = setInterval(checktfacebookpopup, 500);

        function checktfacebookpopup() {
            if (facebook_popup.closed) {
                clearInterval(timer_facebook);
            }
        }
    }
});

$(document).on('click','.sharepinontwitter', function(e){
    e.preventDefault();
    //check user is still logged in
    if (document.cookie.indexOf("loggedin") == -1) {window.location.href = window.location.href;}
    UserTracking($(this).attr('data-url'),'twitter');

    if (cid && title) {
        title = encodeURIComponent(title);
        var url_twitter = 'https://www.ednora.com/resource/' + $(this).attr('data-url').trim()+"/?p="+cid;
        var twitterpopup =  popupwindow('https://twitter.com/share?text='+title+'&via=Ednorallc&url='+encodeURIComponent(url_twitter) +'&source=ednora.com&v=3','Login to your Twitter account to share the Resource',400,300);
        var timer_twitter = setInterval(checktwitterpopup, 500);

        function checktwitterpopup() {
            if (twitterpopup.closed) {
                clearInterval(timer_twitter);
            }
        }
        title = '';
    }
});
$(document).on('click','.sharepinongoogle', function(e){
    e.preventDefault();
    //check user is still logged in
    if (document.cookie.indexOf("loggedin") == -1) {window.location.href = window.location.href;}
    UserTracking($(this).attr('data-url'),'google');
    if (cid) {
        url_google = 'https://www.ednora.com/resource/' + $(this).attr('data-url').trim()+"/?p="+cid;
        var google_popup =  popupwindow('https://plus.google.com/share?url='+encodeURIComponent(url_google),'Login to your Google+ account to share the Pick',400,300);
        var timer_google = setInterval(checkgooglepopup, 500);

        function checkgooglepopup() {
            if (google_popup.closed) {
                clearInterval(timer_google);
            }
        }

    }
});
$(document).on('click','.sharepinonfacebookmessenger', function(e){
    UserTracking($(this).attr('data-url'),'facebook messenger');
    if (cid) {
        facebookMessenger($(this).attr('data-url'),cid)
    }
});

$(document).on('click','.shareviaemail', function(e){
    var slug =  $(this).data('url');
    $('#pinslug').val(slug);
    $('#user_share').modal('show');
    $("#user_share").on('shown.bs.modal', function() {
        $(this).find('#user_share_email').focus();
    });
 });


$(document).on('click','#usershareok', function(e){
    //check user is still logged in
    if (document.cookie.indexOf("loggedin") == -1) {window.location.href = window.location.href;}

    var slug = $("#pinslug").val();
    var email = $('#user_share_email').val();

    email = email.trim();

    if (email.length==0){
        $('#user_share_email_validation').text('Required field').show();
        return false;
    }else if (!validateEmail(email)){
        $('#user_share_email_validation').text('Enter valid email').show();
        return false;
    }
    if(validateEmail(email)) {
        var data = new FormData();
        data.append('pinslug', slug);
        data.append('email', email);
        $('#emailsharesuccess').text('The resource was shared successfully');
        $.ajax({
            type: "POST",
            data: data,
            dataType: 'json',
            url: "/user-share/",
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.status == 'success') {

                    var share =   setTimeout(function() {
                        $('#user_share').modal('hide');

                    }, 0);

                }

            },
            error: function (data) {
            }
        });
    }
    else{
        $('#user_share').modal('show');
        $('#user_share_email_validation').text('Enter valid email');
        $('#user_share_email_validation').show();
        $('#usershareok').attr('disabled','disabled');

    }

});

$(document).on('show.bs.modal','#user_share', function () {
    $('#user_share_email').val('');
    $('#emailsharesuccess').text('');
    $('#user_share_email_validation').hide();
});

$(document).ready(function () {
    $('#user_share_email').keypress(function (e){
        if(e.keyCode == 13) {
            $('#usershareok').click();
        }
    });
});
    $(document).on('click', '.share_square', function (e) {//prevent share and logout if user is logout on another tab
        if (document.cookie.indexOf("loggedin") == -1) {
            window.location.href = window.location.href;
        }
    });
from readability.readability import Document
from bs4 import BeautifulSoup 
from bs4 import BeautifulSoup
import urllib.parse
import urllib.request
import urllib.parse
import urllib.request


def readability(url):
    try:
        opener = urllib.request.FancyURLopener({})
        opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0'), ('Connection', 'keep-alive') ,('Cache-Control','max-age=0'),('Accept','image/webp,image/*,*/*;q=0.8')]
        content = opener.open(url)
        html = content.read()
        html_article, image_list = Document(html).summary()
        title = Document(html).title()
        soup = BeautifulSoup(html_article, "lxml")
        article = soup.get_text()
        return title, article, image_list
    except Exception as e: print('readabilty', e)
    return "", "", []

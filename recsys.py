###################################################
#
# This code implements a patent pending algorithm
# to generate recommendations for user based on
# content details and user's activity. All content
# in this code is a copyright of Pickks.com. Any
# code taken from this file will be a violation of
# Pickks.com copyright and violators will be
# prosecuted.
#
###################################################

#sklearn
#numpy
#scipy
# Following variable needs to be turned on for 
# mode. 
TestMode = False
debug = False
debug_user_id = 45
debug_content_id = None

import datetime
print ("Starting")
print (datetime.datetime.now())

###################################################
# Configurations
###################################################
# Give search the same as pick
search_weight = 1.0

###################################################
# Class definitions
###################################################
class Content:
   'Content details'

   def __init__(self, id, name):
       self.id = id
       self.name = name
       self.tags = set()
       self.keywords = set()
   
class User:
    'User details'
    
    def __init__(self, id, company, industry, function, title, preference, 
                 m_rating=0, nc_rated=0, rc_mean_rating=0):
        self.id = id
        self.company = company
        self.industry = industry
        self.function = function
        self.title = title
        self.preference = preference
        self.mean_rating = m_rating
        self.rated_content_mean = rc_mean_rating
        self.num_content_rated = nc_rated
        self.profile = {}
        self.rated_content = set()
        self.active = True
        
class Rating:
    'Content Ratings from Users'
    
    def __init__(self, userID, contentID, rating, timestamp):
        self.userID = userID
        self.contentID = contentID
        self.rating = rating
        self.timestamp = timestamp

###################################################
# First declare all the globals that will be
# populated/used by different functions
###################################################

# These are populated when reading content JSON
contents               = []
content_id_obj_hash    = {}
content_idx_to_id_hash = {}
content_id_to_idx_hash = {} 

# These are populated when reading user JSON
users               = []
user_id_inst_hash   = {}
user_idx_to_id_hash = {}
user_id_to_idx_hash = {}

###################################################
# Helper functions go here
###################################################
from scipy import spatial
def CosineSimilarity(v1, v2):
   # If v1 or v2 have all elements as 0, just
   # return 0, else calculate the value
   # NOTE: This will return a 0 even if both arrays
   #       have all zeros. I am not expecting that
   #       input. Else we should fix it.
   returnVal = 0
   v1_allzero = all(v==0 for v in v1)
   v2_allzero = all(v==0 for v in v2)
   if not (v1_allzero or v2_allzero):
      returnVal = 1-spatial.distance.cosine(v1, v2)
   return returnVal
      
import iso8601
import pytz
import math
def AgeMultiplier(date):
   d1 = iso8601.parse_date(date)
   n1 = datetime.datetime.now(tz=pytz.utc)
   diff = n1 - d1
   diff_days = diff.days
   mult = 25*pow(0.5, diff_days)+1
   return mult

import scipy
import operator
from scipy.stats import pearsonr

def PearsonCorrCoeff(v1, v2):
   returnR = 0
   returnP = 0
   v1_allzero = all(v==0 for v in v1)
   v2_allzero = all(v==0 for v in v2)
   if not (v1_allzero or v2_allzero):
      returnR, returnP = pearsonr(v1, v2)
   return returnR, returnP

def PrettyPrintMatrix(matrix):
   s = [["{:.2f}".format(e) for e in row] for row in matrix]
   lens = [max(map(len, col)) for col in zip(*s)]
   fmt = ' '.join('{{:{}}}'.format(x) for x in lens)
   table = [fmt.format(*row) for row in s]
   print ('\n'.join(table))
   
###################################################
# End helper functions
###################################################

import json
import glob
import datetime
import re
import numpy

###################################################
# Read all content info and create an array of contents.
# Also put them in different hashes for easy access
###################################################
def ReadContentJSON(dump_dir):
   global contents
   global content_id_obj_hash
   global content_idx_to_id_hash
   global content_id_to_idx_hash
   
   contentCount = 0
   content_json = dump_dir + '/contents.json'
   with open(content_json, 'r') as content_file:
      count = 0
      for line in content_file:
         content = json.loads(line.replace('\r\n', ''))
         # Create content with ID and Title
         new_content = Content(content["CID"], content["title"])
         contents.append(new_content)
         content_id_obj_hash[new_content.id] = new_content
         content_idx_to_id_hash[contentCount] = new_content.id
         content_id_to_idx_hash[new_content.id] = contentCount
         contentCount += 1
         
         # Now add tags for content
         c_content_tags = new_content.tags
         for tag in content["Tags"]:
            if tag:
               c_content_tags.add(tag.lower())
         count += 1
         # Now add keywords for content (Enable after keyword added to content)
         #c_content_keywords = new_content.keywords
         #for keyword in content["Keywords"]:
         #   c_content_keywords.append(keyword)

   print ("Done reading content JSON file")
   print (datetime.datetime.now())

###################################################

###################################################
# Read all content info and create an array of contents.
# Also put them in different hashes for easy access
# Note the mapping:
# review_influence - Job Function
# movie_watching - Industry
# UserPreference - UserPreference
# fav_actor - Company
# country - Title
###################################################
def ReadUserJSON(dump_dir):
   global users
   global user_id_inst_hash
   global user_idx_to_id_hash
   global user_id_to_idx_hash

   userCount = 0
   user_json = dump_dir + '/users.json'
   with open(user_json, 'r') as user_file:
      for line in user_file:
         user = json.loads(line.replace('\r\n', ''))
         # First create a user
         new_user = User(user["UID"], user["fav_actor"], user["movie_watching"],
                         user["reviews_influence"], user["country"], user["UserPreference"])
         users.append(new_user)
         userID = new_user.id
         user_id_inst_hash[userID] = new_user
         user_idx_to_id_hash[userCount] = userID
         user_id_to_idx_hash[userID] = userCount
         userCount += 1   
         
   print ("Done reading user JSON file")
   print (datetime.datetime.now())

###################################################
# Following happens in this function
# Step 1: It then puts all activity in a hash (to get the max
# of all rating for any given content). If a user
# clicks on thumbnail, and then follows the link but
# also saves the pick, we want to pick the max of all
# these ratings as user's rating
# Step 3: Create a user profile
# Step 4: Create CB and CBCF rating matrix
###################################################
def ReadSearchJSON(dump_dir, file_to_read, search_weight):
   global user_id_inst_hash

   global debug
   global debug_user_id

   reg_exp = dump_dir + "/user_searches/(\w+).json"
   reg_comp = re.compile(reg_exp)
   reg_match = reg_comp.match(file_to_read)
   userID = int(reg_match.group(1))
   new_user = user_id_inst_hash[userID]
   c_profile = new_user.profile

   # TODO: Remove stop words from search profile
   
   # Now read all searches and build user profile
   with open(file_to_read,'r') as search_file:
      for line in search_file:
         search = json.loads(line)
         # TODO: Maybe we should split the string 
         c_tag = search["String"].lower()
         # Remove quotes from search
         c_tag = c_tag.replace('"','')
         c_time = search["Time"]
         
         if c_tag in c_profile:
            c_profile[c_tag] += search_weight * AgeMultiplier(c_time)
         else:
            c_profile[c_tag] = search_weight * AgeMultiplier(c_time)

   ## Print user profile for debug
   if debug and userID==debug_user_id:
      print ("User profile after search")
      print (', '.join(new_user.profile.keys()))

###################################################
# The following mapping is used for assigning
# weights
# 'cte'  -> 1         # click to expand
# 'ctu'  -> 2         # click to follow URL
# 'cts'  -> 3         # click to share
# 'like' -> 4         # Like
# 'pick' -> 5         # Save a pick
# 'unlike' -> -4         # Like
# 're-pick' -> 5         # Save a pick
###################################################
activity_type_to_rating = {'cte'  : 0.2,
                           'cfu'  : 0.4,
                           'cts'  : 0.6,
                           'like' : 0.8,
                           'pick' : 1.0,
                           're-pick': 1.0,
                           'unlike': -0.8
}
   
###################################################
# Following happens in this function
# Step 1: It puts all activity in a hash (to get the max
# of all rating for any given content). If a user
# clicks on thumbnail, and then follows the link but
# also saves the pick, we want to pick the max of all
# these ratings as user's rating
# Step 2: Create a user profile
# Step 3: Create CB and CBCF rating matrix
###################################################
def ReadActivityJSON(dump_dir, file_to_read, ratingMatrix,
                     cb_ratingMatrix, cbcf_ratingMatrix):
   global user_id_inst_hash
   global user_idx_to_id_hash
   global user_id_to_idx_hash

   global debug
   global debug_user_id
   
   reg_exp = dump_dir + "/user_activity/(\w+).json"
   reg_comp = re.compile(reg_exp)
   reg_match = reg_comp.match(file_to_read)
   userID = int(reg_match.group(1))
   new_user = user_id_inst_hash[userID]
   u_idx = user_id_to_idx_hash[userID]

   # Now read all activities and build user profile
   # First make a hash of content->rating so we take the maximum rating only
   user_cid_rating_hash = {}
   rated_content_set = new_user.rated_content
   with open(file_to_read,'r') as activity_file:
      for line in activity_file:
         activity = json.loads(line)
         #print (activity_type_to_rating[activity["type"]])
         if activity["content_id"] in user_cid_rating_hash:
            cur_rating_obj = user_cid_rating_hash[activity["content_id"]]
            cur_rating_val = cur_rating_obj.rating
            # If unlike rating, then remove the like rating
            if activity["type"] == 'unlike':
               # Found unlike, if current rating is only like, then remove it
               if cur_rating_val == 0.8:
                  del user_cid_rating_hash[activity["content_id"]]
            
            # Need to keep the maximum rating activity only
            if activity_type_to_rating[activity["type"]] > cur_rating_val:
               new_rating = Rating(userID, activity["content_id"], activity_type_to_rating[activity["type"]], activity["created_time"])
               user_cid_rating_hash[activity["content_id"]] = new_rating
         else:
            new_rating = Rating(userID, activity["content_id"], activity_type_to_rating[activity["type"]], activity["created_time"])
            user_cid_rating_hash[activity["content_id"]] = new_rating

   # Now that we have all user activity, time to 
   # create user profile based on user activity
   c_profile = new_user.profile
   for cid in user_cid_rating_hash.keys():
      rated_content_set.add(cid)
      c_rating = user_cid_rating_hash[cid]
      c_time = c_rating.timestamp
      c_content = content_id_obj_hash[cid]
      c_tags = c_content.tags
      c_keywords = c_content.keywords
      # First add the tags in profile
      for c_tag in c_tags:
         if c_tag in c_profile:
            c_profile[c_tag] += c_rating.rating
         else:
            c_profile[c_tag] = c_rating.rating
      
      # Then add the keywords in user profile
      for c_keyword in c_keywords:
         if c_keyword in c_profile:
            c_profile[c_keyword] += c_rating.rating * AgeMultiplier(c_time)
         else:
            c_profile[c_keyword] = c_rating.rating * AgeMultiplier(c_time)

   new_user.num_content_rated = len(user_cid_rating_hash.keys())
   userProfile = set(new_user.profile.keys())

   ## Print user profile for debug
   if True and debug and userID==debug_user_id:   
      print ("User profile after activity")
      print (', '.join(userProfile))
   
   # Now create content based rating for all content
   # Also create a mixed content/collaborative filtering rating
   user_cid_rating_hash_keys = user_cid_rating_hash.keys()
   for c_idx in range(len(contents)):
      cid = content_idx_to_id_hash[c_idx]
      content = content_id_obj_hash[cid]
      contentTagKeywordUnion = (content.tags | content.keywords)
      userContentUnion = (userProfile | contentTagKeywordUnion)

      userVec = []
      contentVec = []
      for elem in userContentUnion:
         if elem in userProfile:
            userVec.append(new_user.profile[elem])
         else:
            userVec.append(0)

         if elem in contentTagKeywordUnion:
            contentVec.append(1)
         else:
            contentVec.append(0)
            
      ## Print the two vectors for debug
      #print ('User Vec    ', ' '.join(str(userVec)))
      #print ('Content Vec ', ' '.join(str(contentVec)))
      cb_sim =  CosineSimilarity(userVec, contentVec)
      #print (cb_sim)
      #print (u_idx, c_idx)
      cb_ratingMatrix[u_idx][c_idx] = cb_sim
      # If user has rated the item, put their rating
      # If not, then just put content based rating
      # TODO: Make sure they are normalized to 1
      if cid in user_cid_rating_hash_keys:
         user_rating = user_cid_rating_hash[cid].rating
         cbcf_ratingMatrix[u_idx][c_idx] = user_rating
         ratingMatrix[u_idx][c_idx] = user_rating
      else:
         cbcf_ratingMatrix[u_idx][c_idx] = cb_sim
         ratingMatrix[u_idx][c_idx] = -1  # -1 means not rated by user

   # Print CB and CBCF arrays for debug
   if True and debug and userID==debug_user_id:
      cid_cb_hash = {}
      cid_cbcf_hash = {}
      print ("Begin CB and CBCF for UserID ", userID)
      for c_idx, val in enumerate(cb_ratingMatrix[u_idx]):
         cid = content_idx_to_id_hash[c_idx]
         cid_cb_hash[cid] = val

      for c_idx, val in enumerate(cbcf_ratingMatrix[u_idx]):
         cid = content_idx_to_id_hash[c_idx]
         cid_cbcf_hash[cid] = val
         
      print ("CID    CB      CBCF")
      for cid in cid_cb_hash.keys():
         print ('{0:2d}  {1:3f}  {2:3f}'.format(cid, cid_cb_hash[cid], cid_cbcf_hash[cid]))
      print ("End CB and CBCF for userID ", userID)
      
   # Calculate the psuedo-rating mean
   c_rating_sum = 0
   for rating in cbcf_ratingMatrix[u_idx]:
      c_rating_sum += rating

   new_user.mean_rating = c_rating_sum/len(contents)
   # Print CB and CBCF arrays for debug
   if True and debug and userID==debug_user_id:
      print (c_rating_sum)
      print (new_user.mean_rating)
      print ("******")
   
   #n_array = numpy.array(cbcf_ratingMatrix[u_idx])
   #new_user.mean_rating = numpy.mean(n_array, axis=0)
   #print (new_user.mean_rating)
   # Calculate the actual-rating mean
   # Only used for testing purpose
   c_rating_sum = 0
   c_rating_count = 0
   for rating in ratingMatrix[u_idx]:
      if rating != -1:
         c_rating_sum += rating
         c_rating_count += 1

   if c_rating_count > 0:
      new_user.rated_content_mean = c_rating_sum/c_rating_count
   else:
      new_user.rated_content_mean = 0

   if False and debug and new_user.id == debug_user_id:
      ux_idx = user_id_to_idx_hash[debug_user_id]
      for m_idx in range(len(contents)):
         print ("ux_idx : m_idx : cb_ratingMatrix[ux_idx][m_idx]", ux_idx, m_idx, cb_ratingMatrix[ux_idx][m_idx])
 
###################################################


###################################################
# Find the intersection of each pair
# Also find the harmonic mean of user rating count
###################################################
def GetHybridCorWeight(hybridCorWeightMatrix):
   global users
   global user_id_to_idx_hash
   
   for user_x in users:
      content_set_x = user_x.rated_content
      for user_y in users:
         content_set_y = user_y.rated_content
         sigWeightFactor = 0
         commonItemsCount = len(content_set_x.intersection(content_set_y))
         if (commonItemsCount >= 50):
            sigWeightFactor = 1
         else:
            sigWeightFactor = commonItemsCount/50

         harMean = 0
         if not (len(content_set_x) + len(content_set_y) == 0):
            harMean = 2*len(content_set_x)*len(content_set_y)/(len(content_set_x)+len(content_set_y))

         ux_idx = user_id_to_idx_hash[user_x.id]
         uy_idx = user_id_to_idx_hash[user_y.id]
         hybridCorWeightMatrix[ux_idx][uy_idx] = sigWeightFactor + harMean


###################################################
# The routine that does prediction computation
# TODO: Use PyTables for large arrays
# http://www.davekuhlman.org/scipy_guide_01.html#installing-pytables
# http://stackoverflow.com/questions/15626673/matrix-operations-with-gigantic-matrices-in-python?lq=1
# TODO: Parallel numpy
# http://stackoverflow.com/questions/17785275/share-large-read-only-numpy-array-between-multiprocessing-processes/17786444#17786444
###################################################
def ComputeCBCFPredictionForUser(user_x, cb_ratingMatrix, cbcf_ratingMatrix, hybridCorWeightMatrix):
   global user_id_to_idx_hash
   
   global debug
   global debug_user_id

   ux_idx = user_id_to_idx_hash[user_x.id]
   #print "Building for user with id " + str(uid_x)
   # First find PC between this user and all other users
   # We store that in a dictionary
   # TODO: Use other user profile information while finding
   #       PC between users
   corDict = {}
   for user_y in users:
      uy_idx = user_id_to_idx_hash[user_y.id]
      x = scipy.array(cbcf_ratingMatrix[ux_idx])
      y = scipy.array(cbcf_ratingMatrix[uy_idx])

      # R is Pearson correlation coefficient
      # P is only a probability (that we will ignore)
      r_row, p_value = PearsonCorrCoeff(x, y)
      corDict[uy_idx] = r_row

   # Now sort corDict in a reverse order and pick top n neighbors
   n = 20
   sortedCorDict = sorted(corDict.items(), key=operator.itemgetter(1))
   sortedCorDict.reverse()
   topNbors = [w[0] for w in sortedCorDict[:n]]
   #print topNbors

   # Now calculate the self-weight
   selfWeightMax = 2
   selfWeight = selfWeightMax
   if (user_x.num_content_rated < 50): 
      selfWeight = user_x.num_content_rated * selfWeightMax / 50
   
   # PredDict is for local sorting only
   predDict = {}
   if debug and user_x.id == debug_user_id:
      print ("Self-weight ", selfWeight)
      print ("  CID    Mean      Num1       num2      den1      den2    pred ")

   for m_idx in range(len(contents)):      
      numTerm1 = selfWeight * (cb_ratingMatrix[ux_idx][m_idx] - user_x.mean_rating)
      numTerm2 = 0.0
      denTerm1 = selfWeight
      denTerm2 = 0.0
      for nbor_idx in topNbors:
         if (ux_idx != nbor_idx):
            psonCoeff = corDict[nbor_idx]
            hw = hybridCorWeightMatrix[ux_idx][nbor_idx]
            denTerm2 += hw * psonCoeff
            nbor_user = users[nbor_idx]
            nbor_mean_rating = nbor_user.mean_rating
            nbor_movie_rating = cbcf_ratingMatrix[ux_idx][m_idx]
            numTerm2 += hw * psonCoeff * (nbor_movie_rating - nbor_mean_rating)

      if (denTerm1+denTerm2) == 0:
         predVal = 0
      else:
         predVal = user_x.mean_rating + (numTerm1 + numTerm2)/(denTerm1 + denTerm2)
         
      if debug and user_x.id == debug_user_id:
         print ('{0:2f} {1:2f}  {2:2f}  {3:2f} {4:2f} {5:2f} {6:2f} '.format(content_idx_to_id_hash[m_idx], user_x.mean_rating, numTerm1, numTerm2, denTerm1, denTerm2, predVal))
      if not math.isnan(predVal):
         # TODO: Remove content already pinned by user 
         # by setting its prediction to 0
         predDict[m_idx] = predVal
   
   # Get top 1000 predictions for this user
   n = 1000
   sortedPredDict = sorted(predDict.items(), key=operator.itemgetter(1))
   sortedPredDict.reverse()

   topPredContentID = []
   insertIdx = 0
   # Don't pick anything less than min CB rating
   # TODO: We need a better hueristic
   min_rating = user_x.mean_rating
   # for rating in cb_ratingMatrix[ux_idx]:
   #    if rating != 0:
   #       if rating < min_rating:
   #          min_rating = rating
      
   for w in sortedPredDict[:n]:
      if debug and user_x.id == debug_user_id:
         print ('{0:2f} '.format(w[1]))
      if (w[1] > min_rating):
         topPredContentID.insert(insertIdx, content_idx_to_id_hash[w[0]])
         insertIdx += 1

   if debug and user_x.id == debug_user_id:
      print (min_rating)
      print ('Recommendation    ', ' '.join(str(topPredContentID)))
      
   # Put the top predictions in out_file_list
   rec_to_dump = {}
   rec_to_dump["Recommendations"] = topPredContentID
   rec_to_dump["UID"] = user_x.id
   return rec_to_dump

###################################################
# The main routine that puts things together
###################################################
def GenerateRecommendation(dump_dir, result_dir):
   # Step 1
   # First Read all the content JSON
   ReadContentJSON(dump_dir)
   
   # Step 2
   # Then read user JSON
   ReadUserJSON(dump_dir)
   
   # Step 3
   # Searches done by each user is stored in a file
   glob_str = dump_dir + "/user_searches/*.json"
   all_activity_files_to_read = glob.glob(glob_str)
   for file_to_read in all_activity_files_to_read:
      ReadSearchJSON(dump_dir, file_to_read, search_weight)

   print ("Done reading search JSON file")
   print (datetime.datetime.now())

   # Step 4
   # Activity for each user is stored in a file
   # With some effort this loop an be parallelized
   # Each loop can be executed on a different machine
   # and the profile dumped in a file for read by main 
   # process
   ratingMatrix      = [[0 for x in range(len(contents))] for x in range(len(users))]
   cb_ratingMatrix   = [[0 for x in range(len(contents))] for x in range(len(users))]
   cbcf_ratingMatrix = [[0 for x in range(len(contents))] for x in range(len(users))]

   glob_str = dump_dir + "/user_activity/*.json"
   all_activity_files_to_read = glob.glob(glob_str)
   debug_pop = False
   for file_to_read in all_activity_files_to_read:
      ReadActivityJSON(dump_dir, file_to_read, ratingMatrix,
                       cb_ratingMatrix, cbcf_ratingMatrix)

   print ("Done reading activity JSON file")
   print (datetime.datetime.now())

   # Step 5
   # Get hybrid correlation weight that will be used
   # in final prediction calculation
   hybridCorWeightMatrix = [[0 for x in range(len(users))] for x in range(len(users))]

   GetHybridCorWeight(hybridCorWeightMatrix)

   # Step 6
   # Compute predictions for each user
   out_file_list = []
   for user_x in users:
      out_file_list.append(
         ComputeCBCFPredictionForUser(user_x, cb_ratingMatrix, cbcf_ratingMatrix, hybridCorWeightMatrix)
      )

   print ("Done creating predictions")
   print (datetime.datetime.now())

   # Step 7
   # Finally create the JSON file with all predictions

   fn = str(datetime.datetime.now())
   fn = fn.replace(' ','_')
   fn = fn.split('.')[0]
   filename = result_dir + "/result_"+fn+".json"
   #print (filename)
   # replace any space with '_' and get rid
   # of millisecond and use that as result filename
   with open(filename, 'w') as fp:
      for item in out_file_list:
         json.dump(item, fp, sort_keys=True)
         fp.write('\n')

   return filename
__author__ = 'sayone'
import urllib.parse
import urllib.request
import urllib.parse
import urllib.request
import re
from pdf_miner.pdfminer.pdfinterp import PDFResourceManager, process_pdf
from pdf_miner.pdfminer.converter import TextConverter
from pdf_miner.pdfminer.layout import LAParams
from io import StringIO
from pdf_miner.pdfminer.utils import set_debug_logging


def get_pdf_info(url):
    password = ''
    pagenos = set()
    maxpages = 3
    caching = True
    laparams = LAParams()
    outfp = StringIO()
    rsrcmgr = PDFResourceManager(caching=caching)
    opener = urllib.request.FancyURLopener({})
    opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0'),
                         ('Connection', 'keep-alive')]
    device = TextConverter(rsrcmgr, outfp, laparams=laparams)
    fp = opener.open(url)
    f = process_pdf(rsrcmgr, device, fp, pagenos, maxpages=maxpages, password=password, caching=caching,
                    check_extractable=True)
    fp.close()
    device.close()
    return  outfp.getvalue().strip()




def get_paragraphs(text):
    #print '##################################'
    #print text
    #print '##################################'
    ########################################
    # Note: This variable is used to configure
    # the number of pages to be read
    words_in_para = 15
    para_toreturn = 3
    ########################################
    return_title = ""
    return_val = ""
    buffer = ""
    prev_line = ""
    para_count = 0
    for line in text.splitlines():
        # Remove the final '-'
        # print (line)
        # TODO: Fix this
        if line.endswith('-'):
            line = line[:-1]
        # If line is empty (\n only) then we probably ended
        # a paragraph, so add buffer to return_val
        if line == "":
            # Some paragraphs should be skipped
            # Let us see if we can find them
            accept_para = True
            # Criterion 1: Number of words in paragraph to be
            #              greater than 10
            # Criterion 2: If return_title is null, then the first line that doesn't have anything stupid may be
            #              the title. Also should have more than 3 words
            if return_title == "":
                if len(buffer.split()) > 3:
                    if any(c.isalpha() for c in buffer):
                        return_title = buffer
                    # I accepted the para for title, let us not take it for description
                    accept_para = False

            if len(buffer.split()) < 10:
                accept_para = False
            if accept_para:
                return_val += buffer
                #print ('Accepting: ' + buffer)
                para_count += 1
            buffer = ""
        else:
            # Some lines should be skipped. Let us
            # see if we can find them
            accept_line = True
            # Criterion 1: Line should not contain more than 4 consecutive special characters (.,#,/,\)
            if re.search('\.\.\.\.', line) or re.search('\#\#\#\#', line) or re.search('\\\\\\\\', line) or re.search('\/\/\/\/', line):
                accept_line = False
                #print line
            if accept_line:
                buffer += line
                buffer += ' '

        if para_count == 3:
            break
        prev_line = line

    return return_title, return_val

text= get_pdf_info(url)
#print (text)
(title, description) = get_paragraphs(text)

